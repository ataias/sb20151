#ifndef OBJECT_H__
#define OBJECT_H__

/*
 * Alunos:
 * - Ataias Pereira Reis - 10/0093817
 * - Pedro Paulo Struck Lima - 11/0073983
 * ---------------------------------------
 * Object.h
 *
 * Declara protótipos da classe Object que é utilizada para armazenar código objeto
 * inclui métodos:
 *  constructor Object() - inicializa objeto
 *  push() - insere um código string com seu número de linha originário, pode também indicar
 *           se é uma constante
 *  get(pos) - obtém o código string no endereço pos, pode emitir exceção
 *  getLine(pos) - diz a linha na qual o código em pos se originou
 *  isConst(pos) - indica se o que está no endereço pos é uma constante
 *  set() - permite modificar um endereço do código objeto
 *  size() - diz o tamanho do código
 *  setDataInit() - usado para marcar o início da seção de dados
 *  isInTextSection() - indica se endereço é da seção de texto (não está sendo utilizado no momento)
 *  getDataInit() - diz o início da seção de dados
 * */

class Object{
private:
  std::vector<std::string> obj;
  std::vector<bool> isConstant;
  std::vector<unsigned> line;
  unsigned addrDataInit = 0;
  bool hasData = false;
public:
  Object();
  void push(std::string code, unsigned line);
  void push(std::string code, unsigned line, bool isConst);
  std::string get(unsigned pos);
  unsigned getLine(unsigned pos);
  bool isConst(unsigned pos);
  void print();
  void set(std::string address, unsigned pos);
  unsigned size();
  void setDataInit(unsigned addrDataInit);
  bool isInTextSection(unsigned pos);
  unsigned getDataInit();
};

#endif
