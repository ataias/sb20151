#ifndef CMDTABLE_H__
#define CMDTABLE_H__

/*
 * Alunos:
 * - Ataias Pereira Reis - 10/0093817
 * - Pedro Paulo Struck Lima - 11/0073983
 * ---------------------------------------
 * CmdTable.h
 *
 * Declara protótipos da classe CmdTable que contém informações sobre instruções e diretivas
 * inclui métodos:
 *  print() - imprimir tabela inteira
 *  get() - obter um comando a partir de seu nome ou opcode
 *  isCmd() - verifica se uma string é um comando válido
 *  isCmdType1() - verifica se string é um comando que só utiliza um operando
 *  isJump() - verifica se string é um tipo de jump
 * */

#include <Struct.h>

class CmdTable{
private:
  std::vector<Cmd> t;
public:
  CmdTable();

  void print();

  Cmd get(std::string command);

  Cmd get(unsigned opcode);

  bool isCmd(std::string l);

  bool isCmdType1(std::string l);

  bool isJump(std::string l);
};

#endif
