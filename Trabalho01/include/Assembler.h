#ifndef ASSEMBLER_H__
#define ASSEMBLER_H__

/*
 * Alunos:
 * - Ataias Pereira Reis - 10/0093817
 * - Pedro Paulo Struck Lima - 11/0073983
 * ---------------------------------------
 * Assembler.h
 *
 * Declara protótipos classe Assembler que é largamente utilizada no projeto
 *
 * */

#include <Object.h>
#include <Struct.h>
#include <Table.h>
#include <set>
#include <map>

//ainda em construção
class Assembler{
private:
  enum Section { TEXT, DATA, UNDEF};
  Section cSection; //utilizado para dizer seção atual

  //instanciar tabelas e classe objeto
  Table sym = Table("Symbol not found\n", "SYMBOLS TABLE");
  Table def = Table("Definition not found\n", "TABLE DEFINITION");
  Table use = Table("Use symbol not found\n", "TABLE USE");
  Object object = Object();
  CmdTable cmdTable = CmdTable();

  //flags
  bool isModule = false; //indica se é um módulo. Caso não seja, não deve ter begin ou end
  bool endModule = false;
  bool toBeLinked = false; //verdadeiro se houver sido utilizadas public ou extern

  //expressões regulares
  std::regex opsyntax; //operand syntax
  std::regex validtoken;
  std::regex labelsyntax;
  std::regex tokensyntax;


  unsigned nErros = 0; //número de erros

  //instanciar vetores utilizados no projetos
  std::vector<Elements> splitted; //assembly dividido em seções
  std::vector<std::string> code; //código assembly
  std::vector<std::string> externvars; //indica quais variáveis são externas
  std::vector<bool> bitMapRelative; //bit 1 indica que endereço é relativo
  
  //Este map foi adicionado para se verificar erro de out of scope
  // mapeia unsigned para struct Var (veja Struct.h)
  //unsigned indica o endereço no qual a variável foi definida
  //Var contém o nome da variável, seu tamanho alocado e a linha na qual foi definida
  std::map<unsigned,Var> variableScope; 
public:

  Assembler(std::vector<std::string> code);

  void split(std::string line, unsigned l);

  void printSplitted();

  void pass1();

  void pass2();

  void createSymTable();

  void createDefTable();

  void createUseTable();

  void exeCmd(Elements spline, unsigned l, int *pc);

  void printUndeclared(std::set<std::string> uLabels);

  void printLex(Elements spline, unsigned l, std::regex tokentype);

  void verifyDivision(unsigned dataInit);

  void memoryCheck(unsigned dataInit);

  void constCheck(unsigned dataInit);

  void jumpCheck(unsigned dataInit);

  bool writeFile(std::string filename);
};

#endif
