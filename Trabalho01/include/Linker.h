#ifndef LINKER_H__
#define LINKER_H__

/*
 * Alunos:
 * - Ataias Pereira Reis - 10/0093817
 * - Pedro Paulo Struck Lima - 11/0073983
 * ---------------------------------------
 * Linker.h
 *
 * Declara protótipos da classe Linker que é utilizada para ligar dois módulos
 * inclui métodos:
 *  constructor Linker() - inicializa objeto
 *  readFile() - método privado utilizado para ler arquivo
 *  printInfo() - utilizado principalmente no desenvolvimento para verificar resultados na tela.
 *      Imprime a tabela geral de definições, códigos de entrada...
 *  printLinkedInfo() - imprime código objeto ligado
 *  link() - realiza a ligação
 *  writeFile() - escreve o executável no arquivo referente ao argumento
 * */

#include <Struct.h>
#include <Table.h>
#include <Object.h>

class Linker{
private:
  Table defA = Table("Definition not found\n", "TABLE DEFINITION A");
  Table defB = Table("Definition not found\n", "TABLE DEFINITION B");
  Table TGD = Table("Definition not found\n", "GENERAL DEFINITION TABLE");
  Table useA = Table("Use symbol not found\n", "TABLE USE A");
  Table useB = Table("Use symbol not found\n", "TABLE USE B");
  Object objectA = Object();
  Object objectB = Object();
  Object linkedObj = Object();
  CmdTable cmdTable = CmdTable();

  enum Sector { DEFINITION, USE, CODE, BITMAP, UNDEF};

  std::vector<bool> bitMapRelativeA;
  std::vector<bool> bitMapRelativeB;

  int nErros;

  bool readFile(std::string modFilename, Object *obj, Table *use, Table *def, std::vector<bool> *bitMapRel);

public:
  Linker(std::string modA, std::string modB);

  void printInfo();

  void printLinkedInfo();

  void link();

  bool writeFile(std::string filename);

};

#endif
