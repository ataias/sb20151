;Divisão por zero
section text
input a
input a+1
load a
div c
store b
load a+1
div d
store b+1
output b
output b+1
stop

section data
c: const 0x0000
d: const 0x00
umn: const -1
a: space 2
b: space 2


;Data do teste: 22/05/2015
;./montador ../Testes/Bateria\ de\ Erros/T7.asm ../Testes/out.o

;L6: Divisão por zero. Erro semântico.
;L9: Divisão por zero. Erro semântico.
;
;Erros encontrados: 2
;Arquivo objeto não pôde ser gerado.
