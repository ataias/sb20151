; endereço de memória não-reservado, modificação de constante e uns errinhos
section text
mod_b: begin
r: extern
a: input a
input b
input b +1
input b+2
output r+2
output b+3
input c
stop

section data
b: space 2
c: const -4
end

;Data do teste: 22/05/2015
;./montador ../Testes/Bateria\ de\ Erros/T11.asm ../Testes/out.o

;Novos erros encontrados:
;L8: Modificação de valor constante. Erro semântico.
;Por que? b é uma variável, declarada com space 2 e não constate


;L8: Tentativa de acesso a endereço não reservado para variável "b" definida em L15 do arquivo. Erro semântico.
;L10: Tentativa de acesso a endereço não reservado para variável "b" definida em L15 do arquivo. Erro semântico.
;L8: Modificação de valor constante. Erro semântico.
;L11: Modificação de valor constante. Erro semântico.
;L5: Endereço de instruções ao invés de memória. Erro semântico.
;L10: Endereço de memória não reservado. Erro semântico.

;Erros encontrados: 6
;Arquivo objeto não pôde ser gerado.
