; dois rótulos na mesma linha, rótulo repetido e outros

section text
input a
repeate: asdf*: input d
jmpn repeate
output d
stop

section data
a: b: const -1
c: const 2
d: space
d: space 3

;Data do teste: 22/05/2015
;./montador ../Testes/Bateria\ de\ Erros/T8.asm ../Testes/out.o

;L5: Mais de um rótulo definido na mesma linha. Erro sintático.
;L11: Mais de um rótulo definido na mesma linha. Erro sintático.
;L14: Label redefinido. Erro semântico.
;L4: Modificação de valor constante. Erro semântico.
;
;Erros encontrados: 4
;Arquivo objeto não pôde ser gerado.
