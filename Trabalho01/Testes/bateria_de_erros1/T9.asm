; seção inválida

section asd2
input a
repeate: asdf*: input d
jmpn repeate
output d
stop

section data2
a: b: const -1
c: const 2
d: space
d: space 3

;Data do teste: 22/05/2015
;./montador ../Testes/Bateria\ de\ Erros/T9.asm ../Testes/out.o

;L5: Mais de um rótulo definido na mesma linha. Erro sintático.
;L11: Mais de um rótulo definido na mesma linha. Erro sintático.
;L3: Seção "asd2" inválida. Erro Semântico
;L4: Instrução "input" utilizada em seção indefinida. Erro semântico.
;L5: Instrução "input" utilizada em seção indefinida. Erro semântico.
;L6: Instrução "jmpn" utilizada em seção indefinida. Erro semântico.
;L7: Instrução "output" utilizada em seção indefinida. Erro semântico.
;L8: Instrução "stop" utilizada em seção indefinida. Erro semântico.
;L10: Seção "data2" inválida. Erro Semântico
;L11: Uso de diretiva "const" em seção não definida. Erro semântico.
;L12: Uso de diretiva "const" em seção não definida. Erro semântico.
;L13: Uso de diretiva "space" em seção não definida. Erro semântico.
;L14: Label redefinido. Erro semântico.
;L14: Uso de diretiva "space" em seção não definida. Erro semântico.
;
;Erros encontrados: 14
;Arquivo objeto não pôde ser gerado.
