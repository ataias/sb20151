;Declarações ausentes e repetidas e outros erros
section text
mod_a: begin
r: extern

input f
output e
load r
load c
mult d
store r
store d
stop

section data
r: const 0x
c: space 1
d: const -1234
e: const 123 34
end

;Data do teste: 22/05/2015
;./montador ../Testes/Bateria\ de\ Erros/T4.asm ../Testes/out.o
;está acusando "Constante 0x invalida" agora.

;L16: Label redefinido. Erro semântico.
;L16: Constante "0x" inválida. Erro léxico.
;L16: Argumento "0x" inválido. Erro léxico.
;L19: Número de argumentos de const inválido. Erro sintático.
;L6: Label indefinida "f". Erro semântico.
;L12: Modificação de valor constante. Erro semântico.
;
;Erros encontrados: 6
;Arquivo objeto não pôde ser gerado.

