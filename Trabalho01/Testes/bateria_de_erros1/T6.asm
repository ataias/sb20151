;Teste de diretivas e instruções inválidas
section text
mod_a: begin
mod_b: extern
jmp mod_b
jmpnn a b*
add add;04

section data
jmp: cos 30
l: const 0x30
end

;Data do teste: 22/05/2015
;./montador ../Testes/Bateria\ de\ Erros/T6.asm ../Testes/out.o

;L6: Comando "jmpnn" inválido.
;L10: Label não pode ser uma palavra reservada. Erro léxico.
;L10: Comando "cos" inválido.
;L6: Comando inexistente "jmpnn". Erro semântico.
;L6: Argumento "b*" inválido. Erro léxico.
;L10: Comando inexistente "cos". Erro semântico.
;L7: Label indefinida "add". Erro semântico.

;Erros encontrados: 7
;Arquivo objeto não pôde ser gerado.

