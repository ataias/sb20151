;Teste de pulo para rótulos inválidos e outros erros
section text
mod_a: begin
a: extern
jmp r
load c
jmpz a
jmp a* *
jmp c*
copy a, b,

jmp d
stop

section data
c: const 0x1
d: space
end

;Data do teste: 22/05/2015
;./montador ../Testes/Bateria\ de\ Erros/T5.asm ../Testes/out.o

;L10: Quantidade de vírgulas incorreta. Erro sintático.
;L8: Operando "a*" inválido para instrução "jmp". Erro léxico.
;L8: Instrução "jmp" utilizada incorretamente. Erro sintático.
;L9: Operando "c*" inválido para instrução "jmp". Erro léxico.
;L5: Label indefinida "r". Erro semântico.
;L10: Label indefinida "b". Erro semântico.
;L12: Pulo inválido para endereço de memória ao invés de instruções. Erro semântico.
;
;Erros encontrados: 7
;Arquivo objeto não pôde ser gerado.
