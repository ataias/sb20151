;7– divisão por zero;
section text
	input	a
	load 	a
	div		b
	store	a
	output	a
	stop
	
section data
	a:	space
	b:	const	0

;data do teste: 22/05/2015
;Saida do montador 

;L5: Divisão por zero. Erro semântico.

;Erros encontrados: 1
;Arquivo objeto não pôde ser gerado.
