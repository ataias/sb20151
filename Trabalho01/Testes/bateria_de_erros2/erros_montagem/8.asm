;8– instruções com a quantidade de operando inválida;
section text
	input	a	b ;2 em instrução de 1
	copy	a, b, c ;3 em instrução de 2
	load	; 0 em instrução de 1
	stop	c ;1 em instrução de 0
	
section data
	a:	space
	b:	const	1

;data do teste: 22/05/2015
;Saida do montador 

;L3: Instrução "input" utilizada incorretamente. Erro sintático.
;L4: Número de argumentos para instrução "copy" inválido. Erro sintático.
;L5: Número de argumentos para instrução "load" inválido. Erro sintático.
;L6: Número de argumentos para instrução "stop" inválido. Erro sintático.
;L4: Modificação de valor constante. Erro semântico.

;Erros encontrados: 5
;Arquivo objeto não pôde ser gerado.

