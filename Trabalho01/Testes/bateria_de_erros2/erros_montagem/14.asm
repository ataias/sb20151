;14– tipo de argumento inválido;
section text
	input	1		;aqui
	output	2		;aqui
	copy	2, a	;aqui         copy pode aceitar constantes? não acusou aqui
	stop
	
section data
	a:	space	d	;aqui
	b:	const	c	;aqui			parece que está aceitando essa constante como "c" também. C não é numero


;data do teste: 22/05/2015
;Saida do montador

;novos erros
;L5: Operando "2" inválido para instrução "copy". Erro sintático.
;L10: Constante "c" inválida. Erro léxico.

;L3: Operando "1" inválido para instrução "input". Erro léxico.
;L4: Operando "2" inválido para instrução "output". Erro léxico.
;L5: Operando "2" inválido para instrução "copy". Erro sintático.
;L9: Argumento de space inválido. Erro sintático.
;L10: Constante "c" inválida. Erro léxico.
;L3: Endereço de instruções ao invés de memória. Erro semântico.
;L4: Endereço de instruções ao invés de memória. Erro semântico.
;
;Erros encontrados: 7
;Arquivo objeto não pôde ser gerado.

