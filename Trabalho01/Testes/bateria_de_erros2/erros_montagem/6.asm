;6– diretivas ou instruções na seção errada;
section text
	a:	space		;aqui 
	b: 	const 	1	;aqui
	
	input	a
	output	a
	output	c
	stop
	
section data
	output	b 		;aqui
	c:		extern 	;aqui
	public 	b 		;aqui

;data do teste: 22/05/2015
;Saida do montador 

;L3: Space utilizado em seção errada. Erro semântico.
;L4: Const utilizado em seção errada. Erro semântico.
;L12: Instrução "output" utilizada em seção errada. Erro semântico.
;L13: Diretiva extern utilizada fora de módulo. Erro semântico.
;L13: Diretiva "extern" utilizada em seção errada. Erro semântico.
;L14: Diretiva public utilizada fora de módulo. Erro semântico.
;L14: Diretiva "public" utilizada depois da primeira instrução. Erro semântico.

;Erros encontrados: 7
;Arquivo objeto não pôde ser gerado.

