;12– seção (TEXT ou DATA) faltante;
; as duas estão faltando
	input	a
	output	a
	output	b
	stop
	
	a:	space
	b:	const	1

;data do teste: 22/05/2015
;Saida do montador 

;L3: Instrução "input" utilizada em seção indefinida. Erro semântico.
;L4: Instrução "output" utilizada em seção indefinida. Erro semântico.
;L5: Instrução "output" utilizada em seção indefinida. Erro semântico.
;L6: Instrução "stop" utilizada em seção indefinida. Erro semântico.
;L8: Uso de diretiva "space" em seção não definida. Erro semântico.
;L9: Uso de diretiva "const" em seção não definida. Erro semântico.

;Erros encontrados: 6
;Arquivo objeto não pôde ser gerado.

