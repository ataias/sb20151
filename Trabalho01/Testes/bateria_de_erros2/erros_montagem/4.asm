;4– diretivas inválidas;
section text
	input	a
	output	a
	output	b
	stop
	
section data
	a:	spacex
	b:	consty	0



;data do teste: 22/05/2015
;Saida do montador 

;L9: Comando "spacex" inválido.
;L10: Comando "consty" inválido.
;L9: Comando inexistente "spacex". Erro semântico.
;L10: Comando inexistente "consty". Erro semântico.
;L3: Endereço de memória não reservado. Erro semântico.
;L4: Endereço de memória não reservado. Erro semântico.
;L5: Endereço de memória não reservado. Erro semântico.

;Erros encontrados: 7
;Arquivo objeto não pôde ser gerado.
