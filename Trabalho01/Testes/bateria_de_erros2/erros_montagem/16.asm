;16- modificação de valor constante
section text
	input	b	;aqui
	output	b
	load	b
	store	b	;aqui
	stop

section data
	b:	const	1

;data do teste: 22/05/2015
;Saida do montador 

;L2: Modificação de valor constante. Erro semântico.
;L5: Modificação de valor constante. Erro semântico.

;Erros encontrados: 2
;Arquivo objeto não pôde ser gerado.
