;5– instruções inválidas;
section text
	input		a
	inputxxx	a
	outputxx	a
	output		a
	loadxx		b
	laod		a
	jnp			l1
	
	l1:		spot
			stop
	
section data
	a:	space
	b:	const	0


;data do teste: 22/05/2015
;Saida do montador 

;L4: Comando "inputxxx" inválido.
;L5: Comando "outputxx" inválido.
;L7: Comando "loadxx" inválido.
;L8: Comando "laod" inválido.
;L9: Comando "jnp" inválido.
;L11: Comando "spot" inválido.
;L4: Comando inexistente "inputxxx". Erro semântico.
;L5: Comando inexistente "outputxx". Erro semântico.
;L7: Comando inexistente "loadxx". Erro semântico.
;L8: Comando inexistente "laod". Erro semântico.
;L9: Comando inexistente "jnp". Erro semântico.
;L11: Comando inexistente "spot". Erro semântico.

;Erros encontrados: 12
;Arquivo objeto não pôde ser gerado.

