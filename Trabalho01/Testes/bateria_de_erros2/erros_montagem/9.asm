;9– tokens inválidos;
section text
	input	1a
	output	1a
	output	99_constante
	output	_be
	stop
	
section data
	1a:	space
	99_constante:	const 	10
	_be:			const	1


;data do teste: 22/05/2015
;Saida do montador 

;L3: Operando "1a" inválido para instrução "input". Erro léxico.
;L4: Operando "1a" inválido para instrução "output". Erro léxico.
;L5: Operando "99_constante" inválido para instrução "output". Erro léxico.
;L10: Label inválido. Erro léxico.
;L11: Label inválido. Erro léxico.

;Erros encontrados: 5
;Arquivo objeto não pôde ser gerado.
