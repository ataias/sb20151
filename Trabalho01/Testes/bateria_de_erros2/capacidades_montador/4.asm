;4- A diretiva CONST deve aceitar declarações em hexadecimal também
section text
	output	a
	output	b
	output	c
	output	d
	stop
	
section data
	a:	const	8		;8
	b:	const	0x9		;9
	c:	const	0xA		;10
	d:	const	0xa1 	;161

;data do teste: 22/05/2015

;saida do montador

;ok 
