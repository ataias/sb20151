;2- A seção de dados deve vir depois da seção de códigos
section data		;aqui
	a:		space
	
section text		;aqui
	input	a
	output	a
	stop

;data do teste: 22/05/2015

;saida do montador

;mesmo resultado 
;L2: Seção de dados deve vir depois da seção de texto. Erro semântico.
;L5: Seção de texto deve vir antes da seção de dados. Erro semântico.

;Erros encontrados: 2
;Arquivo objeto não pôde ser gerado.
