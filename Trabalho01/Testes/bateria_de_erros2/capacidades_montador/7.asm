;7- O comando COPY deve utilizar uma vírgula entre os operandos (COPY A, B)
section text
	input	a
	copy	a, b	;este copy deve funcionar
	copy	a. b 	;este copy não deve funcionar! '.' entre os operandos
	output	b
	stop
	
section data
	a:	space	1
	b:	space	1

;data do teste: 22/05/2015

;saida do montador

;novo erro detectado: L5: Operando "a.b" inválido para instrução "copy". Erro sintático.

;L5: Número de argumentos para instrução "copy" inválido. Erro sintático.
;L5: Argumento "a." inválido. Erro léxico.
;L5: Operando "a.b" inválido para instrução "copy". Erro sintático.
;
;Erros encontrados: 3
;Arquivo objeto não pôde ser gerado.
