;Entrada com parte real e imaginária de um número complexo
;Saída: quadrante do número complexo
;este programa falha se a entrada for (0,0) ou estiver em um
; dos eixos imaginários

section text
MOD_A: begin
q: extern ;q é o quadrante
mod_b: extern ;função que calcula quadrante
public a ;a é a parte real do número complexo
public b ;b é parte imaginária
public out

;recebe entradas
input a
input b

;mostra as leituras
;output a
;output b

;chama módulo b
jmp mod_b
out: output q ;mostra quadrante
stop

section data
a: space
b: space

end
