;Soma dígitos de um número decimal
section text
	mod_a:	begin
	mod_b:	extern
	public 	out
	public 	zero
	public 	sum
	public 	aux
	public	N
	
	input 	N
	copy  	N, aux
	copy  	zero, sum
	jmp		mod_b

out: 	output  sum
	stop

section data
	zero: 	const 	0
	sum:  	space	1
	aux:  	space	1
	N:    	space	1
	end
