;módulo a chamada este módulo para obter o quadrante q
section text
MOD_B: begin
a: extern ;a é a parte real do número complexo
b: extern ;b é parte imaginária
out: extern
public q ;número do quadrante
public MOD_B

     ;inicializa valor de q
     copy zero, q

     load a
q13: jmpn q23 ;se jump não ocorrer, a > 0
     load b
q1:  jmpn q4
     load um
     store q
     jmp out
q4:  load quatro
     store q
     jmp out
;a < 0
q23: load b
     jmpn q3
q2:  load dois
     store q
     jmp out
q3:  load tres
     store q
     jmp out

section data
zero:   const 0x0
um:     const 0x1
dois:   const 0x2
tres:   const 0x3
quatro: const 0x4
q: space
end
