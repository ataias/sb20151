;Soma dígitos de um número decimal
section text
	mod_b:	begin
	out:	extern
	zero:	extern
	sum:	extern
	aux:	extern
	N:		extern
	public 	mod_b

	loop:	load   N
	jmpz   	out
	div    	dez
	mult   	dez
	store  	aux
	load   	N
	sub    	aux
	add    	sum
	store  	sum
	load   	N
	div    	dez
	store  	N
	jmp    	loop

section data
	dez:  const		10
	end
