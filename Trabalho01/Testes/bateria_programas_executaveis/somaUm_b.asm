section text
	mod_b:	begin
	a:		extern
	l1:		extern		;fator_correcao b = 7
	public	mod_b		; + -> quando os codigos forem concatenados. endereços relativos com o fator de correção.
	load	a			;00 + 7 = 07
	add 	one			;02 + 7 = 09
	store	a			;04 + 7 = 11
	jmp		l1			;06 + 7 = 13
	
section data
	one:	const 	1	;08 + 7 = 15
	end
						;logo, one que estava no endereço 03 em b, vai estar no endereço 10 no codigo total.
						;como checar isso? fazendo uma tabela das variaveis de B em section data e mudar as ocorrências delas
						;no código B aplicando o fator de conversão. No módulo A não é necessário pois o fator de conversao é 0.
						;problema: como achar a section data do modulo_b sendo que não é enviada para o ligador?
						;solução: para cada operando que não está na tabela de uso, aplica-se o fator de correção, pois entende-se
						;que ele é, então, um valor interno do módulo.
						;problema: para contar os operandos da instrução, deve-se fazer uma busca na tabela do Ataias
						;ou fazer outra no código. Ver como fazer essa busca.
