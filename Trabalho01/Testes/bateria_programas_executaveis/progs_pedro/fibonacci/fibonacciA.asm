;Entrada: Um número inteiro positivo
;Saida:	  A sequencia de fibonacci até esse numero
;Alguns exemplos:
;F0 F1 	F2 	F3 	F4 	F5 	F6 	F7 	F8 	F9 	F10 	F11 	F12 	F13 	F14 	F15 	F16 	F17 	F18 	F19 	F20
;0 	1 	1 	2 	3 	5 	8 	13 	21 	34 	55 		89 		144 	233 	377 	610 	987 	1597 	2584 	4181 	6765
;Por exemplo: para a entrada 55, a saida é: 1 1 2 3 5 8 13 21 34 55 55
;o último 55 (repetido) é só o número que foi lido teclado

section text
mod_a:	begin
mod_b:	extern
zero:	extern
one:	extern

public	older
public	old
public	limit
public	final

		copy	zero, older
		copy	one, old	
		input	limit
		output	old
		
		jmp		mod_b
		
final:	output	limit
		stop
	
section data
	older:		space
	old:		space
	limit:		space
	end
