;vai imprimindo a sequencia de fibonacci dentro do loop
section text
mod_b:	begin
older:	extern
old:	extern
limit:	extern
final:	extern

public	mod_b
public 	zero
public 	one

front:	load	older
		add		old
		store	new
		sub		limit
		jmpp	final
		output	new
		copy	old, older		;dando erro nessas duas instruções de COPY
		copy	new, old
		jmp		front
		
section data
	zero:	const	0
	one:	const	1
	new:	space
	end
