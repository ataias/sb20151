;Operações aritmeticas
;Entrada: Um número n inteiro
;Saida:	  f(n) = ( ( ( (n + 3) - 1 ) / 2) * 4 )
;
;Descrição: Apenas um programa pra testar todas as operações aritmeticas
;e também duas transferências de módulo.
;
;no modulo A se faz a soma, a divisão e se imprime o resultado
;no modulo B se faz a subtração e a multiplicação antes de retornar ao A para exibir resultado
;
;Exemplos: F(4)   = 12
;		   F(13)  = 28
;		   F(10)  = 24
;		   F(42)  = 88

section text
mod_a: 		begin
;mod_b:		extern
DOIS:		extern
subtrai:	extern
multiplica:	extern

public 		UM
public		QUATRO
public		n
public		soma
public		divide
public		imprime

			input	n
			load 	n
			
soma:		add		TRES
			jmp		subtrai
			
divide:		div		DOIS
			jmp		multiplica
			
imprime:	output	n
			stop

section data
	n:		space	1
	UM:		const	1
	TRES:	const	3
	QUATRO:	const	4
	end; O programa funcionou sem "end". Acho que deve dar warning
