;Operações aritmeticas
;Entrada: Um número n inteiro
;Saida:	  f(n) = ( ( ( (n + 3) - 1 ) / 2) * 4 )
;
;Descrição: Apenas um programa pra testar todas as operações aritmeticas
;e também duas transferências de módulo.
;
;no modulo A se faz a soma, a divisão e se imprime o resultado
;no modulo B se faz a subtração e a multiplicação antes de retornar ao A para exibir resultado
;
;Exemplos: F(4)   = 12
;		   F(13)  = 28
;		   F(10)  = 24
;		   F(42)  = 88

section text
mod_b: 		begin
UM:			extern
QUATRO:		extern
n:			extern
divide:		extern
imprime:	extern

public		DOIS
public		subtrai
public		multiplica

subtrai:	sub		UM
			jmp		divide
			
			
multiplica:	mult	QUATRO
			store	n
			jmp		imprime

section data
	DOIS:	const	2
	end; O programa funcionou sem "end". Acho que deve dar warning
