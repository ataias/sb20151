;Entrada: 5 números inteiros digitados pelo usuário
;Saída:	  5 números inteiros que são o dobro dos
;		  digitados pelo usuário. 

;Descrição: O programa e recebe 5 números inteiros do usuário pela entrada 
;padrão (teclado) e, ao final, mostra todos os números entrados pelo usuário
;(5 números) multiplicados por 2.

section text
mod_a:	begin
		
		;###Como pegar o indice do vetor vetnum para adicionar o offset cont? ex: input vetnum + contador
		;seria mais compacto e escalável com parametros de loop, mas parece que não dá.
		;Não se tem acesso ao índice do vetor.
		input	vetnum
		input	vetnum+1
		input	vetnum+2
		input	vetnum+3
		input	vetnum+4
		
		load	vetnum
		mult	dois
		store	vetnum
		
		load	vetnum+1
		mult	dois
		store	vetnum+1
		
		load	vetnum+2
		mult	dois
		store	vetnum+2
		
		load	vetnum+3
		mult	dois
		store	vetnum+3
		
		load	vetnum+4
		mult	dois
		store	vetnum+4
	
fim:	output	vetnum
		output	vetnum+1
		output	vetnum+2
		output	vetnum+3
		output	vetnum+4
		stop

section data
	vetnum:	space	5
	dois:	const	2
	end
