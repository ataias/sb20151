;Entrada: 5 números inteiros digitados pelo usuário
;Saída:	  5 números inteiros que são o dobro dos
;		  digitados pelo usuário. 

;Descrição: O programa e recebe 5 números inteiros do usuário pela entrada 
;padrão (teclado) e, ao final, mostra todos os números entrados pelo usuário
;(5 números) multiplicados por 2.

section text
mod_a:	begin
	
;cont e n são variáveis de controle do loop.
;cont começa com 0 e vai até 5.
;a verificação dá load em n (5), subtrai o contador (cont) e checa se
;o resultado é zero. Se for, o loop terminou e se imprime os números
		copy	zero, cont
		load	n
		sub		cont
		
l1:		jmpz	out1
		input	vetnum+cont
		add		vetnum
		load	vetnum
		
		
		;cont++
		load	cont
		add		one
		store	cont
		
		load	n
		sub		cont
		jmp		l1
		

;Aqui o loop de preenchimento terminou e o de print irá iniciar
out1:	copy	cinco, n
		load	n
		copy	
		load	cont
print:	output	vetnum
		

;

section data
	vetnum:	space	5
	cont:	space	1
	zero:	const	0
	n:		const	5
	end
