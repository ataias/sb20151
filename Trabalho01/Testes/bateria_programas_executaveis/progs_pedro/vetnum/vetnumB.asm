;O modulo B apenas multiplica os números do vetor por 2, um a um.
section text
mod_b:	begin
fim:	extern
vetnum:	extern
public mod_b

		load	vetnum
		mult	dois
		store	vetnum
		
		load	vetnum+1
		mult	dois
		store	vetnum+1
		
		load	vetnum+2
		mult	dois
		store	vetnum+2
		
		load	vetnum+3
		mult	dois
		store	vetnum+3
		
		load	vetnum+4
		mult	dois
		store	vetnum+4
		
		jmp		fim

section data
	dois:	const	2
	end
