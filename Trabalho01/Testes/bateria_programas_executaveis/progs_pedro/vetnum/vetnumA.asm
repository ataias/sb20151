;Entrada: 5 números inteiros digitados pelo usuário
;Saída:	  5 números inteiros que são o dobro dos
;		  digitados pelo usuário. 

;Descrição: O programa e recebe 5 números inteiros do usuário pela entrada 
;padrão (teclado) e, ao final, mostra todos os números entrados pelo usuário
;(5 números) multiplicados por 2.

section text
mod_a:	begin
mod_b:	extern
public 	fim
public	vetnum
		
		;###Como pegar o indice do vetor vetnum para adicionar o offset cont? ex: input vetnum + contador
		;seria mais compacto e escalável com parametros de loop, mas parece que não dá.
		;Não se tem acesso ao índice do vetor.
		input	vetnum
		input	vetnum+1
		input	vetnum+2
		input	vetnum+3
		input	vetnum+4
		
		jmp		mod_b
	
fim:	output	vetnum
		output	vetnum+1
		output	vetnum+2
		output	vetnum+3
		output	vetnum+4
		stop

section data
	vetnum:	space	5
	end
