;Nome do programa: Soma de uma PA
;
;Entrada:	um numero inteiro positivo
;Saida:		resultado da soma de todos os inteiros até esse numero
;
;Descrição: O programa calcula o somatório pela fórmula n*(n+1) / 2
;e não por iteração de somas
;
;Exemplos de input -> output:
;3 -> 6
;4 -> 10
;100 -> 5050

section text
mod_a:	begin

;labels externos
mod_b:	extern

;labels públicos
public	n
public	result
public	printSoma

;programa
	input	n
	load	n
	jmpn	LNEG
	jmp		mod_b

printSoma:
	output	result
	jmp		fim

LNEG:
	output	numnegativo
	
fim:
	stop

section data
	result:			space	1
	n:				space	1
	numnegativo:	const	0
	end
