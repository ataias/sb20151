;Nome do programa: Soma de uma PA
;
;Entrada:	um numero inteiro positivo
;Saida:		resultado da soma de todos os inteiros até esse numero
;
;Descrição: O programa calcula o somatório pela fórmula n*(n+1) / 2
;e não por iteração de somas
;
;Exemplos de input -> output:
;3 -> 6
;4 -> 10
;100 -> 5050

section text
mod_b:	begin

;labels externos
n:			extern
result:		extern
printSoma:	extern

;labels públicos
public	mod_b

;programa
	load	n
	add 	um
	mult	n
	div		dois
	store	result
	jmp		printSoma

section data
	um:		const	1
	dois:	const	2
	end
