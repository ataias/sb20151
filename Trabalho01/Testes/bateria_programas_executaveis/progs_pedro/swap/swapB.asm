;Nome: Swap
;
;Entrada: dois números inteiros
;Saída:	dois números inteiros sendo que o primeiro digitado é o último a ser impresso
;
;Exemplos:	1 e 2 -> 2 1
;			99 e 66 -> 66 99

section text
mod_b:	 begin
a:		 extern
imprime: extern

public	 mod_b

		;faz o swap
		load	a
		store 	aux
		load	a+1
		store	a
		load	aux
		store 	a+1

		jmp		imprime

section data
	aux: space 	1
	end
