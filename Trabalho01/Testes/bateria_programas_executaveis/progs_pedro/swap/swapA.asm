;Nome: Swap
;
;Entrada: dois números inteiros
;Saída:	dois números inteiros sendo que o primeiro digitado é o último a ser impresso
;
;Exemplos:	1 e 2 -> 2 1
;			99 e 66 -> 66 99

section text
mod_a:	begin
mod_b:	extern

public	a
public imprime

		input	a
		input	a+1

		jmp		mod_b

imprime: output	a
	     output	a+1
		 stop

section data
	a:	space	2
	end
