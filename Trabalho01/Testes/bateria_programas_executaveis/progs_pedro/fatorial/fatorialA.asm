;Entrada: Um número inteiro positivo
;Saida:	  O fatorial deste numero

;Alguns exemplos:
;Ele só funciona até o 7! Por causa do range de -32768 a 32767
;F0 F1 	F2 	F3 	F4 	F5 		F6 		F7 		F8 		F9 		F10 
;1 	1 	2 	6 	24 	120 	720 	5040 	40320 	362880 	3628800

section text
mod_a:	begin
mod_b:	extern
public	fim
public 	n

		input	n		;n = input
		load	n		;acc = n
		
		jmp 	mod_b
		
fim:	output	n		;imprime
		stop			;termina
	
section data
	n:		space
	end
