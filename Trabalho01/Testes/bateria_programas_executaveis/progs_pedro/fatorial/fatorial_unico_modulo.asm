;Entrada: Um número inteiro positivo
;Saida:	  O fatorial deste numero

;Alguns exemplos:
;Ele só funciona até o 7! Por causa do range de -32768 a 32767
;F0 F1 	F2 	F3 	F4 	F5 		F6 		F7 		F8 		F9 		F10 
;1 	1 	2 	6 	24 	120 	720 	5040 	40320 	362880 	3628800

section text
		input	n		;n = input
		load	n		;acc = n
fat:	sub 	one		;acc = acc - 1
		jmpz	fim		;acc == 0? se sim, fim. Else, continua
		store	aux		;aux = acc
		mult	n		;acc = acc * n
		store	n		;n = acc
		load	aux		;acc = aux(n-1)
		jmp		fat		;volta o loop
		
fim:	output	n		;imprime
		stop			;termina
	
section data
	aux:	space
	n:		space
	one:	const	1
