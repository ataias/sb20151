;o modulo B roda o loop do fatorial

section text
mod_b:	begin
fim:	extern
n:		extern
public	mod_b

fat:	sub 	one		;acc = acc - 1
		jmpz	fim		;acc == 0? se sim, fim e retorna pro modulo A. Else, continua
		store	aux		;aux = acc
		mult	n		;acc = acc * n
		store	n		;n = acc
		load	aux		;acc = aux(n-1)
		jmp		fat		;volta o loop

section data
	aux:	space
	one:	const	1
	end
