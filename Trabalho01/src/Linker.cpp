#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <Table.h>
#include <CmdTable.h>
#include <Linker.h>

using namespace std;

Linker::Linker(std::string modA, std::string modB){
  readFile(modA, &objectA, &useA, &defA, &bitMapRelativeA);
  readFile(modB, &objectB, &useB, &defB, &bitMapRelativeB);

  //tabela geral de definições
  vector<Symbol>::iterator it;
  vector<Symbol> tableGD = defA.getVector();
  for(it = tableGD.begin(); it != tableGD.end(); ++it)
    TGD.push(it->label, it->value);
  vector<Symbol>().swap(tableGD);

  tableGD = defB.getVector();
  for(it = tableGD.begin(); it != tableGD.end(); ++it){
    TGD.push(it->label, it->value + objectA.size());
  }
  nErros = 0;
}

bool Linker::readFile(std::string modFilename, Object *obj, Table *use, Table *def, vector<bool> *bitMapRel){
  string line;
  ifstream file(modFilename);

  Sector sector = UNDEF;

  if (file.is_open()){ //verifica se arquivo foi aberto
    while ( getline (file,line) ){

      //remover \n
      line.erase(remove(line.begin(), line.end(), '\n'), line.end());

      //remover carriage return
      line.erase(remove(line.begin(), line.end(), 13), line.end());

      if(line.empty()) continue;

      if(!line.compare("TABLE USE")){
        sector = USE;
        continue;
      }

      if(!line.compare("TABLE DEFINITION")){
        sector = DEFINITION;
        continue;
      }

      if(!line.compare("CODE")){
        sector = CODE;
        continue;
      }

      if(!line.compare("BIT MAP")){
        sector = BITMAP;
        continue;
      }

      stringstream stream(line);
      string token;

      if(sector == USE){
        string label, addr;
        stream >> label;
        stream >> addr;

        // cout <<  label << " " << addr << endl;
        use->push(label, stoi(addr));
      }

      if(sector == DEFINITION) {
        string label, addr;
        stream >> label;
        stream >> addr;

        // cout <<  label << " " << addr << endl;
        def->push(label, stoi(addr));
      }

      if(sector == CODE) {
        while(stream >> token) {
          obj->push(token, 0);
        }
      }

      if(sector == BITMAP) {
        bool b;
        while(stream >> b) {
          bitMapRel->push_back(b);
        }
      }



    }
    file.close();
  } else { //mensagem de erro se não foi possível abrir arquivo
    cout << "Não foi possível abrir o arquivo." << endl;
    return false;
  }

  return true;

}

void Linker::printInfo(){
  useA.print();
  defA.print();
  objectA.print();
  vector<bool>::iterator it;
  for(it = bitMapRelativeA.begin(); it != bitMapRelativeA.end(); ++it){
    cout << *it << " ";
  }
  cout << endl;

  useB.print();
  defB.print();
  objectB.print();
  for(it = bitMapRelativeB.begin(); it != bitMapRelativeB.end(); ++it){
    cout << *it << " ";
  }
  cout << endl;

  TGD.print();
}

void Linker::link(){
  int value;
  vector<Symbol> use = useA.getVector();
  for(unsigned i=0; i<objectA.size(); i++){
    value = stoi(objectA.get(i));
    if(bitMapRelativeA.at(i)){
      for(unsigned j=0; j<use.size(); j++){
        if((unsigned) use.at(j).value == i){
          try {
            value += TGD.get(use.at(j).label);
          } catch (const char *e) {
            cout << "Variável \"" << use.at(j).label << "\" ";
            cout << "não encontrada na tabela geral de definições." << endl;
            throw "Erro de ligação\n";
          }
        }
      }
    }
    linkedObj.push(to_string(value), 0);
  }

  use = useB.getVector();
  bool set = false;
  for(unsigned i=0; i<objectB.size(); i++){
    value = stoi(objectB.get(i));
    set = false;
    if(bitMapRelativeB.at(i)){
      for(unsigned j=0; j<use.size(); j++){
        if((unsigned) use.at(j).value == i){
          try {
            value += TGD.get(use.at(j).label);
            set = true;
          } catch (const char *e) {
            cout << "Variável \"" << use.at(j).label << "\" ";
            cout << "não encontrada na tabela geral de definições." << endl;
            throw "Erro de ligação\n";
          }
        }
      }
      if(!set) value += objectA.size();
    }
    linkedObj.push(to_string(value), 0);
  }

}

void Linker::printLinkedInfo(){
  linkedObj.print();
}

bool Linker::writeFile(string filename){
  if(nErros){
    cout << endl << "Erros encontrados: " << nErros << endl;
    cout << "Arquivo executável não pôde ser gerado." << endl;
    return false;
  } else {
    //gerar arquivo objeto filename
    ofstream objectFile;
    objectFile.open(filename);
    string aux;

    for(unsigned i=0; i < linkedObj.size(); i++){
      aux = linkedObj.get(i);
      objectFile << aux.c_str() << " ";
    }
  }
  return true;
}
