#include <iostream>
#include <vector>
#include <regex>
#include <CmdTable.h>

using namespace std;

//compilar
//g++ -c -std=c++11 -Wall -Wextra CmdTable.cpp -I ../include

CmdTable::CmdTable(){
  //initialize
  //Adicionar instruções
  t.push_back({"add",    1, 1,  true,  false});
  t.push_back({"sub",    1, 2,  true,  false});
  t.push_back({"mult",   1, 3,  true,  false});
  t.push_back({"div",    1, 4,  true,  false});
  t.push_back({"jmp",    1, 5,  true,  true});
  t.push_back({"jmpn",   1, 6,  true,  true});
  t.push_back({"jmpp",   1, 7,  true,  true});
  t.push_back({"jmpz",   1, 8,  true,  true});
  t.push_back({"copy",   2, 9,  false, false});
  t.push_back({"load",   1, 10, true,  false});
  t.push_back({"store",  1, 11, true,  false});
  t.push_back({"input",  1, 12, true,  false});
  t.push_back({"output", 1, 13, true,  false});
  t.push_back({"stop",   0, 14, false,  false});

  //Adicionar diretivas
  //opcode 0 indica que é diretiva
  t.push_back({"section",  1, 0, false, false});
  t.push_back({"space",    1, 0, false, false});
  t.push_back({"const",    1, 0, false, false});
  t.push_back({"public",   1, 0, false, false});
  t.push_back({"extern",   0, 0, false, false});
  t.push_back({"begin",    0, 0, false, false});
  t.push_back({"end",      0, 0, false, false});
}

void CmdTable::print(){
  cout << "Mnemonic\t #Operands\t Opcode\n";
  for (unsigned i=0; i<t.size(); i++)
    cout << t[i].mnemonic << "\t\t" << t[i].operands << "\t\t" << t[i].opcode << endl;
}

//get - retorna estrutura do comando a partir de seu mnemônico
Cmd CmdTable::get(string command){
  bool found=false;
  unsigned pos = -1;
  for (unsigned i=0; i<t.size(); i++)
    if(t[i].mnemonic.compare(command)==0) {
      found = true;
      pos = i;
      break;
    }
    if (!found) {
      throw "Comando não encontrado\n";
    }
  return t[pos];
}

//get - retorna estrutura do comando a partir de seu opcode
Cmd CmdTable::get(unsigned opcode){
  bool found=false;
  unsigned pos = -1;
  for (unsigned i=0; i<t.size(); i++)
    if(t[i].opcode==opcode) {
      found = true;
      pos = i;
      break;
    }
    if (!found) {
      throw "Comando não encontrado\n";
    }

    if (opcode<=0 || opcode >14){
      throw "Opcode inválido\n";
    }
  return t[pos];
}

//método que verifica se a string é um comando válido
bool CmdTable::isCmd(string l){
  for (unsigned i=0; i<t.size(); i++)
    if(t[i].mnemonic.compare(l)==0)
      return true;
  return false;
}

bool CmdTable::isCmdType1(std::string l){
  for (unsigned i=0; i<t.size(); i++)
    if(!t[i].mnemonic.compare(l))
      return t[i].type1;
  return false;
}

bool CmdTable::isJump(std::string l){
  for (unsigned i=0; i<t.size(); i++)
    if(!t[i].mnemonic.compare(l))
      return t[i].jump;
  return false;
}
