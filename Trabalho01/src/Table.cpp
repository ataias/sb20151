#include <iostream>
#include <vector>
#include <Table.h>

//compilar
//g++ -c -std=c++11 -Wall -Wextra Table.cpp -I ../include

using namespace std;

Table::Table(const char* error, string table_name){
  this->error = error;
  this->table_name = table_name;
}

void Table::push(std::string label, int value, bool Extern){
  t.push_back({label, value, Extern});
}

void Table::push(std::string label, int value){
  t.push_back({label, value, false});
}

void Table::print(){
  std::cout << std::endl << table_name << std::endl;
  for (unsigned i=0; i<t.size(); i++)
    cout << t[i].label << "\t " << t[i].value << endl;
}

//pega uma linha da tabela de símbolos ou definição ou uso
unsigned Table::get(string label){
  bool found=false;
  unsigned pos = -1;
  for (unsigned i=0; i<t.size(); i++)
    if(t[i].label.compare(label)==0) {
      found = true;
      pos = i;
      break;
    }
    if (!found) {
      throw error;
    }
  return t[pos].value;
}

Symbol Table::getS(std::string label){
  int value = 0;
  try {
    value = get(label);
  } catch (const char* e){
    cout << "Símbolo definido como public \"" << label << "\" não foi encontrado." << endl;
    throw "Símbolo public não definido\n";
  }
  return {label, value, false};
}

//return true se label foi encontrada e pode modificar seu valor
bool Table::set(string label, int value){
  for (unsigned i=0; i<t.size(); i++)
    if(t[i].label.compare(label)==0) {
      t[i].value = value;
      return true;
    }
  return false;
}

vector<Symbol> Table::getVector(){
  return t;
}
