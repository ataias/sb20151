/*
 * Alunos:
 * - Ataias Pereira Reis - 10/0093817
 * - Pedro Paulo Struck Lima - 11/0073983
 * ---------------------------------------
 * montador.cpp
 *
 * Formato do input: arquivo texto ASCII com assembly inventado de Software Básico
 *
 * Input: (por linha de comando). Nomes de dois arquivos: Um de entrada e um de saída.
 * 		 ./montador ../triangulo.asm ../triangulo.o
 *
 * Output: Arquivo montador em formato TEXTO (na extensão escolhida pelo usuário, como .o)
 *       Caso sejam utilizadas diretivas public e extern, tabela de uso e definição
 *       são incluídas no objeto
 *
 * Compilar: make montador #de dentro da pasta src
 *
 * argv[0]: ./montador
 * argv[1]: entrada.asm
 * argv[2]: saida.o
 *
 * */

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <sstream>
#include <cstdint>
#include <regex>
#include <Table.h>
#include <CmdTable.h>
#include <Assembler.h>

using namespace std;

//Função de pré-processamento
//Retira caracteres indesejados, como tabs repetidos e espaços
vector<string> preprocess(string filename){
  string line;
  ifstream file(filename);
  vector<string> code;
  unsigned i = 0;

  if (file.is_open()){
    while ( getline (file,line) ){
      //trocar todos os tabs por espaços
      replace(line.begin(), line.end(), '\t', ' ');

      //troca "," por " ,"
      i=0;
      while(i<line.length()){
        if(line[i]==',') {
          line.replace(i, 1, " , ");
          i+=3;
        } else {
          i++;
        }
      }

      //troca ":" por " :"
      i=0;
      while(i<line.length()){
        if(line[i]==':') {
          line.replace(i, 1, " :");
          i+=2;
        } else {
          i++;
        }
      }

      //troca "+" por " + "
      i=0;
      while(i<line.length()){
        if(line[i]=='+') {
          line.replace(i, 1, " + ");
          i+=3;
        } else {
          i++;
        }
      }

      //troca "-" por " - "
      i=0;
      while(i<line.length()){
        if(line[i]=='-') {
          line.replace(i, 1, " - ");
          i+=3;
        } else {
          i++;
        }
      }

      //troca caracteres em maiúscula para minúscula e remove comentarios
      i=0;
      while(line[i] != '\0'){
        if(line[i] <= 0x5A /*Z*/ && line[i] >= 0x41 /*A*/)
          line[i] += 0x20;

        //0x3B = ; (ignorar tudo o que tiver depois de ;)
        if(line[i] == 0x3B){
          line.erase((size_t) i, (size_t) (line.length()-i));
          break;
        }
        i++;
      }

      //remover espaços repetidos no meio do programa
      //Ideia para remover espaços adjacentes: http://goo.gl/JmRvYG
      //ideia de função lambda: http://goo.gl/HfIQDF
      string::iterator new_end = unique(line.begin(), line.end(),[](char a, char b) { return a == ' ' && b == ' '; } );
      line.erase(new_end, line.end());

      //remover espaço do início da linha, se houver
      if(line[0]==' ')  line.erase(0, (size_t) 1);

      //remover \n
      line.erase(remove(line.begin(), line.end(), '\n'), line.end());

      //remover carriage return
      line.erase(remove(line.begin(), line.end(), 13), line.end());

      //adicionar linha pré-processada ao vetor<string> code
      code.push_back(line);
    }
    file.close();
  }

  else cout << "Não foi possível abrir o arquivo." << endl;
  return code;
}

void modoDeUsar(){
  cout << endl << "Modo de usar: " << endl;
  cout << endl << "./montador entrada.asm saida.o" << endl;
}


int main(int argc, char* argv[]){

  if(argc < 3){
    modoDeUsar();
    return 0;
  }

  string asmfile = argv[1];
  string objfile = argv[2];

  vector<string> code = preprocess(argv[1]);

  try {
    Assembler assemble(code);
    assemble.pass1();
    assemble.pass2();
    assemble.writeFile(argv[2]);
  } catch (const std::regex_error& e){
    cout << "Erro capturado: " << e.what() << endl;
    cout << "Provavelmente o compilador usado não suporta totalmente regex do C++11." << endl;
    cout << "Utilize g++ a partir da versão 4.9 ou clang++ a partir da versão 3.6." << endl;
  }
  return(0);
}
