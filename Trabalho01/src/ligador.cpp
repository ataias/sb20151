#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <sstream>
#include <cstdint>
#include <regex>
#include <Table.h>
#include <CmdTable.h>
#include <Linker.h>

using namespace std;

void modoDeUsar(){
  cout << endl << "Modo de usar: " << endl;
  cout << endl << "./ligador ../moda.o ../modb.o ../mod.e" << endl;
}

int main(int argc, char* argv[]) {

  if(argc < 4){
    modoDeUsar();
    return 0;
  }

  try {
    Linker ligacao = Linker(argv[1], argv[2]);
    ligacao.link();
    ligacao.writeFile(argv[3]);

    #if __NOTRELEASE
      ligacao.printInfo();
      ligacao.printLinkedInfo();
    #endif
  } catch (char const *e) {
    cout << "Erro de ligação. Verificar programas assembly." << endl;
  }
}
