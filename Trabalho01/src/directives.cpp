//Diretiva "section"
if(!spline.operation.compare("section")){
  //para posterior comparação, salva seção atual em "old"

  Section old = cSection;
  //Verificação do argumento da diretiva "section" --------
  if(!spline.op1[0].compare("text"))
    cSection = TEXT;
  else if(!spline.op1[0].compare("data")){
    cSection = DATA;
    object.setDataInit(*pc);
  }
  else {
    cout << "L" << l << ": Seção \"" << spline.op1[0] << "\" inválida. ";
    cout << "Erro sintático." << endl;
    nErros++;
  }
  //------------------------------------------------------

  //se houver 0 ou 2 argumentos, imprime esse erro

  if(!spline.op2.empty() || spline.op1.empty() || spline.op1.size() > 1){
    cout << "L" << l << ": Número de argumentos para \"section\" inválido. ";
    cout << "Erro sintático." << endl;
    nErros++;
  }

  printLex(spline, l, validtoken);

  //tentativa de definir seção de novo
  if((old == cSection) && (cSection != UNDEF)){
    cout << "L" << l << ": Seção duplicada. ";
    cout << "Erro semântico." << endl;
    nErros++;
  }

  //Tentativa de definir seção de dados antes da seção de texto
  if(old == UNDEF && cSection == DATA){
    cout << "L" << l << ": Seção de dados deve vir depois da seção de texto. ";
    cout << "Erro semântico." << endl;
    nErros++;
  }

  if(old == DATA && cSection == TEXT){
    cout << "L" << l << ": Seção de texto deve vir antes da seção de dados. ";
    cout << "Erro semântico." << endl;
    nErros++;
  }
  return;
}

//Diretiva "space"
if(!spline.operation.compare("space")){
  //argumento de space é um inteiro sem sinal de + ou -.
  //esqueci de considerar que o space pode não ter argumento, no momento precisa de argumento mesmo para o caso de operando igual a 1
  int reserve = 1;
  regex num ("\\d+");
  regex sign ("[\\+-]");
  unsigned numpos = 0;

  if(!spline.op1.empty()){

    if(!spline.op1.at(0).compare("+")){
      reserve = 1;
      numpos = 1;
    } else if(!spline.op1.at(0).compare("-")) {
      reserve = -1;
      numpos = 1;
    }

    if(regex_match(spline.op1.at(numpos), num)){
      reserve *= stoi(spline.op1.at(numpos));
    } else {
      cout << "L" << l << ": Argumento de space inválido. ";
      cout << "Erro sintático." << endl;
      nErros++;
    }

    if(spline.op1.size() > (1+numpos) || !spline.op2.empty()){
      cout << "L" << l << ": Número de argumentos de space inválido. ";
      cout << "Erro sintático." << endl;
      nErros++;
    }


  }

  printLex(spline, l, validtoken);

  //no caso de space utilizado com argumento igual a zero, uma opção é só ignorar...
  //não sei se isso é um erro que justifique não compilar... enfim...
  if(reserve <= 0){
    cout << "L" << l << ": Space utilizado com argumento inválido. ";
    cout << "Erro sintático." << endl;
    nErros++;
  }

  //space deve ser usado na seção de dados
  if(cSection == TEXT){
    cout << "L" << l << ": Space utilizado em seção errada. " ;
    cout << "Erro semântico." << endl;
    nErros++;
  }

  if(cSection == UNDEF){
    cout << "L" << l << ": Uso de diretiva \"space\" em seção não definida. " ;
    cout << "Erro semântico." << endl;
    nErros++;
  }

  if(spline.label.empty()){
    cout << "L" << l << ": Uso de diretiva \"space\" sem label " ;
    cout << "Erro sintático." << endl;
    nErros++;
  } else {
		//adiciona nome da variável e seu tamanho, para poder verificar erro de espaço não reservado
    if(reserve > 0)
	   variableScope[*pc] = (Var) {spline.label, (unsigned) reserve, l};
  }

  //fazer um push aqui no código, com zeros
  if(reserve>0){
    *pc += reserve;
    do {
      object.push("0", l);
      bitMapRelative.push_back(false);
    } while (--reserve);
  }
  //--------------------------------------

  return;
}

//Diretiva "const"
if(!spline.operation.compare("const")){
  //o operando op1, assim como op2, são vetores de strings
  //assim, pode-se ter como op1 "+", "0x40", ou "-", "30", ou até "24" (um elemento)
  //arg é uma variável para juntar esses elementos em uma string
  string arg;
  vector<string>::iterator sp;
  for(sp = spline.op1.begin(); sp != spline.op1.end(); ++sp)
    arg.append(sp->c_str());

  int constV = 1;
  unsigned numpos = 0;

  if(!spline.op1.empty()){
    if(!spline.op1.at(0).compare("+")){
      numpos = 1;
    } else if(!spline.op1.at(0).compare("-")) {
      numpos = 1;
      constV = -1;
    }

    regex hex ("([\\+-]?)0[xX]([a-f0-9]+)");
    regex num ("[\\+-]?\\d+");

    if(spline.op1.size() > numpos) {
      if(regex_match(spline.op1.at(numpos),hex)){
          constV *= stoi(spline.op1.at(numpos), NULL, 16);
      } else if(regex_match(spline.op1.at(numpos), num)) {
          constV *= stoi(spline.op1.at(numpos));
      } else {
        cout << "L" << l << ": Constante \"" << spline.op1.at(numpos) << "\" inválida. ";
        cout << "Erro léxico." << endl;
        nErros++;
      }
    } else {
      cout << "L" << l << ": Constante \"" << arg << "\" inválida. ";
      cout << "Erro léxico." << endl;
      nErros++;
    }

    if(spline.op1.size() > (1+numpos) || !spline.op2.empty()){
      cout << "L" << l << ": Número de argumentos de const inválido. ";
      cout << "Erro sintático." << endl;
      nErros++;
    }
  } else {
    cout << "L" << l << ": Número de argumentos de const inválido. ";
    cout << "Erro sintático." << endl;
    nErros++;
  }

  regex constReg ("[\\+-]|([\\+-]?)((0x)?[0-9a-f]+)");
  printLex(spline, l, constReg);

  if(spline.label.empty()){
    cout << "L" << l << ": Uso de diretiva \"const\" sem label. ";
    cout << "Erro sintático." << endl;
    nErros++;
  } else {
		variableScope[*pc] = (Var) {spline.label, 1, l};
  }

  //const deve ser usado na seção de dados
  if(cSection == TEXT){
    cout << "L" << l << ": Const utilizado em seção errada. " ;
    cout << "Erro semântico." << endl;
    nErros++;
  }

  if(cSection == UNDEF){
    cout << "L" << l << ": Uso de diretiva \"const\" em seção não definida. " ;
    cout << "Erro semântico." << endl;
    nErros++;
  }

  *pc += 1; //soma 1 à posição atual
  object.push(to_string(constV), l, true);
  bitMapRelative.push_back(false);
  // --------------------------------------------------
  return;
}

//Diretiva "public"
if(!spline.operation.compare("public")){
  toBeLinked = true;
  //public utilizada na seção texto, antes de qualquer operação, mas depois da definição de módulo
  if(!isModule){ //se módulo não definido
    cout << "L" << l << ": Diretiva public utilizada fora de módulo." ;
    cout << " Erro semântico." << endl;
    nErros++;
  }

  if(spline.op1.empty()){
    cout << "L" << l << ": Diretiva public utilizada sem argumento." ;
    cout << " Erro sintático." << endl;
    nErros++;
  }

  //se 2 ou mais argumentos, imprime erro
  if(!spline.op2.empty() || spline.op1.size() > 1){
    cout << "L" << l << ": Número de argumentos para diretiva\"" << spline.operation << "\" inválido.";
    cout << " Erro sintático." << endl;
    nErros++;
  }

  //tem de verificar se a seção é de texto e se *pc == 0.
  if(*pc!=0){
    cout << "L" << l << ": Diretiva \"public\" utilizada depois da primeira instrução.";
    cout << " Erro semântico." << endl;
    nErros++;
  }

  printLex(spline, l, validtoken);

  //push na tabela de definições
  def.push(spline.op1[0], 0);
  //----------------------------
}

//Diretiva "extern"
if(!spline.operation.compare("extern")){
  toBeLinked = true;

  if(!isModule){ //se módulo não definido
    cout << "L" << l << ": Diretiva extern utilizada fora de módulo." ;
    cout << " Erro semântico." << endl;
    nErros++;
  }

  //diretiva extern deve ser utilizada na seção de texto
  if(cSection == DATA){
    cout << "L" << l << ": Diretiva \"" << spline.operation << "\" utilizada em seção errada.";
    cout << " Erro semântico." << endl;
    nErros++;
  }

  if(cSection == UNDEF){
    cout << "L" << l << ": Diretiva \"" << spline.operation << "\" utilizada em seção indefinida.";
    cout << " Erro semântico." << endl;
    nErros++;
  }

  //se houver 1 ou 2, imprime erro
  if(!spline.op1.empty()){
    cout << "L" << l << ": Número de argumentos para diretiva\"" << spline.operation << "\" inválido.";
    cout << " Erro sintático." << endl;
    nErros++;
  }

  //finalmente, vamos adicionar a label num vetor que depois será utilizado para criar a tabela de usa, juntamente com a tabela de símbolos
  if(spline.label.empty()){
    cout << "L" << l << ": Label não utilizada com diretiva \"" << spline.operation << "\". ";
    cout << "Erro sintático." << endl;
    nErros++;
  } else {
    externvars.push_back(spline.label);
  }

  printLex(spline, l, validtoken);


  return;
}

//diretiva "begin"
if(!spline.operation.compare("begin")){
  if(endModule) {
    cout << "L" << l << ": Diretiva \"begin\" utilizada após término do módulo. ";
    cout << "Erro semântico." << endl;
    nErros++;
  }

  printLex(spline, l, validtoken);
  //begin deve ser utilizada na seção de texto
  if(cSection == DATA){
    cout << "L" << l << ": Diretiva \"" << spline.operation << "\" utilizada em seção errada.";
    cout << " Erro semântico." << endl;
    nErros++;
  }

  if(cSection == UNDEF){
    cout << "L" << l << ": Diretiva \"" << spline.operation << "\" utilizada em seção indefinida.";
    cout << " Erro semântico." << endl;
    nErros++;
  }

  //se houver 1 argumento ou mais, imprimir erro
  if(!spline.op1.empty()){
    cout << "L" << l << ": Número de argumentos para diretiva \"" << spline.operation << "\" inválido.";
    cout << " Erro sintático." << endl;
    nErros++;
  }

  if(isModule){
    cout << "L" << l << ": Arquivo só pode ter um módulo. ";
    cout << " Erro semântico." << endl;
    nErros++;
  }

  isModule = true;
  return;
}

//diretiva "end"
if(!spline.operation.compare("end")){
  if(!isModule) {
    cout << "L" << l << ": Diretiva \"end\" utilizada sem módulo definido. ";
    cout << "Erro semântico." << endl;
    nErros++;
  }

  printLex(spline, l, validtoken);
  //end deve ser utilizada na seção de dados
  if(cSection == TEXT){
    cout << "L" << l << ": Diretiva \"" << spline.operation << "\" utilizada em seção errada.";
    cout << " Erro semântico." << endl;
    nErros++;
  }

  if(cSection == UNDEF){
    cout << "L" << l << ": Diretiva \"" << spline.operation << "\" utilizada em seção indefinida.";
    cout << " Erro semântico." << endl;
    nErros++;
  }

  //se houver 1 argumento ou mais, imprimir erro
  if(!spline.op1.empty()){
    cout << "L" << l << ": Número de argumentos para diretiva \"" << spline.operation << "\" inválido.";
    cout << " Erro sintático." << endl;
    nErros++;
  }

  endModule = true;

  return;
}
