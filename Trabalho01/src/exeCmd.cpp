
void Assembler::printLex(Elements spline, unsigned l, regex tokentype){
  //verificar erro léxico em argumentos
  vector<string>::iterator it;
  if(!spline.op1.empty()){
    for(it = spline.op1.begin(); it != spline.op1.end(); ++it){
      if(!regex_match(*it, tokentype)){
        cout << "L" << l << ": Argumento \"" << *it << "\" inválido. ";
        cout << "Erro léxico." << endl;
        nErros++;
      }
    }
  }

  if(!spline.op2.empty()){
    for(it = spline.op2.begin(); it != spline.op2.end(); ++it){
      if(!regex_match(*it, tokentype)){
        cout << "L" << l << ": Argumento \"" << *it << "\" inválido. ";
        cout << "Erro léxico." << endl;
        nErros++;
      }
    }
  }

  if(!spline.opN.empty()){
    for(it = spline.opN.begin(); it != spline.opN.end(); ++it){
      if(!regex_match(*it, tokentype)){
        cout << "L" << l << ": Argumento \"" << *it << "\" inválido. ";
        cout << "Erro léxico." << endl;
        nErros++;
      }
    }
  }
}
//Este método irá analisar uma linha e executar a diretiva ou instrução existente nela
//caso seja primeira passagem, essa função só mexe nas tabelas
//caso seja segunda passagem, só haverá criação do código objeto com base nas tabelas já criadas
void Assembler::exeCmd(Elements spline, unsigned l, int *pc){

  #include "directives.cpp"
  #include "instructions.cpp"

}
