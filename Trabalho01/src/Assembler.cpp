/*
 * Alunos:
 * - Ataias Pereira Reis - 10/0093817
 * - Pedro Paulo Struck Lima - 11/0073983
 * ---------------------------------------
 * Assembler.cpp
 *
 * Métodos da classe Assembler, largamente utilizada no projeto
 * Contém métodos para primeira e segunda passagem, juntamente com as verificações de erros
 * A lista de métodos está disponível no arquivo Assembler.h no diretório include/
 *
 * */

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <sstream>
#include <cstdint>
#include <regex>
#include <map>
#include <Table.h>
#include <CmdTable.h>
#include <Assembler.h>
#include <set>
#include <cctype>
#include <Object.h>

using namespace std;

//compilar
//g++ -c -std=c++11 -Wall -Wextra Assembler.cpp -I ../include

//construtor da Classe
Assembler::Assembler(vector<string> code){
  this->code = code;

  try {

    //define o formato de uma label do assembly inventado
    regex labelsyntax ("[a-z_]([a-z0-9_]*) :.*");
    this->labelsyntax = labelsyntax;

    //define o formato de um token valido do assembly inventado
    regex tokensyntax ("[a-z_]([a-z0-9_]*)");
    this->tokensyntax = tokensyntax;

    regex opsyntax ("\\b([a-z_](\\w*)( \\+ [0-9]+)?)");
    this->opsyntax = opsyntax;

    regex validtoken ("([a-z_][a-z0-9_]*)|([,:+-])|((0x)?[0-9]+)");
    this->validtoken = validtoken;

  } catch (const regex_error& e) {
    cout << "Erro capturado: " << e.what() << endl;
    cout << "Provavelmente o compilador usado não suporta totalmente regex do C++11." << endl;
    cout << "Utilize g++ 4.9, clang++ 3.6 ou superior." << endl;
  }

  //splitted é criada no método split
  vector<Elements> splitted;
  this->splitted = splitted;

}

//split deve separar as linhas do pré-processamento em
//rótulo, operação, operando1, operando2, operandoN
//line é a string da linha, l é o número da linha
void Assembler::split(string line, unsigned l){
  stringstream stream(line);
  string token = ""; //auxiliary variable

  //Parse label
  //verifica se há label, se houver, salva na string label
  string label = "";
  regex colon(":");
  smatch m;
  int nlabel = 0;

  string str_op;

  try {
    str_op = line.substr(stream.tellg(),line.size());
    //regex search verifica se há algum sinal de dois pontos antes de consumir tokens
    if(regex_search (str_op,m,colon)){
      while(token.compare(":")){
        stream >> token;
        if(token.compare(":"))
          label.append(token.c_str());
      }
      if(cmdTable.isCmd(label)){
        cout << "L" << l << ": Label não pode ser uma palavra reservada. ";
        cout << "Erro léxico." << endl;
        nErros++;
      }
      nlabel++;
    }
  } catch (out_of_range e){
    ;
  }

  //verifica se há mais labels definidas, o que é um erro
  try {
    str_op = line.substr(stream.tellg(),line.size());
    while(regex_search (str_op,m,colon)){
      do{
        stream >> token;
        if(cmdTable.isCmd(token)){
          cout << "L" << l << ": Label não pode ser uma palavra reservada. ";
          cout << "Erro léxico." << endl;
          nErros++;
        }
      } while(token.compare(":"));
      str_op = line.substr(stream.tellg(),line.size());
      nlabel++;
    }
  } catch (out_of_range e) {
    ;
  }

  if(nlabel > 1) {
    cout << "L" << l << ": Mais de um rótulo definido na mesma linha. ";
    cout << "Erro sintático." << endl;
    nErros++;
  }

  //Parse operation
  //verifica se há operação, se houver, salva na string operation
  string operation;
  if(stream >> operation){
    if(!cmdTable.isCmd(operation)){
      cout << "L" << l << ": Comando \"" << operation << "\" inválido." << endl;
      nErros++;
    }
  }

  //vector<string> salva operandos no vetor
  //exemplo: "r + 19" será salvo como três string nesse vector -> "r", "+" e "19".
  //se houver um segundo operando, isto estará em outro vetor de string
  const int n = 1000;
  vector<string> op[n];
  vector<string> opN;

  //este while separa operandos divididos por vírgulas para vetores específicos
  int i = 0;
  while(stream >> token){
    if(!token.compare(",")) { //conta número de vírgulas
      i++;
      continue;
    }
    op[i].push_back(token);
  }

  //os dois primeiros operandos são tratados em vector<string>'s  op1 e op2
  //os outros, são todos jogados em opN neste while abaixo
  unsigned k = 2;
  vector<string>::iterator it;
  while(!op[k].empty()){
    opN.push_back("");
    for(it = op[k].begin(); it != op[k].end(); ++it){
      opN[k-2].append(it->c_str());
    }
    k++;
  }

  //analisa número de vírgulas de acordo com o número de operandos
  //dois operandos precisam de uma vírgula para o copy
  //se houver um número diferente, dará erro
  int commas = i;
  int args = 0;
  vector<string>::iterator vs;
  for(int i = 0; i < n; i++)
    if(!op[i].empty())
      args++;

  if(args != (commas+1) && (args >= 1) && commas > 0){
    cout << "L" << l << ": Quantidade de vírgulas incorreta. ";
    cout << "Erro sintático." << endl;
    nErros++;
  }

  splitted.push_back({label, operation, op[0], op[1], opN});
}

//imprime na tela os operandos separados
void Assembler::printSplitted(){
  vector<elements>::iterator sp; unsigned l =0;
  for(sp = splitted.begin(); sp != splitted.end(); ++sp, ++l){
    cout << "L" << (l+1) << ":";
    if(!sp->label.empty()) cout << " L\"" << sp->label << "\"";
    if(!sp->operation.empty()) cout << " Cmd\"" << sp->operation << "\"";
    if(!sp->op1.empty()){
      cout << " Op1\"";
      for(unsigned j=0; j<sp->op1.size(); j++)
        cout << sp->op1[j];
      cout << "\"";
      }

    if(!sp->op2.empty()){
      cout << " Op2\"";
      for(unsigned j=0; j<sp->op2.size(); j++)
        cout << sp->op2[j];
      cout << "\"";
    }

    unsigned i = 0;
    if(!sp->opN.empty())
    try{
      while(!sp->opN.at(i).empty()){

          cout << " Op" << to_string(i+3) << "\"";
          cout << sp->opN.at(i);
          cout << "\"";
          i = i + 1;
      }
    } catch (out_of_range e) {
      ;
    }

    cout << endl;
  }
}

//Imprimir erros de variáveis não declaradas
void Assembler::printUndeclared(set<string> uLabels){
  vector<elements>::iterator sp; unsigned l = 0;
  set<string>::iterator ul;
  for(sp = splitted.begin(); sp != splitted.end(); ++sp, ++l){

    if(!sp->operation.empty()) {
      if(!sp->op1.empty()){
        for(ul = uLabels.begin(); ul != uLabels.end(); ++ul)
          if(!sp->op1[0].compare(*ul)){
            cout << "L" << (l+1) << ": Label indefinida \"" << sp->op1[0] << "\". ";
            cout << "Erro semântico." << endl;
            nErros++;
          }
      }

      if(!sp->op2.empty()){
        for(ul = uLabels.begin(); ul != uLabels.end(); ++ul)
          if(!sp->op2[0].compare(*ul)){
            cout << "L" << (l+1) << ": Label indefinida \"" << sp->op2[0] << "\". ";
            cout << "Erro semântico." << endl;
            nErros++;
          }
      }

    }

  }
}

//Primeira passagem
//gera tabela de símbolos, tabela de definições e tabela de uso...
void Assembler::pass1(){
  for(unsigned l=0; l<code.size(); l++){
    split(code[l], l+1);
  }

  #if __NOTRELEASE
  printSplitted(); cout << endl;
  #endif

  createSymTable();
  createDefTable();
  createUseTable();

  //informações úteis na tela do terminal quando estiver sendo debugado
  #if __NOTRELEASE
  sym.print();
  def.print();
  use.print();
  #endif

}

//Segunda passagem
//deve gerar código objeto
void Assembler::pass2(){

  //imprimir código objeto antes da troca das labels
  #if __NOTRELEASE
    object.print();
  #endif

  //verificar se módulo foi terminado corretamente
  if(isModule && !endModule){
    cout << "Warning: " << "Módulo iniciado mas não terminado com \"end\"" << endl;
  }

  //trocar labels pelos valores de endereço
  vector<Symbol> symv = sym.getVector();
  vector<Symbol>::iterator symbol;
  smatch m;
  string address;
  for(symbol = symv.begin(); symbol != symv.end(); ++symbol){
    string labelReg = "\\b";
    labelReg.append(symbol->label.c_str()).append("\\b");
    regex var (labelReg);
    for(unsigned i = 0; i < object.size(); i++){
      if(regex_search(object.get(i),m,var)){
        address = object.get(i);
        unsigned sz = m[0].str().size();
        address.replace(0, sz, to_string(symbol->value));
        object.set(address, i);
      }
    }
  }

  //Verificar labels que não foram definidos
  set<string> uLabels;
  for(unsigned i = 0; i <object.size(); i++){
    if(regex_search(object.get(i), m, tokensyntax)){
      uLabels.insert(object.get(i));
    }
  }


  //agora que se tem a variável uLabels com variáveis não declaradas,
  //deve-se imprimir a linha onde são utilizadas
  printUndeclared(uLabels);

  //esta seção soma endereços, por exemplo, "0+1"
  regex sum ("\\d+\\+\\d+");
  regex num ("\\d+");
  Var varScope;
  for(unsigned i = 0; i < object.size(); i++){
    if(regex_match(object.get(i), sum)){
      regex_search(object.get(i),m,num);
      string num1 = m[0].str();
      string num2 = object.get(i).erase(0,num1.size());
      int addr = stoi(num1) + stoi(num2);
      //verificação de erro out_of_scope
      try {
        varScope = variableScope.at(stoul(num1));
        if(!(varScope.size > stoul(num2))) {
          cout << "L" << object.getLine(i) << ": Tentativa de acesso a endereço não reservado ";
          cout << "para variável \"" << varScope.label << "\" definida em L";
          cout << varScope.line;
          cout << " do arquivo. Erro semântico." << endl;
          nErros++;
        }
      } catch (out_of_range e){
        ;
      }

      object.set(to_string(addr), i);
    }
  }

  //necessário saber início da seção de dados para funções semânticas abaixo

  //descobrir início da seção de dados
  unsigned dataInit = 0;
  try { //tenta obter início da seção de dados
    dataInit = object.getDataInit();
    verifyDivision(dataInit);
    constCheck(dataInit);
    memoryCheck(dataInit);
    jumpCheck(dataInit);
  } catch (const char *e){
    //caso não tenha sido bem sucedida a tentativa,
    //não há seção de dados para ser analisada.
    return;
  }

  //liberar memória
  symv.clear();
  symv.shrink_to_fit();

  //código objeto depois da substituição
  #if __NOTRELEASE
    object.print();
  #endif
}

//gerar tabela de símbolos
void Assembler::createSymTable(){
  int pc = 0; //position counter
  cSection = UNDEF; //inicialmente, seção indefinida

  vector<elements>::iterator sp; unsigned l = 1;
  for(sp = splitted.begin(); sp != splitted.end(); ++sp, ++l){
    if(!sp->label.empty()){ //se houver uma label nesta linha
      try { //tente obtê-la na tabela de símbolos
        sym.get(sp->label); //emite exceção caso não exista
        cout << "L" << l << ": Label redefinido. ";
        cout << "Erro semântico." << endl;
        nErros++;
        //aumente o número de erros semânticos, label redefinido
      } catch (const char* e) { //símbolo não definido, hora de definir
        sym.push(sp->label, pc);
        if(!regex_match(sp->label, tokensyntax)){
          cout << "L" << l << ": Label inválido. ";
          cout << "Erro léxico." << endl;
          nErros++;
        }
      }
    }
    exeCmd(*sp, l, &pc);
  }
}

//gerar tabela de definições
void Assembler::createDefTable(){
  vector<Symbol> symv = def.getVector();
  vector<Symbol>::iterator symbol;
  for(symbol = symv.begin(); symbol != symv.end(); ++symbol){
    try {
      Symbol defsymbol = sym.getS(symbol->label);
      def.set(defsymbol.label, defsymbol.value);
      if(defsymbol.Extern) cout << "Erro semântico. Símbolo definido como public e extern." << endl;
    } catch (const char* e) {
      cout << "Erro semântico. Símbolo \"" << symbol->label << "\" definido como public não foi definido." << endl;
    }
  }
  //liberar memória
  symv.clear();
  symv.shrink_to_fit();
}

//gerar tabela de uso
void Assembler::createUseTable(){
  vector<Symbol> symv = sym.getVector();
  vector<string>::iterator evar;
  smatch m;
  for(evar = externvars.begin(); evar != externvars.end(); ++evar){
    string regvar = "\\b";
    regex var (regvar.append(evar->c_str()).append("\\b"));
    for(unsigned i = 0; i < object.size(); i++)
        if(regex_search(object.get(i),m,var))
          use.push(m[0].str(), i, true);
    try {
      use.get(evar->c_str());
    } catch (const char* e) {
      cout << "Warning: Símbolo \"" << evar->c_str() << "\" definido como externo não foi utilizado." << endl;
    }
  }
  //liberar memória
  symv.clear();
  symv.shrink_to_fit();
}

bool Assembler::writeFile(string filename){
  if(nErros){
    cout << endl << "Erros encontrados: " << nErros << endl;
    cout << "Arquivo objeto não pôde ser gerado." << endl;
    return false;
  } else {
    //gerar arquivo objeto filename
    ofstream objectFile;
    objectFile.open(filename);
    string aux;

    if(toBeLinked){
      vector<Symbol>::iterator it;
      vector<Symbol> tab;

      //tabela de uso
      objectFile << "TABLE USE" << endl;
      tab = use.getVector();
      for(it = tab.begin(); it != tab.end(); ++it){
        for (auto & c: it->label) c = toupper(c);
        objectFile << it->label << " " << it->value << endl;
      }
      objectFile << endl;
      vector<Symbol>().swap(tab); //free memory

      //tabela de definições
      objectFile << "TABLE DEFINITION" << endl;
      tab = def.getVector();
      for(it = tab.begin(); it != tab.end(); ++it){
        for (auto & c: it->label) c = toupper(c);
        objectFile << it->label << " " << it->value << endl;
      }
      objectFile << endl;
      vector<Symbol>().swap(tab); //free memory

      objectFile << "CODE" << endl;
      for(unsigned i=0; i < object.size(); i++){
        aux = object.get(i);
        objectFile << aux.c_str() << " ";
      }

      objectFile << endl;
      objectFile << endl << "BIT MAP" << endl;
      vector<bool>::iterator itb;
      for(itb = bitMapRelative.begin(); itb!=bitMapRelative.end(); ++itb){
        objectFile << *itb << " ";
      }

    } else {
      for(unsigned i=0; i < object.size(); i++){
        aux = object.get(i);
        objectFile << aux.c_str() << " ";
      }
    }
    return true;
  }
}

//verificação de divisão por zero deve ser feita ao término da segunda passagem
//com todos os símbolos já trocados
void Assembler::verifyDivision(unsigned dataInit){
  int op = 0; //opcode

  Cmd command;

  unsigned i = 0;
  while(i < dataInit) {
    op = stoi(object.get(i));
    try {
      //exceção ocorre se op não for uma instrução válida
      command = cmdTable.get(op);
      if(!command.mnemonic.compare("div")){
        //object.get(i+1) é o operando de div, tipo "div dois", seria o endereço de "dois"
        //verifica se o que tem no endereço é uma constante
        try {
          if(object.isConst(stoi(object.get(i+1)))){ //caso seja, verifica se é zero
            if(stoi(object.get(stoi(object.get(i+1)))) == 0){
              cout << "L" << object.getLine(i) << ": Divisão por zero. ";
              cout << "Erro semântico." << endl;
              nErros++;
            }
          }
        } catch (invalid_argument e) {
          i = i + command.operands + 1;
        }
      }
    } catch (out_of_range e){
      ;
    }
    i = i + command.operands + 1;
  }
}

//verificar modificação de constante
void Assembler::constCheck(unsigned dataInit){
  int op = 0; //opcode

  regex cmd1op ("store|input");
  regex cmd2op ("copy");
  Cmd command;

  //iterar até antes da seção de dados
  unsigned i = 0;
  while(i < dataInit) {
    op = stoi(object.get(i));
    try {
      //exceção ocorre se op não for uma instrução válida
      command = cmdTable.get(op);
      try {
        if(regex_match(command.mnemonic, cmd1op))
          if(object.isConst(stoi(object.get(i+1)))) {
            cout << "L" << object.getLine(i) << ": Modificação de valor constante. ";
            cout << "Erro semântico." << endl;
            nErros++;
          }

        if(regex_match(command.mnemonic, cmd2op))
          if(object.isConst(stoi(object.get(i+2)))){
            cout << "L" << object.getLine(i) << ": Modificação de valor constante. ";
            cout << "Erro semântico." << endl;
            nErros++;
          }
      } catch (invalid_argument e) {
        ;
      }
      i = i + command.operands + 1;
    } catch (out_of_range e){
      i = i + command.operands + 1;
    }

  }
}

//essa função verifica operandos de funções que leem ou escrevem na memória
void Assembler::memoryCheck(unsigned dataInit){
  int op = 0; //opcode

  regex cmd1op ("add|sub|mult|div|load|store|input|output");
  regex cmd2op ("copy");
  Cmd command;

  //fim da seção de dados
  unsigned dataEnd = object.size() - 1;

  vector<Symbol> useSym = use.getVector();
  vector<Symbol>::iterator it;

  unsigned i = 0;
  bool broke = false;
  while(i < dataInit) {
    op = stoi(object.get(i)); //opcode da operação
    try { //exceção ocorre se op não for uma instrução válida
      command = cmdTable.get(op);
      unsigned address;
      try {
        address = stoi(object.get(i+1));
      } catch (invalid_argument e) {
        //provavelmente label indefinido se chegou aqui no catch
        i = i + command.operands + 1;
        return;
      }
      if(regex_match(command.mnemonic, cmd1op)){
        for(it = useSym.begin(); it != useSym.end(); ++it){
          if(it->value == (long int) i + 1) {
            broke = true; break;
          }
        }
        if(broke) { //break outer loop, as it is an extern variable
          broke = false;
          i = i + command.operands + 1;
          continue;
        }
        if(address > dataEnd) {
          cout << "L" << object.getLine(i) << ": Endereço de memória não reservado. ";
          cout << "Erro semântico." << endl;
          nErros++;
        }

        if(address < dataInit) {
          cout << "L" << object.getLine(i) << ": Endereço de instruções ao invés de memória. ";
          cout << "Erro semântico." << endl;
          nErros++;
        }
      }
      if(regex_match(command.mnemonic, cmd2op)){
        for(it = useSym.begin(); it != useSym.end(); ++it){
          if(it->value == (long int) i + 2) {
            broke = true; break;
          }
        }
        if(broke) { //break outer loop, as it is an extern variable
          broke = false;
          i = i + command.operands + 1;
          continue;
        }
        try {
          address = stoi(object.get(i+2));
        } catch (invalid_argument e) {
          //provavelmente label indefinido se chegou aqui no catch
          i = i + command.operands + 1;
          return;
        }
        if(address > dataEnd){
          cout << "L" << object.getLine(i) << ": Endereço de memória não reservado. ";
          cout << "Erro semântico." << endl;
          nErros++;
        }

        if(address < dataInit) {
          cout << "L" << object.getLine(i) << ": Argumento 2. Endereço de instruções ao invés de memória. ";
          cout << "Erro semântico." << endl;
          nErros++;
        }
      }
      i = i + command.operands + 1;
    } catch (out_of_range e){
      i = i + command.operands + 1;
    }

  }
}

void Assembler::jumpCheck(unsigned dataInit){
  int op = 0; //operação

  Cmd command;

  //fim da seção de dados
  unsigned dataEnd = object.size() - 1;

  vector<Symbol> useSym = use.getVector();
  vector<Symbol>::iterator it;

  unsigned i = 0;
  regex num ("([\\+-]?)\\d+");
  bool broke = false;
  while(i < dataInit) {
    op = stoi(object.get(i)); //opcode da operação
    try { //exceção ocorre se op não for uma instrução válida
      command = cmdTable.get(op);
      unsigned address;
      try {
        address = stoi(object.get(i+1));
        if(!regex_match(object.get(i+1), num)){
          i = i + command.operands + 1;
          continue;
        }
      } catch (invalid_argument e) {
        //provavelmente label indefinido se chegou aqui no catch
        i = i + command.operands + 1;
        continue;
      }
      if(cmdTable.isJump(command.mnemonic)){
        for(it = useSym.begin(); it != useSym.end(); ++it){
          if(it->value == (long int) i + 1) {
            broke = true; break;
          }
        }
        if(broke) { //break outer loop, as it is an extern variable
          broke = false;
          i = i + command.operands + 1;
          continue;
        }
        if(address > dataEnd) {
          cout << "L" << object.getLine(i) << ": Pulo para endereço de memória não reservado. ";
          cout << "Erro semântico." << endl;
          nErros++;
        }

        if(address >= dataInit) {
          cout << "L" << object.getLine(i) << ": Pulo inválido para endereço de memória ao invés de instruções. ";
          cout << "Erro semântico." << endl;
          nErros++;
        }
      }
      i = i + command.operands + 1;
    } catch (out_of_range e){
      i = i + command.operands + 1;
    }

  }
}

#include "exeCmd.cpp"
