- Universidade de Brasília
- Semestre 1º/2015

##Alunos
- Ataias Pereira Reis 10/0093817
- Pedro Paulo Struck Lima 11/0073983

##Etapas

1. Montador
------------
- Pré-processador: objetivo é retirar espaços desnecessários e comentários. Troca todos os tabs por espaços e adiciona espaço antes de vírgulas.
- Montagem: usar algoritmo de duas passagens. Primeira passagem: gerar tabela de símbolos. Segunda passagem: gerar tabela de definições e de uso enquanto monta o código. Para leitura dos tokens, usar stringstream: http://goo.gl/7gVAE4

2. Ligador
------------

##Requisitos
Compilador `g++-4.9` ou superior. Opção é `clang++-3.6` ou superior.

##Instalando requisitos

Dependendo do sistema operacional, pode ser mais simples ou mais difícil executar nosso trabalho. Para facilitar a vida, veja o seguinte:

#Fedora 21

  `sudo yum install gcc-c++`

#OpenSuse 13.2
Pode-se instalar o g++ 4.9 a partir do comando:

  `sudo zypper install gcc49-c++`

No entanto, este gcc não se torna o padrão do sistema. Para usá-lo no programa, edite o Makefile, trocando o valor da variável `CXX` de `g++` para `g++-4.9`.

#Ubuntu 15.04
Instalar o g++ padrão fornece versão 4.9 ou superior nesta plataforma:

    `sudo apt-get install g++`

#Ubuntu 14.04
Se desejar utilizar o gcc, pode-se instalar com os seguintes comandos:

  `sudo add-apt-repository ppa:ubuntu-toolchain-r/test`

  `sudo apt-get update`

  `sudo apt-get install g++-4.9`


É necessário mudar o compilador (variável `CXX`) no Makefile para `g++-4.9`. Caso prefira o clang, é necessário adicionar os repositórios para  a última versão, que são os seguintes:

  `deb http://llvm.org/apt/trusty/ llvm-toolchain-trusty-3.6 main`

  `deb-src http://llvm.org/apt/trusty/ llvm-toolchain-trusty-3.6 main`

No terminal, adicione a chave para os repositórios e instale:

  `wget -O - http://llvm.org/apt/llvm-snapshot.gpg.key|sudo apt-key add -`

  `sudo apt-get update && sudo apt-get install clang-3.6 lldb-3.6`


No Makefile, ponha o nome do compilador como `clang++-3.6`.

#Mac OS X

Instale o último compilador que vem com o Xcode, deve funcionar sem problemas.

##Compilando
Entre no diretório `src` e execute `make`

##Executando
`./montador`, uma vez compilado e dentro do diretório

##Observações
-Adicional-
A pasta `Testes` está indo apenas como um adicional. Ela contém alguns programas que usamos para teste com vários erros para 
serem detectados (bateria_de_erros1) e alguns outros testes verificando as capacidades do montador e os erros que ele 
deve identificar, de acordo com a especificação (bateria_de_erros2).
No diretório `bateria_programas_executaveis` estão alguns programas simples que fizemos pra testar o montador e o ligador, pra 
ver se funcionavam com o simulador oferecido no moodle. Todos os programas estão funcionando normalmente.
---

Erros, principalmente semânticos, podem aparecem desconectados dos outros erros da linha. Ou seja, pode ser que não apareçam todos os erros de uma linha em sequência. Por exemplo, erros de símbolos indefinidos aparecem após erros semânticos de todo o arquivo.

Alguns erros foram destrinchados em outros. Veja esta seção de programa:

  `input b+2; Linha 8`

  `input b+2; Linha 10`

  `section data`

  `b: space 2 ;Linha 15`

  `c: const -4; Linha 16`

Erros que aparecem para a linha 8:

  `L8: Tentativa de acesso a endereço não reservado para variável "b" definida em L15 do arquivo. Erro semântico.`

  `L8: Modificação de valor constante. Erro semântico.`

Note que o endereço b+2 é o endereço da constante c, por isso é acusado modificação de valor constante. Ao mesmo tempo, o vetor b só tem dois elementos e o acesso ao elemento logo abaixo dele é um erro (isto não estava claro na especificação, mas o professor disse em sala ser um erro.)

Além destes erros, há outro que nosso programa emite:

`L10: Endereço de memória não reservado. Erro semântico.`

Este erro ocorre quando o endereço é realmente não reservado para o programa, ou seja, tentativa de acessar endereços abaixo da variável c. Para uma linha, pode-se ter tanto mensagens de erro de memória não reservada como dela não ser reservada para a variável, dependendo se foge do tamanho do programa ou não. Logo, atente para isso quando for interpretar as mensagens de erro, já que isso não parece ter sido padronizado pela especificação e fizemos do melhor jeito que pensamos.

Alguns erros são escondidos por outros. Em várias casos é possível pegar todos, mas nem sempre. Por exemplo, uma seção de dados faltante esconde que modificação de constante seja notificada. Após a correção da seção, o erro pode ser notificado.

Não diferenciamos instrução inválida de diretiva inválida. O erro para ambas é "comando inválido".

Um tipo de bug que não foi resolvido é o seguinte:

`copy	a. b; Linha 5`

fornece os erros:

`L5: Número de argumentos para instrução "copy" inválido. Erro sintático.`

`L5: Argumento "a." inválido. Erro léxico.`

`L5: Operando "a.b" inválido para instrução "copy". Erro sintático.`

O primeiro está ok. Para copy ter dois argumentos, é necessário vírgula, logo o programa pensou ter só um argumento. O segundo erro também está ok, uma variável não pode ter ".". O terceiro é parece ser um bug, mas tem uma explicação. No caso de se ter algo como "copy a + a" ele acusaria "a+a" inválido. Ele concatena os argumentos não separados por vírgula e verifica se é uma soma.

Erros não aparecem pela ordem do código, as linhas são sempre indicadas mas certos tipos de erros que aparecem antes podem ser mostrados depois de erros mais ao final do programa, é bom ler todos para dizer se está faltando algo ou não.

É feito uso de uma mapa de bits no arquivo objeto. O professor disse que podia. 
