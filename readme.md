Trabalhos de Software Básico - 1º/2015
========================================================

Alunos
-------
* Ataias Pereira Reis 10/0093817
* Pedro Paulo Struck Lima 11/0073983

Info
-------
Program that assembles an "invented assembly" language to create object code and also an ELF Linux executable. 
Made in C++.
