- Universidade de Brasília
- Semestre 1º/2015
- Trabalho 2 de Software Básico
- Tradutor

##Alunos
- Ataias Pereira Reis 10/0093817
- Pedro Paulo Struck Lima 11/0073983

##Etapas

1. Tradutor
------------
- O tradutor se baseia largamente no montador apresentado por nós para o primeiro trabalho. Os tipos de erros foram removidos, ou seja, o programa não informa se o erro é léxico, sintático ou semântico. Isso faz parte da especificação do trabalho 2. Como um ligador não faz parte deste trabalho, as diretivas relacionadas a múltiplos módulos - extern, public, begin e end - também foram removidas do código e é um erro tentar entrar com um código deste tipo.

- Foi pedido um tradutor que aceitasse somente números sem sinal. No entanto, nosso tradutor aceitar números inteiros com sinal. Isso permitiu que alguns dos programas teste que fizemos no primeiro trabalho que usavam números negativos pudessem ser executados também.

- O tradutor gera saídas em três arquivos: assembly (.asm), código não-formatado (.cod) e elf. O nome com extensão deve ser digitado, assim, a extensão recomendada não precisa ser a utilizada quando se executa o programa. É possível escolher entre dois tipos de impressões do arquivo .cod. Uma é o que foi pedido na especificação: um arquivo só com os bytes em sequência. A outra mostra os endereços e separa as instruções por linha, que permite que se analise mais facilmente o código. De toda forma, o ELF deve ser executável e funcional para se confirmar que a tradução está correta. Para mostrar impressão bonita, vá em `Asm32.h` dentro da pasta `include/` e troque o valor do define `__PRETTY_PRINT_NO_FORMAT` para `1` e compile de novo.

2. ELF

- O ELF foi criado com o uso da biblioteca ELFIO. Ela já foi incluída na pasta `include/`, não é necessário instalar nada para que o programe compile, além de ter um compilador conforme os requisitos. Há seções de texto, dados, string, símbolos e notas. Para ver as notas, pode-se executar `readelf -a programa.elf`. Para se ver as instruções utilizadas e a tabela de símbolos do ELF, pode se utilizar `objdump -M intel -D -f programa.elf | less` que já mostra a saída no programa `less` com sintaxe do Assembly Intel. Todos os endereços utilizados no código não-formatado e no ELF são relativos e estão em Little Endian.


##Requisitos
Compilador `g++-4.9` ou superior. Opção é `clang++-3.6` ou superior. O programa foi testado em Mac OS 10.10, Fedora 21 e Ubuntu 14.04 e funcionou sem problemas. O ELF gerado no Mac foi copiado para uma máquina Linux para ser executado.

##Instalando requisitos

Dependendo do sistema operacional, pode ser mais simples ou mais difícil executar nosso trabalho. Para facilitar a vida, veja o seguinte:

#Fedora 21

  `sudo yum install gcc-c++`

#OpenSuse 13.2
Pode-se instalar o g++ 4.9 a partir do comando:

  `sudo zypper install gcc49-c++`

No entanto, este gcc não se torna o padrão do sistema. Para usá-lo no programa, edite o Makefile, trocando o valor da variável `CXX` de `g++` para `g++-4.9`.

#Ubuntu 15.04
Instalar o g++ padrão fornece versão 4.9 ou superior nesta plataforma:

    `sudo apt-get install g++`

#Ubuntu 14.04
Se desejar utilizar o gcc, pode-se instalar com os seguintes comandos:

  `sudo add-apt-repository ppa:ubuntu-toolchain-r/test`

  `sudo apt-get update`

  `sudo apt-get install g++-4.9`


É necessário mudar o compilador (variável `CXX`) no Makefile para `g++-4.9`. Caso prefira o clang, é necessário adicionar os repositórios para  a última versão, que são os seguintes:

  `deb http://llvm.org/apt/trusty/ llvm-toolchain-trusty-3.6 main`

  `deb-src http://llvm.org/apt/trusty/ llvm-toolchain-trusty-3.6 main`

No terminal, adicione a chave para os repositórios e instale:

  `wget -O - http://llvm.org/apt/llvm-snapshot.gpg.key|sudo apt-key add -`

  `sudo apt-get update && sudo apt-get install clang-3.6 lldb-3.6`


No Makefile, ponha o nome do compilador como `clang++-3.6`.

#Mac OS X

Instale o último compilador que vem com o Xcode, deve funcionar sem problemas.

##Compilando
Entre no diretório `src` e execute `make`

##Executando
`./montador`, uma vez compilado e dentro do diretório

##Observações
-Adicional-
A pasta `Testes` está indo apenas como um adicional. Ela contém alguns programas que usamos para teste com vários erros para serem detectados (erros1) e alguns outros testes verificando as capacidades do montador e os erros que ele deve identificar, de acordo com a especificação (erros2).
No diretório `executaveis` estão alguns programas simples que fizemos pra testar o montador e o ligador, pra ver se funcionavam com o simulador oferecido no moodle. Todos os programas estão funcionando normalmente.

Erros, principalmente semânticos, podem aparecem desconectados dos outros erros da linha. Ou seja, pode ser que não apareçam todos os erros de uma linha em sequência. Por exemplo, erros de símbolos indefinidos aparecem após outros erros semânticos de todo o arquivo.

Não são aceitos imediatos com instruções de pulos, pois o tamanho das instruções na IA-32 é variável e isso poderia 
ocasionar muitos erros.

Todos os inputs devem ser seguidos por um enter. Exemplo, caso a entrada seja 1, 2 e 3, entre:
"1
2
3 "
e não "1 2 3 ", separados por espaço.


