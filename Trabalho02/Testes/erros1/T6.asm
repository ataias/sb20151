;Teste de diretivas e instruções inválidas
section text
jmpnn a b*
add: add;04

section data
jmp: cos 30
l: const 0x30

;resultados do teste em 01/07/2015
;./montador ../Testes/Bateria\ de\ Erros/T6.asm ../Testes/out.o

;L3: Comando inexistente "jmpnn". Erro.
;L3: Argumento "b*" inválido. Erro.
;L4: Número de argumentos para instrução "add" inválido. Erro.
;L7: Comando inexistente "cos". Erro.


;Erros encontrados: 4
;Arquivo objeto não pôde ser gerado.
