; seção inválida

section asd2
input a
repeate: asdf*: input d
jmpn repeate
output d
stop

section data2
a: b: const -1
c: const 2
d: space
d: space 3

;resultados do teste em 01/07/2015
;./montador ../Testes/Bateria\ de\ Erros/T9.asm ../Testes/out.o

;L3: Seção "asd2" inválida. Erro.
;L4: Instrução "input" utilizada em seção indefinida. Erro.
;L5: Instrução "input" utilizada em seção indefinida. Erro.
;L6: Instrução "jmpn" utilizada em seção indefinida. Erro.
;L7: Instrução "output" utilizada em seção indefinida. Erro.
;L8: Instrução "stop" utilizada em seção indefinida. Erro.
;L10: Seção "data2" inválida. Erro.
;L11: Uso de diretiva "const" em seção não definida. Erro.
;L12: Uso de diretiva "const" em seção não definida. Erro.
;L13: Uso de diretiva "space" em seção não definida. Erro.
;L14: Label redefinido. Erro.
;L14: Uso de diretiva "space" em seção não definida. Erro.

;Erros encontrados: 14
;Arquivo objeto não pôde ser gerado.
