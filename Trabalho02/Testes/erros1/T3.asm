;testes para seção data faltante e outros erros
section text
input a
input b
load c s
store a*
stop a
jmpz 2
jmpp ab
store c		;modificação de constante
stop

c: const 0xaa
a: space
b: space

;resultados do teste em 01/07/2015
;achou quase todos, não achou tentativa de modificação de constante, ele só acha se a seção data tiver sido definida
;./montador ../Testes/Bateria\ de\ Erros/T3.asm ../Testes/out.o

;L5: Instrução "load" utilizada incorretamente. Erro sintático.
;L6: Operando "a*" inválido para instrução "store". Erro léxico.
;L7: Número de argumentos para instrução "stop" inválido. Erro sintático.
;L8: Operando "2" inválido para instrução "jmpz". Erro léxico.
;L13: Const utilizado em seção errada. Erro semântico.
;L14: Space utilizado em seção errada. Erro semântico.
;L15: Space utilizado em seção errada. Erro semântico.
;L9: Label indefinida "ab". Erro semântico.
;
;Erros encontrados: 8
;Arquivo objeto não pôde ser gerado.

