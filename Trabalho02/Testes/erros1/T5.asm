;Teste de pulo para rótulos inválidos e outros erros
section text

jmp r
load c
jmp a* *
jmp c*
copy c, b,

jmp d
stop

section data
c: const 0x1
d: space


;resultados do teste em 01/07/2015
;./montador ../Testes/Bateria\ de\ Erros/T5.asm ../Testes/out.o

;L6: Operando "a*" inválido para instrução "jmp".  Erro.
;L6: Instrução "jmp" utilizada incorretamente. Erro.
;L7: Operando "c*" inválido para instrução "jmp".  Erro.
;L4: Label indefinida "r". Erro.
;L6: Label indefinida "a*". Erro.
;L8: Label indefinida "b". Erro.
;L10: Pulo inválido para endereço de memória ao invés de instruções. Erro.

;
;Erros encontrados: 7
;Arquivo objeto não pôde ser gerado.
