;argumento inválido...

section text
input +e
load - a
load b + 34 + 3
load c--1
load c, d, e
stop

section data
a: const -
b: const 0xx40
c: space a*
d: space -2
e: space
f: const -0x50

;resultados do teste em 01/07/2015
;./montador ../Testes/Bateria\ de\ Erros/T10.asm ../Testes/out.o


;L4: Operando "+" inválido para instrução "input". Erro léxico.
;L4: Instrução "input" utilizada incorretamente. Erro sintático.
;L5: Operando "-" inválido para instrução "load". Erro léxico.
;L5: Instrução "load" utilizada incorretamente. Erro sintático.
;L6: Instrução "load" utilizada incorretamente. Erro sintático.
;L7: Instrução "load" utilizada incorretamente. Erro sintático.
;L8: Número de argumentos para instrução "load" inválido. Erro sintático.
;L12: Constante "-" inválida. Erro léxico.
;L13: Argumento "0xx40" inválido. Erro léxico.
;L13: Constante "0xx40" inválida. Erro léxico.
;L14: Argumento de space inválido. Erro sintático.
;L14: Argumento "a*" inválido. Erro léxico.
;L15: Space utilizado com argumento inválido. Erro sintático.
;
;Erros encontrados: 13
;Arquivo objeto não pôde ser gerado.
