; dois rótulos na mesma linha, rótulo repetido e outros

section text
input a
repeate: asdf*: input d
jmpn repeate
output d
stop

section data
a: b: const -1
c: const 2
d: space
d: space 3

;resultados do teste em 01/07/2015
;./montador ../Testes/Bateria\ de\ Erros/T8.asm ../Testes/out.o

;L14: Label redefinido. Erro.
;L4: Modificação de valor constante. Erro.

;
;Erros encontrados: 2
;Arquivo objeto não pôde ser gerado.
