;Testando número de operandos das instruções e operandos inválidos
section text
  input a, b
  input c
  copy a, b, c*
  copy a, b
  copy a
  input
  add
  add a
  sub
  c:  sub a
  div
  div b
  jmp
  jmp c
  jmpn c
  jmpn
  jmpp c
  jmpp
  jmpz
  jmpz c
  jmpz c a
  load
  load c&
  output
  output a c
  stop a c
  stop

section data
  a: space
  b: space
  c: space

;resultados do teste em 01/07/2015
;Todos os erros foram capturados

;./montador ../Testes/Bateria\ de\ Erros/T1.asm ../Testes/out.o
;L3: Número de argumentos para instrução "input" inválido. Erro sintático.
;L5: Número de argumentos para instrução "copy" inválido. Erro sintático.
;L5: Argumento "c*" inválido. Erro léxico.
;L7: Número de argumentos para instrução "copy" inválido. Erro sintático.
;L8: Número de argumentos para instrução "input" inválido. Erro sintático.
;L9: Número de argumentos para instrução "add" inválido. Erro sintático.
;L11: Número de argumentos para instrução "sub" inválido. Erro sintático.
;L13: Número de argumentos para instrução "div" inválido. Erro sintático.
;L15: Número de argumentos para instrução "jmp" inválido. Erro sintático.
;L18: Número de argumentos para instrução "jmpn" inválido. Erro sintático.
;L20: Número de argumentos para instrução "jmpp" inválido. Erro sintático.
;L21: Número de argumentos para instrução "jmpz" inválido. Erro sintático.
;L23: Instrução "jmpz" utilizada incorretamente. Erro sintático.
;L24: Número de argumentos para instrução "load" inválido. Erro sintático.
;L25: Operando "c&" inválido para instrução "load". Erro léxico.
;L26: Número de argumentos para instrução "output" inválido. Erro sintático.
;L27: Instrução "output" utilizada incorretamente. Erro sintático.
;L28: Número de argumentos para instrução "stop" inválido. Erro sintático.
;L34: Label redefinido. Erro semântico.
;L4: Endereço de instruções ao invés de memória. Erro semântico.
;
;Erros encontrados: 20
;Arquivo objeto não pôde ser gerado.

