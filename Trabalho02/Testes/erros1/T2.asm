;testes para seção text faltante e outros erros
input a
input b
load c
store a*
stop a
jmpz 2
jmpp ab
stop

section data
c: const 0xaa
a: space
b: space

;resultados do teste em 01/07/2015
;pegou todos os erros
;./montador ../Testes/Bateria\ de\ Erros/T2.asm ../Testes/out.o

;L2: Instrução "input" utilizada em seção indefinida. Erro semântico.
;L3: Instrução "input" utilizada em seção indefinida. Erro semântico.
;L4: Instrução "load" utilizada em seção indefinida. Erro semântico.
;L5: Instrução "store" utilizada em seção indefinida. Erro semântico.
;L5: Operando "a*" inválido para instrução "store". Erro léxico.
;L6: Instrução "stop" utilizada em seção indefinida. Erro semântico.
;L6: Número de argumentos para instrução "stop" inválido. Erro sintático.
;L7: Instrução "jmpz" utilizada em seção indefinida. Erro semântico.
;L7: Operando "2" inválido para instrução "jmpz". Erro léxico.
;L8: Instrução "jmpp" utilizada em seção indefinida. Erro semântico.
;L9: Instrução "stop" utilizada em seção indefinida. Erro semântico.
;L11: Seção de dados deve vir depois da seção de texto. Erro semântico.
;L8: Label indefinida "ab". Erro semântico.
;
;Erros encontrados: 13
;Arquivo objeto não pôde ser gerado.

