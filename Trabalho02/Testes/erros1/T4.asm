;Declarações ausentes e repetidas e outros erros
section text
r:
input f
output e
load r
load c
mult d
store r
store d
stop

section data
r: const 0x
c: space 1
d: const -1234
e: const 123 34

;resultados do teste em 01/07/2015
;./montador ../Testes/Bateria\ de\ Erros/T4.asm ../Testes/out.o
;está acusando "Constante 0x invalida" agora.

;L14: Label redefinido. Erro.
;L14: Constante "0x" inválida. Erro.
;L14: Argumento "0x" inválido. Erro.
;L17: Número de argumentos de const inválido. Erro.
;L4: Label indefinida "f". Erro.
;L10: Modificação de valor constante. Erro.

;
;Erros encontrados: 6
;Arquivo objeto não pôde ser gerado.
