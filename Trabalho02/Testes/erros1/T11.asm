; endereço de memória não-reservado, modificação de constante e uns errinhos
section text

a: input a
input b
input b +1
input b+2
output b+3
input c
stop

section data
b: space 2
c: const -4

;resultados do teste em 01/07/2015
;./montador ../Testes/Bateria\ de\ Erros/T11.asm ../Testes/out.o

;Novos erros encontrados:
;L8: Modificação de valor constante. Erro semântico.
;Por que? b é uma variável, declarada com space 2 e não constate


;L7: Tentativa de acesso a endereço não reservado para variável "b" definida em L13 do arquivo. 
;L8: Tentativa de acesso a endereço não reservado para variável "b" definida em L13 do arquivo. 
;L7: Modificação de valor constante. Erro.
;L9: Modificação de valor constante. Erro.
;L4: Endereço de instruções ao invés de memória. Erro.
;L8: Endereço de memória não reservado. Erro.

;Erros encontrados: 6
;Arquivo objeto não pôde ser gerado.
