;compilar e ligar:
;nasm -f elf -o todas_ia32.o todas_ia32.asm
;ld -m elf_i386 -o todas_ia32 todas_ia32.o

;programa: todas.asm
;Entrada: nada
;Saída:	  n numeros inteiros em sequência, de 1 até n, onde n
;é a quantidade de testes do programa
;Atualmente, n = 8.

;Descrição: testa todas as instruções do assembly inventado e vai 
;imprimindo um inteiro para cada parte que ele passa.

;(X)ADD
;(X)SUB
;(X)MULT
;(X)DIV
;(X)JMP
;(X)JMPN
;(X)JMPP
;(X)JMPZ
;(X)COPY
;(X)LOAD
;(X)STORE
;()INPUT
;(X)OUTPUT
;(X)STOP


NOTFOUND:	equ 404
section text
	;add -> imprime 1
	load 	zero
	store	vetnum
	load 	um
	store	vetnum+1
	load	vetnum
	add		vetnum+1
	store	result
	output	result

	;sub -> imprime 2
	load 	um
	store	vetnum+2
	load 	tres
	store	vetnum+1
	load	vetnum+1
	sub		vetnum+2
	store	vetnum+2
	output	vetnum+2 ;testa impressa na terceira posicao vetor
	
	;mult e div -> imprime 3
	;2*6/4 = 3
	load 	dois
	store	vetnum
	load 	seis
	store	vetnum+1
	load 	quatro
	store	vetnum+2
	load	vetnum
	mult	vetnum+1
	div		vetnum+2
	store	vetnum+2
	output	vetnum+2;testa impressa na terceira posicao vetor
	
	;jmp -> imprime 4
	load 	erro
	store	vetnum
	jmp		lable1
	output	vetnum ;se imprimir o 9, não fez o jump


lable1:
	load	quatro
	store 	vetnum+1
	output	vetnum+1
	;continuando o teste
	;jmpn -> imprime 5
	load 	tres
	store 	vetnum
	load 	quatro
	store	vetnum+1
	load 	vetnum
	sub 	vetnum+1
	jmpn	lable2
	load 	erro
	store	vetnum
	output 	vetnum

lable2:
	load	cinco
	store 	result
	output	result
	;jmpp -> imprime 6
	load 	quatro
	store 	vetnum
	load 	dois
	store	vetnum+1
	load 	vetnum
	sub 	vetnum+1
	jmpp	lable3
	load 	erro
	store	vetnum
	output 	vetnum
	
lable3:
	load	seis
	store 	result
	output	result
	;jmpz -> imprime 7
	load 	oito
	store 	vetnum+1
	load 	oito
	store	vetnum+2
	load 	vetnum+1
	sub 	vetnum+2
	jmpz	lable4
	load 	erro
	store	vetnum
	output 	vetnum

lable4:
	load	sete
	store 	result
	output	result
	;copy -> imprime 8
	load 	quatro
	store 	vetnum
	load 	cinco
	store	vetnum+1
	load	nove
	store	vetnum+2 ;vetor está [4, 5, 9]
	
	;copiaremos o 4 pro 4 e somaremos as duas primeiras posicoes e armazenaremos na posicao tres, ficando 4 4 8
	copy	vetnum, vetnum+1
	load	vetnum
	add		vetnum+1
	store	vetnum+2
	if 		NOTFOUND
	output 	vetnum+2
	
fim:
	stop

section data
	zero:	const	0
	um:		const	1
	dois:	const	2
	tres:	const	3
	quatro:	const	4
	cinco:	const	5
	seis:	const	6
	sete:	const	7
	oito:	const	8
	nove:	const	9
	aux:	space	2
	aux2:	space	2
	erro:	const	NOTFOUND
	
	vetnum:	space	3
	result:	space	1
