;entrada: Um numero inteiro
;saida:	  0 se o numero for par, 1 se for impar.

;3/2 = 1      3 = 1 * 2 + 1
;8/2 = 4      8 = 4 * 2 + 0

not: equ 0
one: equ 1
two: equ 2
flag: equ 10
flag2:	equ 0
section data
	a:		space one
	aux:	space one
	if	flag			;diretiva if com label true
	zero:	const not
	um:		const one
	dois:	const two
	resto:	space one
	result:	space one

section text
	input a
	load a
	div	dois
	store result
	
	load result
	if	135				;diretiva if com imediato
	mult dois
	store aux
	
	;resto = a - aux
	load a
	sub aux
	store resto
	
	load resto
	jmpz par 	;if		resto == 0, a é par
	jmp impar 	;else	se não, a é impar
	
par: 
	output zero
	jmp fim
	
impar:
	output um
	;jmp fim
	
fim:
	if flag2	;diretiva if falsa. deve apagar a linha seguinte.
	output dois ;flag2 é false. o programa não deve imprimir o número 2.
	
	stop
