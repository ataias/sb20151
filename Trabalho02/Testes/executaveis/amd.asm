;Entrada: Um número inteiro positivo
;Saida:	  Três numeros inteiros

;;AMD - Anos, Meses, Dias
;Explicação: O número inteiro lido é o número de dias
;			 A saída são três números, na ordem, anos, meses, dias, calculados com oo numero de dias entrado pelo usuário
;			 Considera-se o ano com 365 dias e o mês com 30 dias.

;Exemplos de input/output
;456 -> 1 3 1
;500 -> 1 4 15

qtd_ano:	equ 365
qtd_mes:	equ 30
one:		equ 1
section data
	dias_ano:	const 	qtd_ano
	dias_mes: 	const 	qtd_mes
	dias_lidos: space 	one
	anos:		space	one
	meses:		space	one
	dias:		space	one
	resto:		space	one
	aux:		space 	one
	
section text
	input 	dias_lidos
	load 	dias_lidos
	
	div		dias_ano
	store	anos	;pegou anos
	
	; resto = dias_lidos - (anos * 365)
	mult	dias_ano
	store	aux
	load 	dias_lidos 
	sub		aux
	store	resto
	
	div		dias_mes
	store 	meses
	
	;resto = resto - (meses * 30), que finalmente será o número de dias
	mult	dias_mes
	store	aux
	load	resto
	sub 	aux
	store	dias
	
	output	anos
	output	meses
	output	dias
	
	stop
