;Entrada: Número complexo (com parte real e imaginária)
;Saída: quadrante do número complexo
;este programa falha se a entrada for (0,0) ou estiver em um
;dos eixos imaginários

section text

	;recebe entradas
	input a
	input b

	load zero
	copy zero, q

	load a
	q13: jmpn q23 ;se jump não ocorrer, a > 0
	load b
	q1:  jmpn q4
	load um
	store q
	jmp out
	q4:  load quatro
	store q
	jmp out
	;a < 0
	q23: load b
	jmpn q3
	q2:  load dois
	store q
	jmp out
	q3:  load tres
	store q
	jmp out

	out: output q ;mostra quadrante
	stop

section data
	a: space
	b: space
	zero:   const 0x0
	um:     const 0x1
	dois:   const 0x2
	tres:   const 0x3
	quatro: const 0x4
	q: space
