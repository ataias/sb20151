;Entrada: 3 números inteiros digitados pelo usuário
;Saída:	  números inteiros que são o dobro dos
;		  digitados pelo usuário.

;Descrição: Recebe os 3 inteiros. Multiplica o primeiro e o segundo por 2. Deixa o terceiro intacto. Imprime o vetor
;depois copia o segundo para o primeiro e o terceiro elemento e imprime o vetor

;exemplo de execução
;input:
;1 
;2 
;3
;output: 
;2 
;4 
;3
;4
;4
;4
;

section text

	;###Como pegar o indice do vetor vetnum para adicionar o offset cont? ex: input vetnum + contador
	;seria mais compacto e escalável com parametros de loop, mas parece que não dá.
	;Não se tem acesso ao índice do vetor.
	input	vetnum
	input	vetnum+1
	input	vetnum+2
	
	load	vetnum
	mult	dois
	store	vetnum

	load	vetnum+1
	mult	dois
	store	vetnum+1
	
	output	vetnum
	output	vetnum+1
	output	vetnum+2
	
	copy	vetnum+2, vetnum+0
	copy	vetnum+1, vetnum+0
	copy	vetnum+1, vetnum+2
fim:
	output	vetnum
	output	vetnum+1
	output	vetnum+2
	
	stop

section data
	vetnum:	space	3
	dois:	const	2
