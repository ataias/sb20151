;Exemplo 1: operando invalido

;entrada: um número inteiro
;saida:	  o número inteiro vezes a constante 2, determinada com EQU

123imprimeDobro: equ 1
section text
	input a
	load a
	mult dois
	store a+1
	output	a
	
	if	123imprimeDobro
	output a+1
	
	stop
	
section data
	a:		space 2
	dois:	const 2
