;Exemplo 2: label válida e presente

;entrada: um número inteiro
;saida:	  o número inteiro e ele vezes a constante 2

imprimeDobro: equ 1
section text
	input a
	load a
	mult dois
	store a+1
	output	a
	
lable:	if	imprimeDobro
	output a+1
	
	stop
	
section data
	a:		space 2
	dois:	const 2
