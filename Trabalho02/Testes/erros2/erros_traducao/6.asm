;6– diretivas ou instruções na seção errada;
section text
	a:	space		;aqui 
	b: 	const 	1	;aqui
	
	input	a
	output	a
	output	c
	stop
	
section data
	output	b 		;aqui
	c:		extern 	;aqui
	public 	b 		;aqui

