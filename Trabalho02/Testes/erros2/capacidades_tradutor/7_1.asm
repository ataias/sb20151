;7- O comando COPY deve utilizar uma vírgula entre os operandos (COPY A, B)
;copy normal
section text
	input	a
	copy	a, b	;este copy deve funcionar
	;copy	a. b    ; daria errado mas está comentado
	output	b
	stop
	
section data
	a:	space	1
	b:	space	1

;data do teste: 

;saida
;

