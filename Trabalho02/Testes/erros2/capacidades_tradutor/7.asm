;7- O comando COPY deve utilizar uma vírgula entre os operandos (COPY A, B)
section text
	input	a
	copy	a, b	;este copy deve funcionar
	copy	a. b 	;este copy não deve funcionar! '.' entre os operandos
	output	b
	stop
	
section data
	a:	space	1
	b:	space	1

;data do teste: 23/06/2015

;saida: ok. detectou erros em copy

;L5: Número de argumentos para instrução "copy" inválido. Erro.
;L5: Argumento "a." inválido. Erro.
;L5: Operando "a.b" inválido para instrução "copy". Erro.

;Erros na montagem, não foi possível traduzir o arquivo.
