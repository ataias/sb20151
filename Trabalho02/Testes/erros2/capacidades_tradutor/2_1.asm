;2_1- A seção de dados pode vir antes ou depois da seção de texto
;caso: data depois de text

section text
	input	a
	output	a
	stop

section data
	a:		space

;data do teste: 23/06/2015

;saida
;ok
