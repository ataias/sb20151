;5- Deve ser possível trabalhar com vetores
section text
	input	a 		;pega 3 números, guarda num vetor de 3 e imprime o vetor na tela
	input	a+1
	input 	a+2
	output	a
	output	a+1
	output	a+2
	stop
	
section data
	a:	space	3

;data do teste: 23/06/2015

;saida
;ok
