;6- Capacidade de aceitar comentários indicados pelo símbolo ";"
;começou
section text
	input	a ;variavel a
	output 	a		;variavel b
	input	b
	output 	b
	stop
;agora começa outra seção
section data
	a: space 1
	b: space 1

;data do teste: 23/06/2015

;saida
;ok
