;2- A seção de dados pode vir antes ou depois da seção de texto
;caso: data antes de text

section data
	a:		space

section text
	input	a
	output	a
	stop

;data do teste: 23/06/2015

;saida
;ok
