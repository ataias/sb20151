/*
 * Alunos:
 * - Ataias Pereira Reis - 10/0093817
 * - Pedro Paulo Struck Lima - 11/0073983
 * ---------------------------------------
 */

#include <iostream>
#include <vector>
#include <Table.h>

//compilar
//g++ -c -std=c++11 -Wall -Wextra Table.cpp -I ../include

using namespace std;

Table::Table(const char* error, string table_name){
  this->error = error;
  this->table_name = table_name;
  this->section_begin = 0;
}

Table::Table(const char* error, string table_name, unsigned section_begin){
  this->error = error;
  this->table_name = table_name;
  this->section_begin = section_begin;
}

void Table::push(std::string label, unsigned value, Section section){
  t.push_back({label,value,section});
}

void Table::push(std::string label, unsigned value){
  t.push_back({label,value, UNDEF});
}

void Table::print(){
  std::cout << std::endl << table_name << std::endl;
  for (unsigned i=0; i<t.size(); i++){
    cout << t[i].label << "\t\t ";
    cout << "0x";
    cout << hex << t[i].value + section_begin;
    cout << endl;
  }
}

//pega uma linha da tabela de símbolos ou definição ou uso
unsigned Table::get(string label){
  bool found=false;
  for (unsigned i=0; i<t.size(); i++)
    if(t[i].label.compare(label)==0) {
      found = true;
      return t[i].value + section_begin;
    }
  if (!found) throw error;
  return 0;
}

unsigned Table::getNO(string label){
  bool found=false;
  for (unsigned i=0; i<t.size(); i++)
    if(t[i].label.compare(label)==0) {
      found = true;
      return t[i].value;
    }
  if (!found) throw error;
  return 0;
}

Symbol Table::getS(std::string label){
  bool found = false; unsigned i = 0;
  for (unsigned i=0; i<t.size(); i++)
    if(t[i].label.compare(label)==0) {
      found = true;
      break;
    }
  if (!found) throw error;
  return t[i];
}

//return true se label foi encontrada e pode modificar seu valor
bool Table::set(string label, int value){
  for (unsigned i=0; i<t.size(); i++)
    if(t[i].label.compare(label)==0) {
      t[i].value = value;
      return true;
    }
  return false;
}

vector<Symbol> Table::getVector(){
  return t;
}

void Table::offset(unsigned section_begin){
  this->section_begin = section_begin;
}

unsigned Table::getOffset(){
  return section_begin;
}
