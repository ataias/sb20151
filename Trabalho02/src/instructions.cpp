/*
 * Alunos:
 * - Ataias Pereira Reis - 10/0093817
 * - Pedro Paulo Struck Lima - 11/0073983
 * ---------------------------------------
 */
 
//-------------- Instruções de argumento do mesmo tipo ------------
//("add|sub|mult|div|jmp|jpmn|jmpp|jmpz|load|store|input|output")
if(cmdTable.isCmdType1(spline.operation)){

  regex sum("\\+\\d+");
  vector<string>::iterator sp;
  //instruções devem ser utilizadas na seção de texto
  if(cSection == DATA){
    cout << "L" << l << ": Instrução \"" << spline.operation << "\" utilizada em seção errada.";
    cout << " Erro." << endl;
    nErros++;
  }

  if(cSection == UNDEF){
    cout << "L" << l << ": Instrução \"" << spline.operation;
    cout << "\" utilizada em seção indefinida.";
    cout << " Erro." << endl;
    nErros++;
  }

  //se houver 0 arguments, ou 2, imprime esse erro
  if(!spline.op2.empty() || spline.op1.empty()){
    cout << "L" << l << ": Número de argumentos para instrução \"";
    cout << spline.operation << "\" inválido.";
    cout << " Erro." << endl;
    nErros++;
  } else { //tem um argumento, verificar se está correto

    if(!regex_match(spline.op1.at(0), tokensyntax)) {
      cout << "L" << l << ": Operando \"" << spline.op1[0];
      cout << "\" inválido para instrução \"";
      cout << spline.operation << "\". ";
      cout << " Erro." << endl;
      nErros++;
    }

    string arg = "";
    if(spline.op1.size() > 1){
      for(sp = spline.op1.begin() + 1; sp != spline.op1.end(); ++sp){
        arg.append(sp->c_str());
      }
      if(!regex_match(arg, sum)) {
        cout << "L" << l << ": Instrução \"" << spline.operation;
        cout << "\" utilizada incorretamente.";
        cout << " Erro." << endl;
        nErros++;
      }
    }
  }

  //push para código objeto. Note que ainda estamos numa mistura da primeira com segunda passagem, mais ou menos
  //neste momento ainda não temos todos os rótulos necessários, então vamos colocar no código objeto um operando tal como "X1", "X1 2" (indica "X1 + 2"), "X1 4" (indica "X1 + 4"), "R" e depois trocamos eles pelo valor adequado
  Cmd ccmd = cmdTable.get(spline.operation);
  object.push(to_string(ccmd.opcode), l);

  string arg = "";

  //se o vetor contém "r", "+", "1" --> junta as strings em uma
  for(sp = spline.op1.begin(); sp != spline.op1.end(); ++sp){
    arg.append(sp->c_str());
  }

  //se não houver imediato, adiciona imediato 0, para padronizar
  // if(arg.size()==1){
  //   arg.append("+0");
  // }

  regex sum2("[a-z_]([a-z0-9_]*)(\\+\\d+)?");
  if(regex_match(arg, sum2)){
    object.push(arg, l);
  } else {
    if(!spline.op1.empty())
      object.push(spline.op1.begin()->c_str(), l);
    else
      object.push("*", l);
  }

  *pc += 2;
  return;
}

//instrução "stop"
if(!spline.operation.compare("stop")){
  //instruções devem ser utilizadas na seção de texto
  if(cSection == DATA){
    cout << "L" << l << ": Instrução \"" << spline.operation;
    cout << "\" utilizada em seção errada.";
    cout << " Erro." << endl;
    nErros++;
  }

  if(cSection == UNDEF){
    cout << "L" << l << ": Instrução \"" << spline.operation;
    cout << "\" utilizada em seção indefinida.";
    cout << " Erro." << endl;
    nErros++;
  }

  //se houver 0 argumentos, ou 1, imprime esse erro
  if(!spline.op2.empty() || !spline.op1.empty()){
    cout << "L" << l << ": Número de argumentos para instrução \"";
    cout << spline.operation << "\" inválido.";
    cout << " Erro." << endl;
    nErros++;
  }

  Cmd ccmd = cmdTable.get(spline.operation);
  object.push(to_string(ccmd.opcode), l);
  *pc += 1;

  return;
}

//instrução "copy"
if(!spline.operation.compare("copy")){

  //instruções devem ser utilizadas na seção de texto
  if(cSection == DATA){
    cout << "L" << l << ": Instrução \"" << spline.operation;
    cout << "\" utilizada em seção errada.";
    cout << " Erro." << endl;
    nErros++;
  }

  if(cSection == UNDEF){
    cout << "L" << l << ": Instrução \"" << spline.operation;
    cout << "\" utilizada em seção indefinida.";
    cout << " Erro." << endl;
    nErros++;
  }

  //se houver 0 argumentos, ou 1, imprime esse erro
  if(spline.op2.empty() || spline.op1.empty() || !spline.opN.empty()){
    cout << "L" << l << ": Número de argumentos para instrução \"";
    cout << spline.operation << "\" inválido.";
    cout << " Erro." << endl;
    nErros++;
  }

  //verificar erro léxico em argumentos
  printLex(spline, l, validtoken);

  Cmd ccmd = cmdTable.get(spline.operation);
  object.push(to_string(ccmd.opcode), l);

  string arg1;
  regex sum("[a-z_]([a-z0-9_]*)(\\+\\d+)?");

  //se o vetor contém "r", "+", "1" --> junta as strings em uma
  for(auto sp : spline.op1) arg1 += sp;

  //se não houver imediato, adiciona imediato 0, para padronizar
  // if(arg1.size()==1) arg1 += "+0";

  if(!arg1.empty())
    if(!regex_match(arg1, sum)){
      cout << "L" << l << ": Operando \"" << arg1;
      cout << "\" inválido para instrução \"copy\".";
      cout << " Erro." << endl;
      nErros++;
    }

  string arg2 = "";
  //se o vetor contém "r", "+", "1" --> junta as strings em uma
  for(auto sp : spline.op2) arg2 += sp;

  //se não houver imediato, adiciona imediato 0, para padronizar
  // if(arg2.size()==1) arg2 += "+0";

  if(!arg2.empty())
    if(!regex_match(arg2, sum)){
      cout << "L" << l << ": Operando \"" << arg2;
      cout << "\" inválido para instrução \"copy\".";
      cout << " Erro." << endl;
      nErros++;
    }

  object.push(arg1, l);
  object.push(arg2, l);

  *pc += 3;
  return;
}

//operações inválidas
if(!cmdTable.isCmd(spline.operation)){
  if(!spline.operation.empty()){
    cout << "L" << l << ": Comando inexistente \"" << spline.operation << "\".";
    cout << " Erro." << endl;
    nErros++;

    //verificar erro léxico no comando
    if(!regex_match(spline.operation, tokensyntax)){
      cout << "L" << l << ": Comando \"" << spline.operation << "\" inválido.";
      cout << " Erro." << endl;
      nErros++;
    }
  }


  //verificar erro léxico em argumentos
  vector<string>::iterator it;
  if(!spline.op1.empty()){
    for(it = spline.op1.begin(); it != spline.op1.end(); ++it)
      if(!regex_match(*it, validtoken)){
        cout << "L" << l << ": Argumento \"" << *it << "\" inválido.";
        cout << " Erro." << endl;
        nErros++;
      }
  }

  if(!spline.op2.empty()){
    for(it = spline.op2.begin(); it != spline.op2.end(); ++it)
    if(!regex_match(*it, validtoken)){
      cout << "L" << l << ": Argumento \"" << *it << "\" inválido.";
      cout << " Erro." << endl;
      nErros++;
    }
  }
  return;
}
