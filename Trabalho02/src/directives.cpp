/*
 * Alunos:
 * - Ataias Pereira Reis - 10/0093817
 * - Pedro Paulo Struck Lima - 11/0073983
 * ---------------------------------------
 */

//Diretiva "section"
if(!spline.operation.compare("section")){
  //para posterior comparação, salva seção atual em "old"
  Section old = cSection;
  //Verificação do argumento da diretiva "section" --------
  if(!spline.op1[0].compare("text")){
    cSection = TEXT;
    if(text.begin > -1) {
      cout << "L" << l << ": Seção duplicada.";
      cout << " Erro." << endl;
      nErros++;
    } else {
      text.begin = *pc;
      if(data.begin > -1) {
        data.end = *pc - 1;
      }
    }

  } else if(!spline.op1[0].compare("data")){
    cSection = DATA;
    if(data.begin > -1) {
      cout << "L" << l << ": Seção duplicada.";
      cout << " Erro." << endl;
      nErros++;
    } else {
      data.begin = *pc;
      if(text.begin > -1){
        text.end = *pc - 1;
      }
    }
    object.setDataInit(*pc);
  } else {
    cout << "L" << l << ": Seção \"" << spline.op1[0] << "\" inválida.";
    cout << " Erro." << endl;
    nErros++;
  }
  //------------------------------------------------------

  //se houver 0 ou 2 argumentos, imprime esse erro

  if(!spline.op2.empty() || spline.op1.empty() || spline.op1.size() > 1){
    cout << "L" << l << ": Número de argumentos para \"section\" inválido.";
    cout << " Erro." << endl;
    nErros++;
  }

  printLex(spline, l, validtoken);

  //tentativa de definir seção de novo
  if((old == cSection) && (cSection != UNDEF)){
    cout << "L" << l << ": Seção duplicada.";
    cout << " Erro." << endl;
    nErros++;
  }


  return;
}

//Diretiva "space"
if(!spline.operation.compare("space")){
  //argumento de space é um inteiro sem sinal de + ou -.
  //esqueci de considerar que o space pode não ter argumento, no momento precisa de argumento mesmo para o caso de operando igual a 1
  int reserve = 1;
  regex num ("\\d+");
  regex sign ("[\\+-]");
  unsigned numpos = 0;

  if(!spline.op1.empty()){

    if(!spline.op1.at(0).compare("+")){
      reserve = 1;
      numpos = 1;
    } else if(!spline.op1.at(0).compare("-")) {
      reserve = -1;
      numpos = 1;
    }

    if(regex_match(spline.op1.at(numpos), num)){
      reserve *= stoi(spline.op1.at(numpos));
    } else {
      cout << "L" << l << ": Argumento de space inválido.";
      cout << " Erro." << endl;
      nErros++;
    }

    if(spline.op1.size() > (1+numpos) || !spline.op2.empty()){
      cout << "L" << l << ": Número de argumentos de space inválido.";
      cout << " Erro." << endl;
      nErros++;
    }
  }

  printLex(spline, l, validtoken);

  //no caso de space utilizado com argumento igual a zero, uma opção é só ignorar...
  //não sei se isso é um erro que justifique não compilar... enfim...
  if(reserve <= 0){
    cout << "L" << l << ": Space utilizado com argumento inválido.";
    cout << " Erro." << endl;
    nErros++;
  }

  //space deve ser usado na seção de dados
  if(cSection == TEXT){
    cout << "L" << l << ": Space utilizado em seção errada." ;
    cout << " Erro." << endl;
    nErros++;
  }

  if(cSection == UNDEF){
    cout << "L" << l << ": Uso de diretiva \"space\" em seção não definida.";
    cout << " Erro." << endl;
    nErros++;
  }

  if(spline.label.empty()){
    cout << "L" << l << ": Uso de diretiva \"space\" sem label";
    cout << " Erro." << endl;
    nErros++;
  } else {
		//adiciona nome da variável e seu tamanho, para poder verificar erro de espaço não reservado
    if(reserve > 0)
	   variableScope[*pc] = (Var) {spline.label, (unsigned) reserve, l};
  }

  //fazer um push aqui no código, com zeros
  if(reserve>0){
    *pc += reserve;
    do {
      object.push("0", l);
    } while (--reserve);
  }
  //--------------------------------------

  return;
}

//Diretiva "const"
if(!spline.operation.compare("const")){
  //o operando op1, assim como op2, são vetores de strings
  //assim, pode-se ter como op1 "+", "0x40", ou "-", "30", ou até "24" (um elemento)
  //arg é uma variável para juntar esses elementos em uma string
  string arg;
  for(auto sp : spline.op1) arg += sp;

  int constV = 1;
  unsigned numpos = 0;

  if(!spline.op1.empty()){
    if(!spline.op1.at(0).compare("+")){
      numpos = 1;
    } else if(!spline.op1.at(0).compare("-")) {
      numpos = 1;
      constV = -1;
    }

    regex hex ("([\\+-]?)0[xX]([a-f0-9]+)");
    regex num ("[\\+-]?\\d+");

    if(spline.op1.size() > numpos) {
      if(regex_match(spline.op1.at(numpos),hex)){
          constV *= stoi(spline.op1.at(numpos), NULL, 16);
      } else if(regex_match(spline.op1.at(numpos), num)) {
          constV *= stoi(spline.op1.at(numpos));
      } else {
        cout << "L" << l << ": Constante \"" << spline.op1.at(numpos) << "\" inválida.";
        cout << " Erro." << endl;
        nErros++;
      }
    } else {
      cout << "L" << l << ": Constante \"" << arg << "\" inválida.";
      cout << " Erro." << endl;
      nErros++;
    }

    if(spline.op1.size() > (1+numpos) || !spline.op2.empty()){
      cout << "L" << l << ": Número de argumentos de const inválido.";
      cout << " Erro." << endl;
      nErros++;
    }
  } else {
    cout << "L" << l << ": Número de argumentos de const inválido.";
    cout << " Erro." << endl;
    nErros++;
  }

  regex constReg ("[\\+-]|([\\+-]?)((0x)?[0-9a-f]+)");
  printLex(spline, l, constReg);

  if(spline.label.empty()){
    cout << "L" << l << ": Uso de diretiva \"const\" sem label.";
    cout << " Erro." << endl;
    nErros++;
  } else {
		variableScope[*pc] = (Var) {spline.label, 1, l};
  }

  //const deve ser usado na seção de dados
  if(cSection == TEXT){
    cout << "L" << l << ": Const utilizado em seção errada.";
    cout << " Erro." << endl;
    nErros++;
  }

  if(cSection == UNDEF){
    cout << "L" << l << ": Uso de diretiva \"const\" em seção não definida.";
    cout << " Erro." << endl;
    nErros++;
  }


  *pc += 1; //soma 1 à posição atual
  object.push(to_string(constV), l, true);
  // --------------------------------------------------
  return;
}

//diretiva "equ"
if (!spline.operation.compare("equ")){
	//string arg;
	//for(auto sp : spline.op1) arg += sp;

	//cout <<  "L" << l <<": Diretiva EQU não pode ser processada corretamente" << endl;
	
	return;
	}

//diretiva "if"
if (!spline.operation.compare("if")){
	
	return;
	}
	
	
