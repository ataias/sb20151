/*
 * Alunos:
 * - Ataias Pereira Reis - 10/0093817
 * - Pedro Paulo Struck Lima - 11/0073983
 * ---------------------------------------
 * Assembler.cpp
 *
 * Métodos da classe Assembler, largamente utilizada no projeto
 * Contém métodos para primeira e segunda passagem, juntamente com as verificações de erros
 * A lista de métodos está disponível no arquivo Assembler.h no diretório include/
 *
 * */

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <sstream>
#include <cstdint>
#include <regex>
#include <map>
#include <Table.h>
#include <CmdTable.h>
#include <Assembler.h>
#include <set>
#include <cctype>
#include <Object.h>

using namespace std;

//compilar
//g++ -c -std=c++11 -Wall -Wextra Assembler.cpp -I ../include

//construtor da Classe
Assembler::Assembler(vector<string> code, unsigned nErrosPre){
  this->code = code;

  try {

    //define o formato de uma label do assembly inventado
    regex labelsyntax ("[a-z_]([a-z0-9_]*) :.*");
    this->labelsyntax = labelsyntax;

    //define o formato de um token valido do assembly inventado
    regex tokensyntax ("[a-z_]([a-z0-9_]*)");
    this->tokensyntax = tokensyntax;

    regex opsyntax ("\\b([a-z_](\\w*)( \\+ [0-9]+)?)");
    this->opsyntax = opsyntax;

    regex validtoken ("([a-z_][a-z0-9_]*)|([,:+-])|((0x)?[0-9]+)");
    this->validtoken = validtoken;

  } catch (const regex_error& e) {
    cout << "Erro capturado: " << e.what() << endl;
    cout << "Provavelmente o compilador usado não suporta totalmente regex do C++11." << endl;
    cout << "Utilize g++ 4.9, clang++ 3.6 ou superior." << endl;
  }

  //splitted é criada no método split
  vector<Elements> splitted;
  this->splitted = splitted;

  text = {-1, -1};
  data = {-1, -1};
  
  nErros = nErrosPre;
  
}

//split deve separar as linhas do pré-processamento em
//rótulo, operação, operando1, operando2, operandoN
//line é a string da linha, l é o número da linha
void Assembler::split(string line, unsigned l){
  stringstream stream(line);
  string token = ""; //auxiliary variable

  //Parse label
  //verifica se há label, se houver, salva na string label
  string label = "";
  regex colon(":");
  smatch m;
  int nlabel = 0;

  string str_op;

  try {
    str_op = line.substr(stream.tellg(),line.size());
    //regex search verifica se há algum sinal de dois pontos antes de consumir tokens
    if(regex_search (str_op,m,colon)){
      while(token.compare(":")){
        stream >> token;
        if(token.compare(":"))
          label.append(token.c_str());
      }
      if(cmdTable.isCmd(label)){
        cout << "L" << l << ": Label não pode ser uma palavra reservada.";
        cout << " Erro." << endl;
        nErros++;
      }
      nlabel++;
    }
  } catch (out_of_range e){
    ;
  }

  //verifica se há mais labels definidas, o que é um erro
  try {
    str_op = line.substr(stream.tellg(),line.size());
    while(regex_search (str_op,m,colon)){
      do{
        stream >> token;
        if(cmdTable.isCmd(token)){
          cout << "L" << l << ": Label não pode ser uma palavra reservada. ";
          cout << " Erro." << endl;
          nErros++;
        }
      } while(token.compare(":"));
      str_op = line.substr(stream.tellg(),line.size());
      nlabel++;
    }
  } catch (out_of_range e) {
    ;
  }

  if(nlabel > 1) {
    cout << "L" << l << ": Mais de um rótulo definido na mesma linha. ";
    cout << " Erro." << endl;
    nErros++;
  }

  //Parse operation
  //verifica se há operação, se houver, salva na string operation
  string operation;
  if(stream >> operation){
    if(!cmdTable.isCmd(operation)){
      cout << "L" << l << ": Comando \"" << operation << "\" inválido.";
      cout << " Erro." << endl;
      nErros++;
    }
  }

  //vector<string> salva operandos no vetor
  //exemplo: "r + 19" será salvo como três string nesse vector -> "r", "+" e "19".
  //se houver um segundo operando, isto estará em outro vetor de string
  const int n = 1000;
  vector<string> op[n];
  vector<string> opN;

  //este while separa operandos divididos por vírgulas para vetores específicos
  int i = 0;
  while(stream >> token){
    if(!token.compare(",")) { //conta número de vírgulas
      i++;
      continue;
    }
    op[i].push_back(token);
  }

  //os dois primeiros operandos são tratados em vector<string>'s  op1 e op2
  //os outros, são todos jogados em opN neste while abaixo
  unsigned k = 2;
  while(!op[k].empty()){
    opN.push_back("");
    for(auto str : op[k])
      opN[k-2] += str;
    k++;
  }

  //analisa número de vírgulas de acordo com o número de operandos
  //dois operandos precisam de uma vírgula para o copy
  //se houver um número diferente, dará erro
  int commas = i;
  int args = 0;

  for(auto str : op)
    if(!str.empty())
      args++;

  if(args != (commas+1) && (args >= 1) && commas > 0){
    cout << "L" << l << ": Quantidade de vírgulas incorreta.";
    cout << " Erro." << endl;
    nErros++;
  }

  splitted.push_back({label, operation, op[0], op[1], opN, l});
}

//imprime na tela os operandos separados
void Assembler::printSplitted(){
  for(auto sp : splitted){
    cout << "L" << sp.line << ":";
    if(!sp.label.empty()) cout << " L\"" << sp.label << "\"";
    if(!sp.operation.empty()) cout << " Cmd\"" << sp.operation << "\"";
    if(!sp.op1.empty()){
      cout << " Op1\"";
      for(auto str : sp.op1) cout << str;
      cout << "\"";
    }

    if(!sp.op2.empty()){
      cout << " Op2\"";
      for(auto str : sp.op2) cout << str;
      cout << "\"";
    }

    unsigned i = 0;
    if(!sp.opN.empty())
    try{
      while(!sp.opN.at(i).empty()){
          cout << " Op" << to_string(i+3) << "\"";
          cout << sp.opN.at(i);
          cout << "\"";
          i = i + 1;
      }
    } catch (out_of_range e) {
      ;
    }

    cout << endl;
  }
}

//Imprimir erros de variáveis não declaradas
void Assembler::printUndeclared(set<string> uLabels){
  for(auto sp : splitted){
    if(!sp.operation.empty()) {
      if(!sp.op1.empty()){
        for(auto undeclared : uLabels)
          if(!sp.op1[0].compare(undeclared)){
            cout << "L" << sp.line << ": Label indefinida \"" << sp.op1[0] << "\".";
            cout << " Erro." << endl;
            nErros++;
          }
      }

      if(!sp.op2.empty()){
        for(auto undeclared : uLabels)
          if(!sp.op2[0].compare(undeclared)){
            cout << "L" << sp.line << ": Label indefinida \"" << sp.op2[0] << "\".";
            cout << " Erro." << endl;
            nErros++;
          }
      }
    }

  }
}

//Primeira passagem
//gera tabela de símbolos, tabela de definições e tabela de uso...
void Assembler::pass1(){
  for(unsigned l=0; l<code.size(); l++){
    split(code[l], l+1);
  }

  #if __NOTRELEASE
  printSplitted(); cout << endl;
  #endif

  createSymTable();

  //informações úteis na tela do terminal quando estiver sendo debugado
  #if __ASSEMBLER_SYMBOLS_SHOW
  sym.print();
  #endif

}

//Segunda passagem
//deve gerar código objeto
void Assembler::pass2(){

  //imprimir código objeto antes da troca das labels
  #if __ASSEMBLER_OBJECT_SHOW
    cout << "\nCódigo objeto antes do offset.";
    object.print();
  #endif

  //deve atualizar a tabela de símbolos caso a seção de dados venha antes da seção de texto
  if(data.end == -1) data.end = object.size() - 1;
  if(text.end == -1) text.end = object.size() - 1;
  unsigned textSize = text.end - text.begin;

  if(text.begin > data.begin){
    vector<Symbol> symT = sym.getVector();
    for(unsigned i = 0; i< symT.size(); i++){
      if(symT.at(i).section == TEXT){
        sym.set(symT.at(i).label, symT.at(i).value - text.begin);
      }
      if(symT.at(i).section == DATA){
        sym.set(symT.at(i).label, symT.at(i).value + textSize + 1);
      }
    }
  }

  //seção de dados vai para o final se não já estava
  object.reorder(text, data);
  if(text.begin > data.begin){
    data.begin = text.end - text.begin + 1;
    data.end = object.size() - 1;
    text.end = data.begin - 1;
    text.begin = 0;
  }


  #if __ASSEMBLER_SYMBOLS_SHOW
  cout << "Tabela de símbolos depois do offset.";
  sym.print();
  #endif


  //trocar labels pelos valores de endereço
  vector<Symbol> symv = sym.getVector();
  smatch m;
  string address;
  string objcode;
  for(auto symbol : symv){
    string labelReg = "\\b"+symbol.label+"\\b"; //regex para dar match em uma label
    regex var (labelReg);
    for(unsigned i = 0; i < object.size(); i++){
      objcode = object.get(i);
      if(regex_search(objcode,m,var)){
        address = object.get(i);
        unsigned sz = m[0].str().size();
        address.replace(0, sz, to_string(sym.get(symbol.label)));
        object.set(address, i);
      }
    }
  }

  //Verificar labels que não foram definidos
  set<string> uLabels;
  for(unsigned i = 0; i <object.size(); i++){
    objcode = object.get(i);
    long pos = objcode.find("+");
    if(pos > -1) objcode.erase(pos, objcode.size() - 1);
    if(regex_search(objcode, m, tokensyntax)){
      uLabels.insert(objcode);
    }
  }

  //agora que se tem a variável uLabels com variáveis não declaradas,
  //deve-se imprimir a linha onde são utilizadas
  printUndeclared(uLabels);

  //esta seção soma endereços, por exemplo, "0+1"
  regex sum ("\\d+\\+\\d+");
  regex num ("\\d+");
  Var varScope;
  for(unsigned i = 0; i < object.size(); i++){
    objcode = object.get(i);
    if(regex_match(objcode, sum)){
      regex_search(objcode,m,num);
      string num1 = m[0].str();
      string num2 = object.get(i).erase(0,num1.size());
      int addr = stoi(num1) + stoi(num2);
      //verificação de erro out_of_scope
      try {
        varScope = variableScope.at(stoul(num1));
        if(!(varScope.size > stoul(num2))) {
          cout << "L" << object.getLine(i) << ": Tentativa de acesso a endereço não reservado ";
          cout << "para variável \"" << varScope.label << "\" definida em L";
          cout << varScope.line;
          cout << " do arquivo. " << endl;
          nErros++;
        }
      } catch (out_of_range e){
        ;
      }

      object.set(to_string(addr), i);
    }
  }

  #if __ASSEMBLER_OBJECT_SHOW
  cout << "\nCódigo objeto reordenado";
  print();
  #endif

  #if __ASSEMBLER_BOUNDS_SHOW
  cout << dec << "text.begin = " << text.begin << endl;
  cout << dec << "text.end = " << text.end << endl;
  cout << dec << "data.begin = " << data.begin << endl;
  cout << dec << "data.end = " << data.end << endl;
  #endif

  try { verifyDivision(); } catch (const char *e){ ; }
  try { constCheck();  } catch (const char *e) { ; }
  try { memoryCheck(); } catch (const char *e) { ; }
  try { jumpCheck(); } catch (const char *e) { ; }

  //liberar memória
  symv.clear();
  symv.shrink_to_fit();

  //código objeto depois da substituição de símbolos
  #if __ASSEMBLER_OBJECT_SHOW
    cout << "\nCódigo objeto final.";
    object.print();
  #endif
}

//gerar tabela de símbolos
void Assembler::createSymTable(){
  int pc = 0; //position counter
  cSection = UNDEF; //inicialmente, seção indefinida

  for(auto sp : splitted){
    if(!sp.label.empty()){ //se houver uma label nesta linha
      try { //tente obtê-la na tabela de símbolos
        sym.get(sp.label); //emite exceção caso não exista
        cout << "L" << sp.line << ": Label redefinido.";
        cout << " Erro." << endl;
        nErros++;
        //aumente o número de erros semânticos, label redefinido
      } catch (const char* e) { //símbolo não definido, hora de definir
        sym.push(sp.label, pc, cSection);
        if(!regex_match(sp.label, tokensyntax)){
          cout << "L" << sp.line << ": Label inválido.";
          cout << " Erro." << endl;
          nErros++;
        }
      }
    }
    exeCmd(sp, sp.line, &pc);
  }
}

bool Assembler::writeFile(string filename){
  if(nErros){
    cout << endl << "Erros encontrados: " << nErros << endl;
    cout << "Arquivo objeto não pôde ser gerado." << endl;
    return false;
  } else {
    //gerar arquivo objeto filename
    ofstream objectFile;
    objectFile.open(filename);
    string aux;

    for(unsigned i=0; i < object.size(); i++)
      objectFile << object.get(i) << " ";

    return true;
  }
}

//verificação de divisão por zero deve ser feita ao término da segunda passagem
//com todos os símbolos já trocados
void Assembler::verifyDivision(){
  int op = 0; //opcode

  Cmd command;

  unsigned i = text.begin;
  while(i < (unsigned) text.end) {
    op = stoi(object.get(i));
    try {
      //exceção ocorre se op não for uma instrução válida
      command = cmdTable.get(op);
      if(!command.mnemonic.compare("div")){
        try {
          if(object.isConst(stoi(object.get(i+1)))){ //caso seja, verifica se é zero
            if(stoi(object.get(stoi(object.get(i+1)))) == 0){
              cout << "L" << object.getLine(i) << ": Divisão por zero.";
              cout << " Erro." << endl;
              nErros++;
            }
          }
        } catch (invalid_argument e) {
          i = i + command.operands + 1;
        }
      }
    } catch (out_of_range e){
      ;
    }
    i = i + command.operands + 1;
  }
}

//verificar modificação de constante
void Assembler::constCheck(){
  int op = 0; //opcode

  regex cmd1op ("store|input");
  regex cmd2op ("copy");
  Cmd command;

  //iterar até antes da seção de dados
  unsigned i = text.begin;
  while(i < (unsigned) text.end) {
    op = stoi(object.get(i));
    try {
      //exceção ocorre se op não for uma instrução válida
      command = cmdTable.get(op);
      try {
        if(regex_match(command.mnemonic, cmd1op))
          if(object.isConst(stoi(object.get(i+1)))) {
            cout << "L" << object.getLine(i) << ": Modificação de valor constante.";
            cout << " Erro." << endl;
            nErros++;
          }

        if(regex_match(command.mnemonic, cmd2op))
          if(object.isConst(stoi(object.get(i+2)))){
            cout << "L" << object.getLine(i) << ": Modificação de valor constante.";
            cout << " Erro no endereço " << object.get(i+2) << endl;
            nErros++;
          }
      } catch (invalid_argument e) {
        ;
      }
      i = i + command.operands + 1;
    } catch (out_of_range e){
      i = i + command.operands + 1;
    }

  }
}

//essa função verifica operandos de funções que leem ou escrevem na memória
void Assembler::memoryCheck(){
  int op = 0; //opcode

  regex cmd1op ("add|sub|mult|div|load|store|input|output");
  regex cmd2op ("copy");
  Cmd command;

  unsigned i = text.begin;

  while(i < (unsigned) text.end) {
    op = stoi(object.get(i)); //opcode da operação
    try { //exceção ocorre se op não for uma instrução válida
      command = cmdTable.get(op);
      unsigned code_end = text.end > data.end ? text.end : data.end;
      unsigned address;
      try {
        address = stoi(object.get(i+1));
      } catch (invalid_argument e) {
        //provavelmente label indefinido se chegou aqui no catch
        i = i + command.operands + 1;
        return;
      }
      if(regex_match(command.mnemonic, cmd1op)){

        if(address > code_end) {
          cout << "L" << object.getLine(i) << ": Endereço de memória não reservado.";
          cout << " Erro." << endl;
          nErros++;
        }

        if(address >= (unsigned) text.begin && address <= (unsigned) text.end) {
          cout << "L" << object.getLine(i) << ": Endereço de instruções ao invés de memória.";
          cout << " Erro." << endl;
          nErros++;
        }
      }
      if(regex_match(command.mnemonic, cmd2op)){

        try {
          address = stoi(object.get(i+2));
        } catch (invalid_argument e) {
          //provavelmente label indefinido se chegou aqui no catch
          i = i + command.operands + 1;
          return;
        }
        if(address > code_end){
          cout << "L" << object.getLine(i) << ": Endereço de memória não reservado.";
          cout << " Erro." << endl;
          nErros++;
        }

        if(address >= (unsigned) text.begin && address <= (unsigned) text.end) {
          cout << "L" << object.getLine(i);
          cout << ": Argumento 2. Endereço de instruções ao invés de memória.";
          cout << " Erro." << endl;
          nErros++;
        }
      }
      i = i + command.operands + 1;
    } catch (out_of_range e){
      i = i + command.operands + 1;
    }

  }
}

void Assembler::jumpCheck(){
  int op = 0; //operação

  Cmd command;

  unsigned i = text.begin;
  regex num ("([\\+-]?)\\d+");
  while(i < (unsigned) text.end) {
    op = stoi(object.get(i)); //opcode da operação
    try { //exceção ocorre se op não for uma instrução válida
      command = cmdTable.get(op);
      unsigned address;
      try {
        address = stoi(object.get(i+1));
        if(!regex_match(object.get(i+1), num)){
          i = i + command.operands + 1;
          continue;
        }
      } catch (invalid_argument e) {
        //provavelmente label indefinido se chegou aqui no catch
        i = i + command.operands + 1;
        continue;
      }
      if(cmdTable.isJump(command.mnemonic)){
        unsigned code_end = text.end > data.end ? text.end : data.end;
        if(address > code_end) {
          cout << "L" << object.getLine(i);
          cout << ": Pulo para endereço de memória não reservado.";
          cout << " Erro." << endl;
          nErros++;
        }

        if(address >= (unsigned) data.begin && address <= (unsigned) data.end) {
          cout << "L" << object.getLine(i);
          cout << ": Pulo inválido para endereço de memória ao invés de instruções.";
          cout << " Erro." << endl;
          nErros++;
        }
      }
      i = i + command.operands + 1;
    } catch (out_of_range e){
      i = i + command.operands + 1;
    }

  }
}

vector<Elements> Assembler::getSplitted(){
  return splitted;
}

unsigned Assembler::getNumberOfErrors(){
  return nErros;
}

#include "exeCmd.cpp"
