/*
 * Alunos:
 * - Ataias Pereira Reis - 10/0093817
 * - Pedro Paulo Struck Lima - 11/0073983
 * ---------------------------------------
 * Asm32.cpp
 *
 * Métodos da classe Asm32, principal responsável pela tradução para assembly e código máquina
 * A lista de métodos está disponível no arquivo Asm32.h no diretório include/
 *
 * Esta classe demora um pouco mais para compilar, imagino que seja por causa
 * da classe "functional", mas não há certeza
 *
 * */

#include <iostream>
#include <string>
#include <fstream>
#include <functional>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <regex>
#include <Asm32.h>

using namespace std;

//Construtor da classe
//splitted: linhas separadas em elementos, é uma saída da classe Assembler

//Para a geração do elf, este construtor também requer os argumentos:
//text_begin: o endereço do início da seção de texto, deve ser definido
//data_begin é o endereço do início da seção de dados, deve ser definido
Asm32::Asm32(vector<Elements> splitted, unsigned text_begin, unsigned data_begin){
  sym_text.offset(text_begin);
  sym_data.offset(data_begin);

  //Adiciona cabeçalho à seção de texto
  function<void (string)> t = [this] (string code) {
    this->text.push_back(code);
  };

  inv4Bytes = [] (u_long addr) {
    return ((addr & 0xFF000000) >> 24) + ((addr & 0x00FF0000) >> 8) + ((addr & 0x0000FF00) << 8) + ((addr & 0x000000FF) << 24);
  };

  pc = 0;
  data_pc = 0;
  u_long lpc = 0;

  //este construtor só preenche o header do arquivo assembly e do código máquina
  t("section .text");
  t("LerUInt:"); sym_text.push("LerUInt", pc);
  t("call clearBuffer"); pc+=5;
  lpc = pc;
  text_mcode.push_back([this, lpc] () -> AsmCode32 {
    u_long addr = (unsigned) ((long long) (sym_text.get("clearBuffer") - (long long) (sym_text.getOffset() + lpc)));
    return {0xe800000000 | inv4Bytes(addr), 5};
  });
  t("push ebx"); pc += 1;
  text_mcode.push_back([this] () -> AsmCode32 {return {0x53, 1};});
  t("push ecx"); pc += 1;
  text_mcode.push_back([this] () -> AsmCode32 {return {0x51, 1};});
  t("push edx"); pc += 1;
  text_mcode.push_back([this] () -> AsmCode32 {return {0x52, 1};});
  t("push esi"); pc += 1;
  text_mcode.push_back([this] () -> AsmCode32 {return {0x56, 1};});

  t("push edi"); pc+=1;
  text_mcode.push_back([this] () -> AsmCode32 {return {0x57, 1};});


  t("mov edx, 16 ;getStr buffer, SIZE_B"); pc += 5;
  text_mcode.push_back([this] () -> AsmCode32 {return {0xba10000000 , 5};});

  t("mov ecx, buffer"); pc += 5;
  text_mcode.push_back([this] () -> AsmCode32 {
    u_long addr = sym_data.get("buffer");
    return {0xb900000000 | inv4Bytes(addr), 5};
  });
  t("mov ebx, 0; teclado"); pc += 5;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xbb00000000, 5};});
  t("mov eax, 3 ;sys_read"); pc += 5;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xb803000000, 5};});
  t("int 0x80"); pc += 2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xcd80, 2};});
  t("");
  t("sub eax, eax ; resultado final"); pc += 2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x29c0, 2};});
  t("sub ebx, ebx ; valor de um caractere"); pc += 2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x29db, 2};});
  t("mov ecx, buffer ;índice"); pc += 5;
  text_mcode.push_back([this] () -> AsmCode32 {
    u_long addr = sym_data.get("buffer");
    return {0xb900000000 | inv4Bytes(addr), 5};
  });
  t("");
  t(";verificar sinal");
  t("mov edi, 1"); pc+=5;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xbf01000000, 5};});
  t("mov bl, [ecx]"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x8a19, 2};});
  t("cmp bl, '+'"); pc+=3;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x80fb2b, 3};});
  t("je LerHasSign"); pc+=2; //pulo curto relativo
  text_mcode.push_back([this] () -> AsmCode32 {
    return {0x740c, 2};
  });

  t("cmp bl, '-'"); pc+=3;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x80fb2d, 3};});

  t("je LerNegative"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x7402, 2};});

  t("jmp loopLer"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xeb06, 2};});

  t("LerNegative:"); sym_text.push("LerNegative", pc);
  t("mov edi, -1"); pc+=5;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xbfffffffff, 5};});

  t("LerHasSign:"); sym_text.push("LerHasSign", pc);
  t("inc ecx"); pc+=1;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x41, 1};});

  t("");
  t("loopLer:"); sym_text.push("loopLer", pc);
  t("mov bl, [ecx]"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x8a19, 2};});
  t("cmp bl, '0'"); pc+=3;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x80fb30, 3};});
  t("jb doneLer"); pc+=2; //pulo relativo, tudo bem
  text_mcode.push_back([this] () -> AsmCode32 { return {0x7216, 2};});
  t("cmp bl, '9'"); pc+=3;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x80fb39, 3};});
  t("ja doneLer"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x7711, 2};});
  t("sub bl, '0' ;transforma em número"); pc+=3;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x80eb30, 3};});
  t("");
  t(";multiplica por 10");
  t("shl eax, 1"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xd1e0, 2};});
  t("mov esi, eax"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x89c6, 2};});
  t("shl eax, 2"); pc+=3;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xc1e002, 3};});
  t("add eax, esi"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x01f0, 2};});
  t("");
  t("add eax, ebx"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x01d8, 2};});
  t("inc ecx"); pc+=1;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x41, 1};});
  t("jmp loopLer"); pc+=2; //pulo relativo também
  text_mcode.push_back([this] () -> AsmCode32 { return {0xebe3, 2};});
  t("doneLer:"); sym_text.push("doneLer", pc);
  t("imul eax, edi"); pc+=3;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x0fafc7, 3};});


  t("pop edi"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x5f, 1};});

  t("pop esi"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x5e, 1};});
  t("pop edx"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x5a, 1};});
  t("pop ecx"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x59, 1};});
  t("pop ebx"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x5b, 1};});
  t("ret"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xc3, 1};});
  t("");
  t("EscreverUInt:"); sym_text.push("EscreverUInt", pc);
  text.push_back("call clearBuffer"); pc+=5;
  lpc = pc;
  text_mcode.push_back([this, lpc] () -> AsmCode32 {
    u_long addr = (unsigned) ((long long) (sym_text.get("clearBuffer") - (long long) (sym_text.getOffset() + lpc)));
    return {0xe800000000 | inv4Bytes(addr), 5};
  });
  t("push eax"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x50, 1};});
  t("push ebx"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x53, 1};});
  t("push ecx"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x51, 1};});
  t("push edx"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x52, 1};});
  t("");
  t("; verificar se há sinal para ser escrito");
  t("push esi"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x56, 1};});
  t("mov esi, 0"); pc+=5;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xbe00000000, 5};});
  t("sub ebx, ebx"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x29db, 2};});
  t("cmp eax, ebx"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x39d8, 2};});

  t("jl EscreverNegative"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x7c02, 2};});

  t("jmp EscreverPositivo"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xeb10, 2};});
  t("");
  t("EscreverNegative:"); sym_text.push("EscreverNegative", pc);
  t("mov ebx, 0xffffffff"); pc+=5;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xbbffffffff, 5};});
  t("mov ecx, eax"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x89c1, 2};});

  t("xor ecx, ebx"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x31d9, 2};});

  t("add ecx, 1"); pc+=3;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x83c101, 3};});

  t("mov esi, eax"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x89c6, 2};});

  t("mov eax, ecx"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x89c8, 2};});
  t("");
  t("EscreverPositivo:"); sym_text.push("EscreverPositivo", pc);

  t("push 0xa ;fim do número"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x6a0a, 2};});
  t("mov ecx, 10"); pc+=5;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xb90a000000, 5};});
  t("toChar:"); sym_text.push("toChar", pc);
  t("div ecx"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xf7f1, 2};});
  t("add edx, '0'"); pc+=3;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x83c230, 3};});
  t("push edx"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x52, 1};});
  t("sub edx, edx"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x29d2, 2};});
  t("cmp eax, edx"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x39d0, 2};});
  t("jnz toChar"); pc+=2; //relativo também
  text_mcode.push_back([this] () -> AsmCode32 { return {0x75f4, 2};});

  t(";verificar se há sinal para colocar no buffer");

  t("sub ebx, ebx"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x29db, 2};});
  t("cmp esi, ebx"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x39de, 2};});
  t("jnz toBufferNegativo"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x7502, 2};});
  t("jmp toBufferPositivo"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xeb02, 2};});

  t("toBufferNegativo:"); sym_text.push("toBufferNegativo", pc);
  t("push dword '-'"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x6a2d, 2};});

  t("toBufferPositivo:"); sym_text.push("toBufferPositivo", pc);

  t("mov ecx, buffer"); pc+=5;
  text_mcode.push_back([this] () -> AsmCode32 {
    u_long addr = sym_data.get("buffer");
    return {0xb900000000 | inv4Bytes(addr), 5};
  });
  t("toBuffer:"); sym_text.push("toBuffer", pc);
  t("pop eax"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x58, 1};});
  t("mov byte [ecx], al"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x8801, 2};});
  t("inc ecx"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x41, 1};});
  t("mov edx, 0xa ;endl"); pc+=5;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xba0a000000, 5};});
  t("cmp al, dl"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x38d0, 2};});
  t("jnz toBuffer"); pc+=2; //também relativo
  text_mcode.push_back([this] () -> AsmCode32 { return {0x75f3, 2};});

  t("mov byte [ecx], 0"); pc+=3; //também relativo
  text_mcode.push_back([this] () -> AsmCode32 { return {0xc60100, 3};});


  t("mov edx, 16 ;length"); pc+=5;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xba10000000, 5};});
  t("mov ecx, buffer ;message pointer"); pc+=5;
  text_mcode.push_back([this] () -> AsmCode32 {
    u_long addr = sym_data.get("buffer");
    return {0xb900000000 | inv4Bytes(addr), 5};
  });
  t("mov ebx, 1 ; stdout"); pc+=5;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xbb01000000, 5};});
  t("mov eax, 4 ;sys_write"); pc+=5;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xb804000000, 5};});
  t("int 0x80"); pc+=2;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xcd80, 2};});
  t("");
  t("pop esi"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x5e, 1};});

  t("pop edx"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x5a, 1};});
  t("pop ecx"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x59, 1};});
  t("pop ebx"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x5b, 1};});
  t("pop eax"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x58, 1};});
  t("ret"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0xc3, 1};});

  t("");
  t("clearBuffer:"); sym_text.push("clearBuffer", pc);
  t("push ecx"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 { return {0x51, 1};});

  t("mov ecx, buffer"); pc+=5;
  text_mcode.push_back([this] () -> AsmCode32 {
    u_long addr = sym_data.get("buffer");
    return {0xb900000000 | inv4Bytes(addr), 5};
  });

  t("mov dword [ecx], 0"); pc+=6;
  text_mcode.push_back([this] () -> AsmCode32 {
    return {0xc70100000000, 6};
  });

  t("add ecx, 4"); pc+=3;
  text_mcode.push_back([this] () -> AsmCode32 {
    return {0x83c104, 3};
  });
  t("mov dword [ecx], 0"); pc+=6;
  text_mcode.push_back([this] () -> AsmCode32 {
    return {0xc70100000000, 6};
  });

  t("add ecx, 4"); pc+=3;
  text_mcode.push_back([this] () -> AsmCode32 {
    return {0x83c104, 3};
  });
  t("mov dword [ecx], 0"); pc+=6;
  text_mcode.push_back([this] () -> AsmCode32 {
    return {0xc70100000000, 6};
  });

  t("add ecx, 4"); pc+=3;
  text_mcode.push_back([this] () -> AsmCode32 {
    return {0x83c104, 3};
  });
  t("mov dword [ecx], 0"); pc+=6;
  text_mcode.push_back([this] () -> AsmCode32 {
    return {0xc70100000000, 6};
  });

  t("pop ecx"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 {
    return {0x59, 1};
  });
  t("ret"); pc++;
  text_mcode.push_back([this] () -> AsmCode32 {
    return {0xc3, 1};
  });

  t("");
  t("global _start");
  t("_start:"); sym_text.push("_start", pc);

  //Adiciona cabeçalho à seção de dados
  data.push_back("");
  data.push_back("section .data");
  data.push_back("buffer: db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0");
  sym_data.push("buffer", data_pc);
  data_pc += 16;
  for(unsigned i = 0; i < 16; i++){
    data_mcode.push_back([this] () -> AsmCode32 {
      return {0x00, 1};
    });
  }

  this->splitted = splitted;

}


void Asm32::print(){
  //imprimir seções de texto e dados
  for(auto it : text) cout << it << endl;
  for(auto it : data) cout << it << endl;
}

//traduz linha do assembly inventado para linhas de assembly ia32
void Asm32::translateLine(Elements asml){

  unsigned lpc = pc;
  function<string (vector<string>)> cat = [] (vector<string> v) -> string {
    string arg = "";
    for(auto it : v)
      arg += it;
    return arg;
  };

  //verificar se é label na seção de texto
  function<void ()> ltext = [this, asml, lpc] () {
    if(!asml.label.empty()){
      text.push_back(asml.label + ":");
      sym_text.push(asml.label, lpc);
    }
  };

  if(!asml.label.empty() && asml.operation.empty()){
    text.push_back(asml.label + ":");
    sym_text.push(asml.label, lpc);
    return;
  }


  string op1 = cat(asml.op1);
  string op2 = cat(asml.op2);

  function<bool (string)> is = [asml] (string operation) -> bool {
    return !asml.operation.compare(operation);
  };

  unsigned imm = 0;

  if(is("add")){
    ltext();
    if(asml.op1.size() == 1) op1 += "+0";
    else imm += 4*stoi(asml.op1.at(2));

    text.push_back("add eax, dword [" + op1 + "*4]");
    text_mcode.push_back([this, imm, asml] () -> AsmCode32 {
      u_long addr = sym_data.get(asml.op1.at(0)) + imm;
      return {0x030500000000 | inv4Bytes(addr), 6};
    });
    pc+=6;

  }

  if(is("sub")){
    ltext();
    if(asml.op1.size() == 1) op1 += "+0";
    else imm += 4*stoi(asml.op1.at(2));
    text.push_back("sub eax, dword [" + op1 + "*4]");
    text_mcode.push_back([this, imm, asml] () -> AsmCode32 {
      u_long addr = sym_data.get(asml.op1.at(0)) + imm;
      return {0x2b0500000000 | inv4Bytes(addr), 6};
    });
    pc+=6;
  }

  if(is("mult")){
    ltext();
    text.push_back("push edx"); pc++;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x52, 1}; });

    if(asml.op1.size() == 1) op1 += "+0";
    else imm += 4*stoi(asml.op1.at(2));

    text.push_back("imul dword ["+op1+"*4]"); pc+=6;
    text_mcode.push_back([this, imm, asml] () -> AsmCode32 {
      u_long addr = sym_data.get(asml.op1.at(0)) + imm;
      return {0xf72d00000000 | inv4Bytes(addr), 6};
    });
    text.push_back("pop edx"); pc++;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x5a, 1}; });
  }

  if(is("div")){
    ltext();
    text.push_back("push edx"); pc++;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x52, 1}; });
    text.push_back("cdq"); pc+=1;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x99, 1}; });

    if(asml.op1.size() == 1) op1 += "+0";
    else imm += 4*stoi(asml.op1.at(2));

    text.push_back("idiv dword ["+op1+"*4]");
    pc+=6;
    text_mcode.push_back([this, imm, asml] () -> AsmCode32 {
      u_long addr = sym_data.get(asml.op1.at(0)) + imm;
      return {0xf73d00000000 | inv4Bytes(addr), 6};
    });


    text.push_back("pop edx"); pc++;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x5a, 1}; });
  }

  //o código montado pelo nasm que eu estava testando tinha basicamente pulos curtos
  //assim que o código ficar maior, podemos necessitar de pulos próximos (2 bytes)
  //abaixo só se usam pulos próximos (5 bytes)
  if(is("jmp")){
    ltext();
    if(asml.op1.size() == 1) op1 += "+0";
    else imm += 4*stoi(asml.op1.at(2));

    text.push_back("jmp " + op1); pc+=5; lpc = pc;
    text_mcode.push_back([this, imm, asml, lpc] () -> AsmCode32 {
      unsigned addr = (unsigned) ((long long)(sym_text.getNO(asml.op1.at(0))+imm) - (long long) (lpc));
      return {0xe900000000 | inv4Bytes(addr), 5};
    });

  }

  if(is("jmpn")){
    ltext();
    text.push_back("push ebx"); pc++;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x53, 1}; });

    text.push_back("sub ebx, ebx"); pc+=2;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x29db, 2}; });
    text.push_back("cmp eax, ebx"); pc+=2;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x39d8, 2}; });

    if(asml.op1.size() == 1) op1 += "+0";
    else imm += 4*stoi(asml.op1.at(2));

    text.push_back("pop ebx"); pc++;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x5b, 1}; });

    text.push_back("jl "+ op1); pc+=6; lpc = pc; //pulo próximo
    text_mcode.push_back([this, imm, asml, lpc] () -> AsmCode32 {
      unsigned addr = (unsigned) ((long long)(sym_text.getNO(asml.op1.at(0))+imm) - (long long) (lpc));
      return {0x0f8c00000000 | inv4Bytes(addr), 6};
    });

  }

  if(is("jmpp")){
    ltext();
    text.push_back("push ebx"); pc++;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x53, 1}; });

    text.push_back("sub ebx, ebx"); pc+=2;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x29db, 2}; });

    text.push_back("cmp eax, ebx"); pc+=2;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x39d8, 2}; });

    if(asml.op1.size() == 1) op1 += "+0";
    else imm += 4*stoi(asml.op1.at(2));

    text.push_back("pop ebx"); pc++;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x5b, 1}; });

    text.push_back("jg " + op1); pc+=6; lpc = pc;
    text_mcode.push_back([this, imm, asml, lpc] () -> AsmCode32 {
      unsigned addr = (unsigned) ((long long)(sym_text.getNO(asml.op1.at(0))+imm) - (long long) (lpc));
      return {0x0f8f00000000 | inv4Bytes(addr), 6};
    });

  }

  if(is("jmpz")){
    ltext();
    // text.push_back("push ebx"); pc++;
    // text_mcode.push_back([this] () -> AsmCode32 { return {0x53, 1}; });

    text.push_back("sub ebx, ebx"); pc+=2;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x29db, 2}; });

    text.push_back("cmp eax, ebx"); pc+=2;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x39d8, 2}; });

    if(asml.op1.size() == 1) op1 += "+0";
    else imm += 4*stoi(asml.op1.at(2));

    text.push_back("je " + op1); pc+=6; lpc = pc;
    text_mcode.push_back([this, imm, asml, lpc] () -> AsmCode32 {
      unsigned addr = (unsigned) ((long long)(sym_text.getNO(asml.op1.at(0))+imm) - (long long) (lpc));
      return {0x0f8400000000 | inv4Bytes(addr), 6};
    });

  }

  if(is("copy")){
    ltext();
    text.push_back("push eax"); pc++;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x50, 1}; });
	
	unsigned imm1=0;
	unsigned imm2=0;
	
    if(asml.op1.size() == 1) op1 += "+0";
    else imm1 += 4*stoi(asml.op1.at(2));

    text.push_back("and eax, 0"); pc+=3;
    text_mcode.push_back([] () -> AsmCode32 {
      return {0x83e000, 3};
    });

    text.push_back("or eax, ["+op1+"*4]"); pc+=6;
    text_mcode.push_back([this, imm1, asml] () -> AsmCode32 {
      u_long addr = sym_data.get(asml.op1.at(0)) + imm1;
      return {0x0b0500000000 | inv4Bytes(addr), 6};
    });

    if(asml.op2.size() == 1) op2 += "+0";
    else imm2 += 4*stoi(asml.op2.at(2));
    
     text.push_back("and dword ["+op2+"*4], 0x0"); pc+=7;
    text_mcode.push_back([this, imm2, asml] () -> AsmCode32 {
      unsigned addr = sym_data.get(asml.op2.at(0)) + imm2;
      return {(0x83250000000000 | ((u_long) inv4Bytes(addr)) << 8), 7};
    });
    
    text.push_back("or dword ["+op2+"*4], eax"); pc+=6;
    text_mcode.push_back([this, imm2, asml] () -> AsmCode32 {
      unsigned addr = sym_data.get(asml.op2.at(0)) + imm2;
      return {(0x090500000000 | inv4Bytes(addr)), 6};
    });

    text.push_back("pop eax"); pc++;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x58, 1}; });
    return;
  }

  if(is("load")){
    ltext();
    if(asml.op1.size() == 1) op1 += "+0";
    else imm += 4*stoi(asml.op1.at(2));

    text.push_back("and eax, 0"); pc+=3;
    text_mcode.push_back([] () -> AsmCode32 {
      return {0x83e000, 3};
    });

    text.push_back("or eax, ["+op1+"*4]"); pc+=6;
    text_mcode.push_back([this, imm, asml] () -> AsmCode32 {
      u_long addr = sym_data.get(asml.op1.at(0)) + imm;
      return {0x0b0500000000 | inv4Bytes(addr), 6};
    });
}

  if(is("store")){
    ltext();
    if(asml.op1.size() == 1) op1 += "+0";
    else imm += 4*stoi(asml.op1.at(2));

    text.push_back("and dword ["+op1+"*4], 0x0"); pc+=7;
    text_mcode.push_back([this, imm, asml] () -> AsmCode32 {
      unsigned addr = sym_data.get(asml.op1.at(0)) + imm;
      return {(0x83250000000000 | ((u_long) inv4Bytes(addr)) << 8), 7};
    });
    text.push_back("or dword ["+op1+"*4], eax"); pc+=6;
    text_mcode.push_back([this, imm, asml] () -> AsmCode32 {
      unsigned addr = sym_data.get(asml.op1.at(0)) + imm;
      return {(0x090500000000 | inv4Bytes(addr)), 6};
    });
  }

  if(is("input")){
    ltext();

    text.push_back("push eax"); pc++;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x50, 1}; });

    text.push_back("call LerUInt"); pc+=5;
    u_long lpc = pc;
    text_mcode.push_back([this, imm, asml, lpc] () -> AsmCode32 {
      u_long addr = (unsigned) ((long long) (sym_text.get("LerUInt") - (long long) (sym_text.getOffset() + lpc)));
      return {0xe800000000 | inv4Bytes(addr), 5};
    });

    if(asml.op1.size() == 1) op1 += "+0";
    else imm += 4*stoi(asml.op1.at(2));

    text.push_back("and dword ["+op1+"*4], 0x0"); pc+=7;
    text_mcode.push_back([this, imm, asml] () -> AsmCode32 {
      unsigned addr = sym_data.get(asml.op1.at(0)) + imm;
      return {(0x83250000000000 | ((u_long) inv4Bytes(addr)) << 8), 7};
    });
    text.push_back("or dword ["+op1+"*4], eax"); pc+=6;
    text_mcode.push_back([this, imm, asml] () -> AsmCode32 {
      unsigned addr = sym_data.get(asml.op1.at(0)) + imm;
      return {(0x090500000000 | inv4Bytes(addr)), 6};
    });

    text.push_back("pop eax"); pc++;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x58, 1}; });
  }

  if(is("output")){
    ltext();
    text.push_back("push eax"); pc++;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x50, 1}; });

    if(asml.op1.size() == 1) op1 += "+0";
    else imm += 4*stoi(asml.op1.at(2));

    //por algum motivo mov eax não funciona no código objeto...
    //vou usar "and" e "or"
    text.push_back("and eax, 0"); pc+=3;
    text_mcode.push_back([] () -> AsmCode32 {
      return {0x83e000, 3};
    });

    text.push_back("or eax, ["+op1+"*4]"); pc+=6;
    text_mcode.push_back([this, imm, asml] () -> AsmCode32 {
      u_long addr = sym_data.get(asml.op1.at(0)) + imm;
      return {0x0b0500000000 | inv4Bytes(addr), 6};
    });


    text.push_back("call EscreverUInt"); pc+=5;
    u_long lpc = pc;
    text_mcode.push_back([this, imm, asml, lpc] () -> AsmCode32 {
      u_long addr = (unsigned) ((long long) (sym_text.get("EscreverUInt") - (long long) (sym_text.getOffset() + lpc)));
      return {0xe800000000 | inv4Bytes(addr), 5};
    });

    text.push_back("pop eax"); pc++;
    text_mcode.push_back([this] () -> AsmCode32 { return {0x58, 1}; });

  }

  if(is("stop")){
    ltext();
    text.push_back("mov eax, 1"); pc+=5;
    text_mcode.push_back([this] () -> AsmCode32 {
      return {0xb801000000, 5};
    });

    text.push_back("mov ebx, 0"); pc+=5;
    text_mcode.push_back([this] () -> AsmCode32 {
      return {0xbb00000000, 5};
    });

    text.push_back("int 0x80"); pc+=2;
    text_mcode.push_back([this] () -> AsmCode32 {
      return {0xcd80, 2};
    });
  }

  if(is("space")){
    if(op1.empty()) op1 = "1";
    int n = stoi(op1);
    string aux = "";
    for(int i = 0; i < n - 1; i++){
      aux += "0, ";
      data_mcode.push_back([this] () -> AsmCode32 { return {0x00000000, 4}; });
    }
    aux += "0";
    data_mcode.push_back([this] () -> AsmCode32 { return {0x00000000, 4}; });

    data.push_back(asml.label+": dd "+aux);
    sym_data.push(asml.label, data_pc);
    data_pc += stoi(op1)*4;
  }

  if(is("const")){
    data.push_back(asml.label+": dd "+op1);
    sym_data.push(asml.label, data_pc);
    data_pc += 4;

    regex hex ("([\\+-]?)0[xX]([a-f0-9]+)");

    u_long Nop1 = 0;
    if(regex_match(op1,hex)){
        Nop1= stoi(op1, NULL, 16);
    } else {
        Nop1 = stoi(op1);
    }

    data_mcode.push_back([this, Nop1] () -> AsmCode32 { return {inv4Bytes(Nop1), 4}; });

  }
}

void Asm32::translate(){
  for(auto it : splitted) translateLine(it);

  #if __DEBUG_COD
  sym_text.print();
  sym_data.print();
  #endif
}

void Asm32::printMachineCode(){
  cout << endl;
  unsigned lpc = 0;
  for(auto it : text_mcode){
    AsmCode32 code = it();
    cout << lpc << ": ";
    for(unsigned i = 1; i <= code.size_b; i++){
      unsigned short aux = (unsigned short) (code.mcode >> (8*(code.size_b - i))) & 0xFF;
      cout << hex << aux << " ";
    }
    cout << endl;
    lpc += code.size_b;
  }
  cout << endl;
}

void Asm32::writeAsm(string filename){
  ofstream file;
  file.open(filename);

  for(auto it : text) file << it << endl;
  for(auto it : data) file << it << endl;

  file.close();
  return;
}

void Asm32::writeBytes(std::string filename){

  ofstream file;
  file.open(filename);

  sym_data.offset(pc);
  sym_text.offset(0); //endereço inicial é zero para código não-formatado

  AsmCode32 code;
  int lpc = 0;

  #if __PRETTY_PRINT_NO_FORMAT
  file <<  "SECTION TEXT" << endl << endl;
  #endif

  for(auto it : text_mcode){
    code = it();
    #if __PRETTY_PRINT_NO_FORMAT
    file << hex << setfill('0') << setw(4) << lpc << ": ";
    #endif
    for(unsigned i = 1; i <= code.size_b; i++){
      unsigned short aux = (unsigned short) (code.mcode >> (8*(code.size_b - i))) & 0xFF;
      file << hex << setfill('0') << setw(2) << aux << " ";
    }
    #if __PRETTY_PRINT_NO_FORMAT
    file << endl;
    #endif
    lpc+=code.size_b;
  }

  #if __PRETTY_PRINT_NO_FORMAT
  file <<  endl << "SECTION DATA" << endl << endl;
  #endif

  for(auto it : data_mcode){
    code = it();
    #if __PRETTY_PRINT_NO_FORMAT
    file << hex << setfill('0') << setw(4) << lpc << ": ";
    #endif
    for(unsigned i = 1; i <= code.size_b; i++){
      unsigned short aux = (unsigned short) (code.mcode >> (8*(code.size_b - i))) & 0xFF;
      file << hex << setfill('0') << setw(2) << aux << " ";
    }
    #if __PRETTY_PRINT_NO_FORMAT
    file << endl;
    #endif
    lpc+=code.size_b;
  }

  file.close();
}

vector<unsigned char> Asm32::getTextM(){
  vector<unsigned char> bytes;

  AsmCode32 code;
  for(auto it : text_mcode){
    code = it();
    for(unsigned i = 1; i <= code.size_b; i++){
      unsigned short aux = (unsigned short) (code.mcode >> (8*(code.size_b - i))) & 0xFF;
      bytes.push_back(aux);
    }
  }

  return bytes;
}

vector<unsigned char> Asm32::getDataM(){
  vector<unsigned char> bytes;

  AsmCode32 code;
  for(auto it : data_mcode){
    code = it();
    for(unsigned i = 1; i <= code.size_b; i++){
      unsigned short aux = (unsigned short) (code.mcode >> (8*(code.size_b - i))) & 0xFF;
      bytes.push_back(aux);
    }
  }

  return bytes;
}

unsigned Asm32::getPC(){
  return pc;
}

void Asm32::setOffset(unsigned text_begin, unsigned data_begin){
  sym_text.offset(text_begin);
  sym_data.offset(data_begin);
}

unsigned Asm32::getStart(){
  return sym_text.get("_start");
}

vector<Symbol> Asm32::getSymTextVec(){
  return sym_text.getVector();
}
vector<Symbol> Asm32::getSymDataVec(){
  return sym_data.getVector();
}
