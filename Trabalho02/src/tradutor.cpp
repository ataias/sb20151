/*
 * Alunos:
 * - Ataias Pereira Reis - 10/0093817
 * - Pedro Paulo Struck Lima - 11/0073983
 * ---------------------------------------
 * tradutor.cpp
 *
 * Este arquivo contém a função principal para o tradutor.
 * Os argumentos são:
 * 1 - arquivo de entrada
 * 2 - nome de arquivo de saída para código assembly, costuma-se usar a extensão .asm
 * 3 - nome de arquivo de saída para código máquina não-formatado, extensão pedida .cod
 * 4 - nome do arquivo de saída executável elf
 *
 * A criação do elf é feita com o auxílio da biblioteca ELFIO
 *
 * Para compilar este tradutor, dentro da pasta src/ pode-se digitar
 * `make tradutor`
 * */


#include <iostream>
#include <vector>
#include <functional>
#include <string>
#include <sstream>
#include <fstream>
#include <Assembler.h>
#include <Asm32.h>
#include <cstdlib>
#include <elfio/elfio.hpp>

using namespace std;
using namespace ELFIO;

//Boa parte desta função foi feita com a ajuda do tutorial disponibilizado em
//http://elfio.sourceforge.net/elfio.pdf
//Acesso em 16 de Junho de 2015

int create_elf_sb(Asm32 obj, string filename, unsigned text_begin, unsigned data_begin){

  elfio writer;
  writer.create(ELFCLASS32, ELFDATA2LSB);

  writer.set_os_abi(ELFOSABI_LINUX);
  writer.set_type(ET_EXEC);
  writer.set_machine(EM_386);

  //seção de texto
  section* text_sec = writer.sections.add(".text");
  text_sec->set_type(SHT_PROGBITS);
  text_sec->set_flags(SHF_ALLOC | SHF_EXECINSTR);
  text_sec->set_addr_align(0x10);

  //obtendo seção de texto
  vector<unsigned char> text_v = obj.getTextM();

  unsigned char *text_aux = (unsigned char*) calloc (text_v.size(), sizeof(unsigned char));
  for(unsigned i = 0; i < text_v.size(); i++){
    text_aux[i] = text_v.at(i);
  }
  char *text = (char *) text_aux;
  text_sec->set_data(text, text_v.size());

  segment* text_seg = writer.segments.add();
  text_seg->set_type(PT_LOAD);
  text_seg->set_virtual_address(text_begin);
  text_seg->set_physical_address(text_begin);
  text_seg->set_flags(PF_X | PF_R);
  text_seg->set_align(0x1000);

  text_seg->add_section_index(text_sec->get_index(),
                              text_sec->get_addr_align());

  //seção de dados
  section* data_sec = writer.sections.add(".data");
  data_sec->set_type(SHT_PROGBITS);
  data_sec->set_flags(SHF_ALLOC | SHF_WRITE);
  data_sec->set_addr_align(0x4);

  vector<unsigned char> data_v = obj.getDataM();
  unsigned char *data_aux = (unsigned char*) calloc (data_v.size(), sizeof(unsigned char));
  for(unsigned i = 0; i < data_v.size(); i++){
    data_aux[i] = data_v.at(i);
  }
  char *data = (char *) data_aux;
  data_sec->set_data(data, data_v.size());

  segment* data_seg = writer.segments.add();
  data_seg->set_type(PT_LOAD);
  data_seg->set_virtual_address(data_begin);
  data_seg->set_physical_address(data_begin);
  data_seg->set_flags(PF_W | PF_R);
  data_seg->set_align(0x10);

  data_seg->add_section_index(data_sec->get_index(),
                              data_sec->get_addr_align());

  //------------------------Tabela de símbolos ------------------------------
  //Para a colocar a tabela de símbolos no elf, analisou-se o exemplo dado em
  //https://github.com/serge1/ELFIO/blob/master/examples/write_obj/write_obj.cpp
  //Acesso em 16 de Junho de 2015

  // Create string table section
    section* str_sec = writer.sections.add( ".strtab" );
    str_sec->set_type      ( SHT_STRTAB );
  // Create string table writer
    string_section_accessor stra( str_sec );

  // Create symbol table section
    section* sym_sec = writer.sections.add( ".symtab" );
    sym_sec->set_type      ( SHT_SYMTAB );
    sym_sec->set_info      ( 2 );
    sym_sec->set_addr_align( 0x4 );
    sym_sec->set_entry_size( writer.get_default_entry_size( SHT_SYMTAB ) );
    sym_sec->set_link      ( str_sec->get_index() );

  // Create symbol table writer
  symbol_section_accessor syma( writer, sym_sec );

  //símbolos da seção de texto
  vector<Symbol> symbols = obj.getSymTextVec();
  vector<Symbol>::iterator it;
  for(it = symbols.begin(); it != symbols.end(); ++it){
    syma.add_symbol( stra, it->label.c_str(), it->value + text_begin, 0, STB_WEAK, STT_FUNC, 0,
                    text_sec->get_index() );
  }

  //símbolos da seção de dados
  symbols = obj.getSymDataVec();
  for(it = symbols.begin(); it != symbols.end(); ++it){
    syma.add_symbol( stra, it->label.c_str(), it->value + data_begin, 0, STB_WEAK, STT_FUNC, 0,
                    data_sec->get_index() );
  }

  //------------------------Fim da Tabela de símbolos -------------------------

  //------------------------Seção de notas ------------------------------------
  //Para criação de notas no elf, analisou-se o exemplo dado em
  //https://github.com/serge1/ELFIO/blob/master/examples/write_obj/write_obj.cpp
  //Acesso em 16 de Junho de 2015
  //Notas podem ser lidas por meio de
  // `readelf -a arquivoelf` no terminal do linux ou mac
  section* note_sec = writer.sections.add( ".note" );
  note_sec->set_type( SHT_NOTE );
  note_section_accessor note_writer( writer, note_sec );
  note_writer.add_note( 0x01, "Criado por meio da biblioteca ELFIO", 0, 0 );
  note_writer.add_note( 0x01, "Trabalho de Software Basico - 2015", 0, 0 );
  note_writer.add_note( 0x01, "Alunos:", 0, 0 );
  note_writer.add_note( 0x01, "Ataias Pereira Reis - 10/0093817", 0, 0 );
  note_writer.add_note( 0x01, "Pedro Paulo Struck Lima - 11/0073983", 0, 0 );

  //------------------------Fim da Seção de notas -----------------------------

  writer.set_entry(obj.getStart());

  writer.save(filename);
  free(text_aux);
  free(data_aux);
  return 0;
}


//Função de pré-processamento
//Retira caracteres indesejados, como tabs repetidos e espaços
vector<string> preprocess(string filename){
  string line;
  ifstream file(filename);
  vector<string> code;
  unsigned i = 0;

  if (file.is_open()){
    while ( getline (file,line) ){
      //trocar todos os tabs por espaços
      replace(line.begin(), line.end(), '\t', ' ');

      //troca "," por " ,"
      i=0;
      while(i<line.length()){
        if(line[i]==',') {
          line.replace(i, 1, " , ");
          i+=3;
        } else {
          i++;
        }
      }

      //troca ":" por " :"
      i=0;
      while(i<line.length()){
        if(line[i]==':') {
          line.replace(i, 1, " :");
          i+=2;
        } else {
          i++;
        }
      }

      //troca "+" por " + "
      i=0;
      while(i<line.length()){
        if(line[i]=='+') {
          line.replace(i, 1, " + ");
          i+=3;
        } else {
          i++;
        }
      }

      //troca "-" por " - "
      i=0;
      while(i<line.length()){
        if(line[i]=='-') {
          line.replace(i, 1, " - ");
          i+=3;
        } else {
          i++;
        }
      }

      //troca caracteres em maiúscula para minúscula e remove comentarios
      i=0;
      while(line[i] != '\0'){
        if(line[i] <= 0x5A /*Z*/ && line[i] >= 0x41 /*A*/)
          line[i] += 0x20;

        //0x3B = ; (ignorar tudo o que tiver depois de ;)
        if(line[i] == 0x3B){
          line.erase((size_t) i, (size_t) (line.length()-i));
          break;
        }
        i++;
      }

      //remover espaços repetidos no meio do programa
      //Ideia para remover espaços adjacentes: http://goo.gl/JmRvYG
      //ideia de função lambda: http://goo.gl/HfIQDF
      string::iterator new_end = unique(line.begin(), line.end(),[](char a, char b) { return a == ' ' && b == ' '; } );
      line.erase(new_end, line.end());

      //remover espaço do início da linha, se houver
      if(line[0]==' ')  line.erase(0, (size_t) 1);

      //remover \n
      line.erase(remove(line.begin(), line.end(), '\n'), line.end());

      //remover carriage return
      line.erase(remove(line.begin(), line.end(), 13), line.end());

      //adicionar linha pré-processada ao vetor<string> code
      code.push_back(line);
    }
    file.close();
  } else {
    cout << "Não foi possível abrir o arquivo." << endl;
  }
  return code;
}
//função para tratar a diretiva EQU
int tratarEQU (vector<string> &code, map<string,int> &map_equ) {
	size_t found_section;	//flag de busca da substring section
	size_t found_equ;		//flag de busca da substring equ
	int valor_int_equ;
	int num_erros = 0;
	unsigned i; //j;
	string valor_equ;	//valor (em formato string) que será colocado no lugar de op_equ no código todo
	string op_equ; 		//operando que será trocado por valor_equ
	string diretiva; 	//guardará a diretiva "equ", mas não será usada pra nada
	string dois_pontos; //não será usado pra nada, só pra ler os dois pontos da linha que tem equ
	
	regex numsintax ("([\\d]+)");
	regex tokensintax ("[a-z_]([a-z0-9_]*)");
	
	//primeira passagem para montar a tabela de equ
	for (i = 0; i < code.size(); i++)
	{
		//monta o mapeamento de equ
		found_section = code.at(i).find("section ");
		if (found_section != string::npos) {
			//j = i;
			break;
		}
		else
		{
			found_equ = code.at(i).find(" equ ");
			
			// se achou a substring "equ" na linha atual
			if (found_equ != string::npos) {
				string leftLine = code.at(i).substr(found_equ + 4);
				//checa duplicata de diretiva equ na mesma linha
				if (leftLine.find(" equ ") == string::npos)
				{
					//iss recebe toda a linha atual de código
					istringstream iss(code.at(i));
					
					//passando as substrings para as respectivas variáveis
					iss >> op_equ >> dois_pontos >> diretiva >> valor_equ;
					
					//checa se a label de EQU está na forma certa da sintaxe de tokens
					if (regex_match(op_equ, tokensintax))
					{
						//adiciona a nova label e seu valor ao mapeamento de EQU se ainda não existe. caso se encontre, dá um erro de redefinição de label.
						if (map_equ.find(op_equ) != map_equ.end())
						{
							cout << "L" << i + 1 << ": ERRO. Label de EQU já definida." << endl;
							num_erros++;
							continue;
						}
						else
						{
							//antes de adicionar ao mapeamento, tem que checar se valor_equ é composta somente de digitos						
							if (regex_match(valor_equ, numsintax))
							{
								valor_int_equ = stoi(valor_equ);
								map_equ[op_equ] = valor_int_equ;
							}
							else
							{
								cout << "L" << i + 1 <<  ": ERRO. Operando de EQU não é um número válido." << endl;
								num_erros++;
								continue;
							}
						}
					}
					else
					{
						//cout << "L" << i + 1 <<  ": ERRO. Label fora do formato esperado (inicia-se sem numeros)." << endl;
						num_erros++;
						continue;
					}
					
				}
				else
				{
					cout << "L" << i + 1 <<  ": ERRO. Diretiva EQU duplicada na mesma linha." << endl;
					num_erros++;
					continue;
				}
			}
			else
			{
				if (code.at(i).length() != 0)
				{
					if (code.at(i).find(" equ") != string::npos && (code.at(i).find(" equ")+4) >= (code.at(i).length()))
					{
						cout << "L" << i + 1 << ": ERRO. Diretiva EQU sem operando." << endl;
					}
					else if (code.at(i).find("equ ") == 0)
					{
						cout << "L" << i + 1 <<  ": ERRO. Diretiva EQU sem label associada." << endl;
					}
					//else 
					//{
						//cout << "L" << i + 1 <<  ": ERRO. Diretiva inválida." << endl;
					//}
					num_erros++;
					continue;
				}
				
			}
			//apaga a linha atual que tem "equ"
			vector<string>::iterator ite = code.begin() + i;
			ite->erase();
		}
	}
	
	//segunda passagem para substituir os valores do mapeamento no código
	for (; i < code.size(); i++) {
		
		for (auto &x: map_equ) {
			string equ_reg = "\\b" + x.first + "\\b";
			code.at(i) = regex_replace(code.at(i), regex(equ_reg), to_string(x.second));
		}
	}
	return num_erros;
}

//função para tratar a diretiva IF
int tratarIF (vector<string> &code, map<string,int> &map_equ) {
	int valor_if;
	int num_erros = 0;
	size_t found_if;		//flag de busca da substring equ
	unsigned i;
	string op_if;		//operando do if
	string diretiva; 	//guardará a diretiva "if", mas não será usada pra nada;
	
	regex numsintax ("([\\d]+)");
	regex tokensintax ("[a-z_]([a-z0-9_]*)");
	
	//procura labels "IF" até o final do código
	for (i = 0; i < code.size(); i++) {
		// se achou a substring "if" na linha atual, avalie.
		found_if = code.at(i).find("if ");
		if (found_if != string::npos && found_if == 0) {
			
			//iss recebe a linha atual de código
			istringstream iss(code.at(i));
			
			//passando as substrings para as respectivas variáveis
			iss >> diretiva >> op_if;
			
			//vê se o operando do if é composto somente por números
			if (regex_match(op_if, numsintax)) {
				valor_if = stoi(op_if);
				
				//se if == 0, apague a linha do if e a seguinte
				//se if != 0, apague só a linha do if
				vector<string>::iterator ite = code.begin() + i;
				ite->erase();
				if (valor_if == 0) {
					(ite + 1)->erase();
				}
			}
			//se não for composto somente por números, checa se é um token válido
			else {
				//se for um token válido, procura op_if na tabela de operandos do EQU (mapeamento de EQU)
				if (regex_match(op_if, tokensintax)) {
					if (map_equ.find(op_if) != map_equ.end()) {
						//se achou, colocar o valor encontrado em valor_if
						//map<string,int>::iterator itm = map_equ.find(op_if);
					}
					else {
						cout << "L" << i + 1 <<  ": ERRO. Operando do IF não definido." << endl;
						num_erros++;
						continue;
					}
				}
				else {
					cout << "L" << i + 1 <<  ": ERRO. O operando do IF é um token inválido." << endl;
					num_erros++;
					continue;
				}
			}
		}
	}
	return num_erros;
}

void imprimirCodigo (vector<string> code) {
	unsigned i;
	for (i = 0; i < code.size(); i++) {
		cout << i+1 << ": " << code.at(i) << endl;
	}
}

void modoDeUsar(){
  cout << endl << "Modo de usar: " << endl;
  cout << endl << "./tradutor inventado.asm ia32.asm ia32.cod ia32.elf" << endl;
  cout << endl << "Nota: extensões são obrigatórias!" << endl;
}

int main(int argc, char* argv[]){
	
  if(argc < 5){
    modoDeUsar();
    return 0;
  }

  string asmfile    = argv[1];
  string ia32       = argv[2];
  string cod        = argv[3];
  string elf_file   = argv[4];

  vector<string> code = preprocess(argv[1]);
  
  int nErrosPre = 0;
  map<string,int> map_equ;
  nErrosPre = tratarEQU(code, map_equ); //tratar as ocorrências da diretiva EQU a partir do código completo
  nErrosPre += tratarIF(code, map_equ);	//tratar as ocorrências da diretiva IF a partir do código completo
  
  //evita que vá para a montagem com erros de preprocessamento
  if (nErrosPre > 0)
  {
	cout << endl;
	
	#if __NOTRELEASE
	imprimirCodigo(code);
	#endif
	
	//cout << endl << "Erros no preprocessamento. Não foi possível traduzir o arquivo." << endl;
	//cout << "Verifique se o seu programa tem secoes text e data." << endl;
	//return 3;
  }
  
  //impossibilidade de abrir arquivo é indicada por retorno de valor 1
  if(code.empty()) return 1;

  vector<Elements> splitted;
  unsigned nErros = 0;
  try {
    //realiza-se a primeira e segunda passagem do montador para se verificarem
    // erros, após isso obtêm-se as linhas separadas em vector<Elements>
    // e também o número de erros
    Assembler assemble(code, nErrosPre);
    assemble.pass1();
    assemble.pass2();
    splitted = assemble.getSplitted();
    nErros = assemble.getNumberOfErrors();
  } catch (const std::regex_error& e){
    cout << "Erro capturado: " << e.what() << endl;
    cout << "Provavelmente o compilador usado não suporta totalmente regex do C++11." << endl;
    cout << "Utilize g++ a partir da versão 4.9 ou clang++ a partir da versão 3.6." << endl;
  }

  //número de erros maior que 0 é indicado por retorno de 2
  if(nErros > 0){
    cout << endl << "Erros na montagem, não foi possível traduzir o arquivo." << endl;
    return 2;
  }

  function<string (vector<string>)> cat = [] (vector<string> v) -> string {
    vector<string>::iterator it;
    string arg = "";
    for(it = v.begin(); it != v.end(); ++it)
      arg += it->c_str();
    return arg;
  };

  //Para garantir que as variáveis não tenham nomes iguais aquelas que
  //são usadas no header do programa, adiciona-se o prefixo "__" nas variáveis
  //do assembly inventado
  vector<Elements>::iterator it;
  for(it = splitted.begin(); it != splitted.end(); ++it){
    if(!it->label.empty()) it->label.append("_ASM");
    if(it->operation.compare("space") &&
       it->operation.compare("const") &&
       it->operation.compare("section")  )
      if(!it->op1.empty()) it->op1.at(0).append("_ASM");
    if(!it->op2.empty()) it->op2.at(0).append("_ASM");
  }

  #if __PPRINTCOD
  unsigned __i = 0;
  cout << endl << "Código depois de substituições de nomes de variávies" << endl;
  for(it = splitted.begin(); it != splitted.end(); ++it){
    __i++;
    cout << dec << __i << ": ";
    if(!it->label.empty()) cout << "Cmd: " << it->label;
    if(!it->operation.empty()) cout << " op: " << it->operation;
    if(!it->op1.empty()) cout << " op1: " << cat(it->op1);
    if(!it->op2.empty()) cout << " op2: " << cat(it->op2);
    cout << endl;
  }
  #endif

  Asm32 asm32 = Asm32(splitted, 0, 0);
  asm32.translate();
  asm32.writeAsm(ia32);
  asm32.writeBytes(cod);

  //calculando início da seção de dados
  vector<unsigned char> text_v = asm32.getTextM();
  unsigned data_begin = text_v.size() + TEXT_BEGIN;
  data_begin = data_begin + (16 - data_begin % 16);
  asm32.setOffset(TEXT_BEGIN, data_begin);

  create_elf_sb(asm32, elf_file, TEXT_BEGIN, data_begin);
  string cmd = "chmod +x " + elf_file;
  system(cmd.c_str());

  //execução bem sucedida, retorna 0
  return 0;
}
