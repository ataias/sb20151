/*
 * Alunos:
 * - Ataias Pereira Reis - 10/0093817
 * - Pedro Paulo Struck Lima - 11/0073983
 * ---------------------------------------
 */

#include <iostream>
#include <vector>
#include <Object.h>

//compilar
//g++ -c -std=c++11 -Wall -Wextra Object.cpp -I ../include

using namespace std;

Object::Object(){

}

void Object::push(std::string code, unsigned l){
  push(code, l, false);
}


void Object::push(string code, unsigned l, bool isConst){
  obj.push_back(code);
  line.push_back(l);
  isConstant.push_back(isConst);
}

string Object::get(unsigned pos){
  return obj.at(pos);
}

bool Object::isConst(unsigned pos){
  return isConstant.at(pos);
}

void Object::print(){
  cout << endl << "OBJECT" << endl;
  vector<string>::iterator sp;
  for(sp = obj.begin(); sp != obj.end(); ++sp){
    cout << *sp << " ";
  }
  cout << endl;
}

unsigned Object::size(){
  return obj.size();
}

void Object::set(std::string address, unsigned pos){
  obj[pos] = address;
}

unsigned Object::getLine(unsigned pos){
  return line.at(pos);
}

void Object::setDataInit(unsigned addrDataInit){
  this->addrDataInit = addrDataInit;
  hasData = true;
}
bool Object::isInTextSection(unsigned pos){
  return (pos > 0 && pos < addrDataInit);
}

unsigned Object::getDataInit(){
  if(!hasData) {
    throw "Não há seção de dados.\n";
  }
  return addrDataInit;
}

void Object::reorder(sectionDef text, sectionDef data){
  if(data.end == -1) data.end = obj.size() - 1;
  if(text.end == -1) text.end = obj.size() - 1;

  //seção de dados antes da seção de texto
  unsigned dataSize = data.end - data.begin + 1;
  unsigned dataInit = 0;
  if(data.begin < text.begin){
    for(unsigned i=0; i<dataSize; i++){
      obj.push_back(obj.at(i));
      line.push_back(line.at(i));
      isConstant.push_back(isConstant.at(i));
    }
    obj.erase(obj.begin(), obj.begin()+dataSize);
    line.erase(line.begin(), line.begin()+dataSize);
    isConstant.erase(isConstant.begin(), isConstant.begin()+dataSize);

    dataInit = text.end - dataSize + 1;
    setDataInit(dataInit);
  }
}
