;código (operando1) (operando2)

;lbl: add label+imm
lbl: add eax, dword [label+imm*4]

;lbl: sub label+imm
lbl: sub eax, dword [label+imm*4]

;lbl: mult label+imm
lbl:
push edx
imul dword [label+imm*4]
pop edx


;lbl: div label+imm
lbl:
push edx
cdq
idiv dword [label+imm*4]
pop edx

;lbl: jmp label+imm
; não posso multiplicar imm por um outro número... não se há certeza de quantos bytes vai ter para a próxima instrução
lbl: jmp label+imm

;lbl: jmpn label+imm
lbl:
push ebx
sub ebx, ebx
cmp eax, ebx
jl label+imm
pop ebx

;lbl: jmpp label+imm
lbl:
push ebx
sub ebx, ebx
cmp eax, ebx
jg label+imm
pop ebx

;lbl: jmpz label+imm?
;bem, o usuário até pode usar um imediato... mas vai dar errado
lbl:
push ebx
sub ebx, ebx ;ebx = 0
cmp eax, ebx ;compare acumulador com 0
je label+imm
pop ebx

;lbl: copy a+imm1, b+imm2
lbl:
push eax
mov eax, [a+imm1*4]
mov dword [b+imm2*4], eax
pop eax

;lbl: load a+imm
lbl:
mov eax, [a+imm*4]

;lbl: store a+imm
lbl:
mov dword [a+imm*4], eax

;lbl: input a+imm
lbl:
push eax
call LerUInt
mov dword [a+imm*4], eax
pop eax
;lbl: output a+imm
lbl:
push eax
mov eax, [a+imm*4]
call EscreverUInt
pop eax

;lbl: stop
; considero que não precise salvar o valor de eax... ou ebx
lbl:
mov eax, 1
mov ebx, 0
int 0x80
