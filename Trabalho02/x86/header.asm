;headerBSD.
%define SIZE_B 16

section .bss
;leitura do input e escrita no output precisam de uma variável buffer
buffer: resb SIZE_B

section .data

section .text
;;;;;;;;;;;;;;;;;;;;;;;; Início de LerUInt ;;;;;;;;;;;;;;;;;;;;;;;;
;Lê uma string do usuário e converte para um número
;Conforme o professor disse, não considera sinal
;Resultado é retornado em eax
LerUInt:
            call clearBuffer
            push ebx
            push ecx
            push edx
            push esi
            push edi ;neste registrador, será salvo o sinal
            mov edx, SIZE_B ;getStr buffer, SIZE_B
            mov ecx, buffer
            mov ebx, 0; teclado
            mov eax, 3 ;sys_read
            int 0x80

            sub eax, eax ; resultado final
            sub ebx, ebx ; valor de um caractere
            mov ecx, buffer ;índice

            ;verificar sinal
            mov edi, 1
            mov bl, [ecx]
            cmp bl, '+'
            je LerHasSign
            cmp bl, '-'
            je LerNegative
            jmp loopLer

LerNegative:   mov edi, -1
LerHasSign:    inc ecx
loopLer:    mov bl, [ecx]
            cmp bl, '0'
            jb doneLer
            cmp bl, '9'
            ja doneLer
            sub bl, '0' ;transforma em número

            ;multiplica por 10
            shl eax, 1
            mov esi, eax
            shl eax, 2
            add eax, esi

            add eax, ebx
            inc ecx
            jmp loopLer
doneLer:
            imul eax, edi ;multiplica pelo sinal
            pop edi
            pop esi
            pop edx
            pop ecx
            pop ebx
            ret
;;;;;;;;;;;;;;;;;;;;; Fim de LerUInt ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;; Início de EscreverUInt ;;;;;;;;;;;;;;;;;;;;;;;;
;Assume número a ser escrito está em eax
;não modifica registradores, todos são salvos na pilha e depois retornados
EscreverUInt:
              call clearBuffer
              push eax ;salva registradores na pilha
              push ebx
              push ecx
              push edx
              push esi

              mov esi, 0
              sub ebx, ebx ;ebx = 0
              cmp eax, ebx
              jl EscreverNegative
              jmp EscreverPositivo

              ;se número for negativo
EscreverNegative:
              mov ebx, 0xffffffff
              mov ecx, eax
              xor ecx, ebx
              add ecx, 1 ;número sem sinal está salvo em ecx
              mov esi, eax ;esi salva eax
              mov eax, ecx



EscreverPositivo:
              push dword 0xa ;fim do número
              mov ecx, 10
     toChar:  div ecx ;eax tem resultado, edx o resto
              add edx, '0'
              push edx ;põe char que faz parte do número final na pilha
              sub edx, edx ;edx = 0
              cmp eax, edx ;verifica se o número que sobrou em eax/10 é zero
              jnz toChar ;enquanto não for zero, repete


              ;verificar se há sinal para colocar no buffer
              sub ebx, ebx
              cmp esi, ebx
              jnz toBufferNegativo
              jmp toBufferPositivo

toBufferNegativo:
              push dword '-'
toBufferPositivo:
              mov ecx, buffer
toBuffer:     pop eax
              mov byte [ecx], al
              inc ecx
              mov edx, 0xa ;endl
              cmp al, dl
              jnz toBuffer

              mov edx, SIZE_B ;length
              mov ecx, buffer ;message pointer
              mov ebx, 1 ; stdout
              mov eax, 4 ;sys_write
              int 0x80

              pop esi
              pop edx ;retira registradores da pilha
              pop ecx
              pop ebx
              pop eax
              ret
;;;;;;;;;;;;;;;;;;;;; Fim de EscreverUInt ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;; Clear buffer ;;;;;;;;;;;;;;;;;;;;;;;;;;
clearBuffer:
push ecx
mov ecx, buffer

mov dword [ecx], 0

add ecx, 4
mov dword [ecx], 0

add ecx, 4
mov dword [ecx], 0

add ecx, 4
mov dword [ecx], 0

pop ecx
ret

global _start
_start:
  mov eax, 10
  call EscreverUInt
  call LerUInt
  add eax, 4
  mov ebx, eax
  mov eax, 1
  int 0x80
