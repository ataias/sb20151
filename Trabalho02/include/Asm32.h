#ifndef ASM32_H__
#define ASM32_H__

#include <Struct.h>
#include <Table.h>

//Início da seção de texto no ELF
#define TEXT_BEGIN 0x08048000

//Há duas formas para se imprimir o código objeto não formatado
//0 - forma como o professor pediu
//1 - forma mais bonita de se analisar
#define __PRETTY_PRINT_NO_FORMAT 0

//mostrar algumas informações da tradução na tela, tabela de símbolos... etc
#define __DEBUG_COD 0

class Asm32 {
private:
  //código de máquina é salvo como um vetor de funções lambda que dependem
  //das tabelas de símbolos - lazy evaluation
  std::vector<std::function<AsmCode32 ()>> text_mcode;
  std::vector<std::function<AsmCode32 ()>> data_mcode;

  //código assembly é salvo nos vetores text, data e bss
  //basta concatenar um em seguida do outro que a montagem do .asm com
  // assembler nasm funciona bem
  std::vector<std::string> text;
  std::vector<std::string> data;

  std::vector<Elements> splitted;

  Table sym_text = Table("Symbol not found\n", "Text Symbols");
  Table sym_data = Table("Symbol not found\n", "Data Symbols");

  //pc - contador de programa, 0 relativo ao início da seção de texto
  unsigned pc;

  //data_pc - contador de programa, 0 relativo ao início da seção de dados
  unsigned data_pc;

  std::function<u_long (u_long)> inv4Bytes;
public:
  //o constructor usa splitted fornecido do montador
  //não há verificação de erros aqui, o montador deve ser sido chamado antes e,
  //caso não haja erros, pode-se então traduzir

  Asm32(std::vector<Elements> splitted, unsigned text_begin, unsigned data_begin);

  // std::function<u_long (unsigned, unsigned)> input(std::string label, unsigned offset);
  std::vector<std::string> readFile(std::string filename);

  //traduz uma linha de elements para assembly ia32
  void translateLine(Elements assembly_line);

  //traduz todo o vetor splitted para assembly ia32
  void translate();
  void print();

  void printMachineCode();

  //escreve arquivo assembly
  void writeAsm(std::string filename);

  //escreve arquivo com código máquina não formatado
  void writeBytes(std::string filename);
  std::vector<unsigned char> getTextM();
  std::vector<unsigned char> getDataM();

  //caso o offset não seja conhecido de início, pode-se mudá-lo
  void setOffset(unsigned text_begin, unsigned data_begin);

  //obtém pc
  unsigned getPC();

  //obtém endereço da variável _start
  unsigned getStart();

  //retorna tabela de símbolos da seção de texto, sem offset
  std::vector<Symbol> getSymTextVec();

  //retorna tabela de símbolos da seção de dados, sem offset
  std::vector<Symbol> getSymDataVec();
};

#endif
