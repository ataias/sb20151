#ifndef ASSEMBLER_H__
#define ASSEMBLER_H__

/*
 * Alunos:
 * - Ataias Pereira Reis - 10/0093817
 * - Pedro Paulo Struck Lima - 11/0073983
 * ---------------------------------------
 * Assembler.h
 *
 * Declara protótipos classe Assembler que é largamente utilizada no projeto
 *
 * */

#include <Object.h>
#include <Struct.h>
#include <Table.h>
#include <set>
#include <map>
#include <regex>

//Mostrar tabela de símbolos na saída padrão
#define __ASSEMBLER_SYMBOLS_SHOW 0

//Mostrar código objeto tabela em algumas etapas do processo de montagem
#define __ASSEMBLER_OBJECT_SHOW 0

//Mostra valor das fronteiras, onde início o código texto e onde termina, assim como
//a parte de dados
#define __ASSEMBLER_BOUNDS_SHOW 0

//ainda em construção
class Assembler{
private:
  Section cSection; //utilizado para dizer seção atual

  //instanciar tabelas e classe objeto
  Table sym = Table("Symbol not found\n", "SYMBOLS TABLE");
  Object object = Object();
  CmdTable cmdTable = CmdTable();

  //expressões regulares
  std::regex opsyntax; //operand syntax
  std::regex validtoken;
  std::regex labelsyntax;
  std::regex tokensyntax;

  unsigned nErros = 0; //número de erros

  //instanciar vetores utilizados no projetos
  std::vector<Elements> splitted; //assembly dividido em seções
  std::vector<std::string> code; //código assembly

  //Este map foi adicionado para se verificar erro de out of scope
  // mapeia unsigned para struct Var (veja Struct.h)
  //unsigned indica o endereço no qual a variável foi definida
  //Var contém o nome da variável, seu tamanho alocado e a linha na qual foi definida
  std::map<unsigned,Var> variableScope;

  SectionDef text, data;
public:

  Assembler(std::vector<std::string> code, unsigned nErrosPre);

  void split(std::string line, unsigned l);

  void printSplitted();

  void pass1();

  void pass2();

  void createSymTable();

  void exeCmd(Elements spline, unsigned l, int *pc);

  void printUndeclared(std::set<std::string> uLabels);

  void printLex(Elements spline, unsigned l, std::regex tokentype);

  void verifyDivision();

  void memoryCheck();

  void constCheck();

  void jumpCheck();

  bool writeFile(std::string filename);

  //deve ser chamada depois da chamada split
  std::vector<Elements> getSplitted();

  unsigned getNumberOfErrors();
};

#endif
