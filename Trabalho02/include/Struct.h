#ifndef STRUCT_H__
#define STRUCT_H__

#include <functional>

//not release = 0 evita que algumas mensagens apareçam na tela
//not release = 1 mostra o processo parcial de montagem, que ocorre até mesmo na presença de erros
#define __NOTRELEASE 0

enum Section { TEXT, DATA, UNDEF};

//Símbolo é uma estrutura utilizada para
// as tabelas de símbolo, definição e uso
typedef struct symbol{
  std::string label;
  unsigned value;
  Section section;
} Symbol;

//Var é uma estrutura utilizada para
// as salvar uma variável ou constante com seu endereço e seu tamanho em words de 16 bits
//também salva número de linha
typedef struct var{
  std::string label;
  unsigned size;
	unsigned line;
} Var;

//cmd é uma estrutura utilizada para
//a tabela de instruções e diretivas
//comandos podem ser instruções (0, 1 ou 2 operandos)
// ou diretivas (até um operando e opcode = 0)
typedef struct cmd{
  std::string mnemonic;
  unsigned operands;
  unsigned opcode;
  bool type1;
  bool jump;
} Cmd;

typedef struct sectionDef{
  int begin;
  int end;
} SectionDef;

//cada linha de código será separada em uma estrutura dessa
typedef struct elements{
  std::string label;
  std::string operation;
  std::vector<std::string> op1;
  std::vector<std::string> op2;
  std::vector<std::string> opN;
  unsigned line;
} Elements;

typedef struct asmcode32{
  u_long mcode;
  u_long size_b; //tamanho em bytes da instrução
} AsmCode32;

#endif
