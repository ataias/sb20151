#ifndef TABLE_H__
#define TABLE_H__

/*
 * Alunos:
 * - Ataias Pereira Reis - 10/0093817
 * - Pedro Paulo Struck Lima - 11/0073983
 * ---------------------------------------
 * Table.h
 *
 * Declara protótipos da classe Table que é utilizada para diversas tabelas.
 * Classe é utilizada no montador para as tabelas de uso, definição e símbolos.
 * No ligador, além destas, é usada para a tabela geral de definições
 * inclui métodos:
 *  constructor Table() - inicializa objeto com uma mensagem de exceção específica
 *  push() - insere um elemento na tabela, uma string com um valor
 *  print() - imprime tabela inteira
 *  get(label) - retorna valor da linha que contém símbolo label
 *  getS(label) - retorna struct Symbol que contém label
 *  getVector() - retorna um vetor com todos os símbolos da tabela
 *  set() - permite modificar dados da tabela
 * */

#include <Struct.h>

class Table{
private:
  std::vector<Symbol> t;
  std::string table_name;
  const char* error;
  unsigned section_begin; //offset
public:
  Table(const char* error, std::string table_name);
  Table(const char* error, std::string table_name, unsigned section_begin);
  void push(std::string label, unsigned value, Section section);
  void push(std::string label, unsigned value);
  void print();

  //pega uma linha da tabela de símbolos ou definição ou uso
  unsigned get(std::string label);
  Symbol getS(std::string label);
  std::vector<Symbol> getVector();
  bool set(std::string label, int value);
  void offset(unsigned section_begin); //adds offset to every value in the table
  unsigned getOffset();
  unsigned getNO(std::string label);
};

#endif
